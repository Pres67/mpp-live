<?php

/**
*  core extra funcitons 
*/

function esig_total_addons_installed()
{
			$array_Plugins = get_plugins();
			$i = 0 ; 	
			if(!empty($array_Plugins))
			{
				foreach($array_Plugins as $plugin_file => $plugin_data) 
				 {
				   if(is_plugin_active($plugin_file)) 
				   {
				        $plugin_name=$plugin_data['Name'] ; 
						
						// if($plugin_name!="WP E-Signature")
						// {  
						   if(preg_match("/WP E-Signature/",$plugin_name))
						   {  
						      if($plugin_name!="WP E-Signature")
						 	  { 
						      		$i++ ; 
							  }
						   }
					}
				}
			}
			
			return $i ; 			 
}

// esignature update addons notificaitons 

function esig_update_count_bubble ($update_data)
{
    $plugin_list=get_transient('esign-auto-downloads');	
    
        $count = 0 ; 	
	    if($plugin_list)
		{
			
	    $count = count($plugin_list);
        }
        else
        {
            return $update_data ; 
        }
        
        $c = $update_data['counts']['plugins'];
        
    $esig_count = $c + $count ; 
    
    $update_data['counts']['plugins'] = $esig_count ; 
    
    return $update_data ; 
    
}
add_filter('wp_get_update_data','esig_update_count_bubble',10,2) ;

