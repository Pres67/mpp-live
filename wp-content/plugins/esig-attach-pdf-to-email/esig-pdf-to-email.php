<?php
/**
 * @package   	      WP E-Signature - Attach PDF to Email
 * @contributors	  Kevin Michael Gray (Approve Me), Abu Shoaib (Approve Me)
 * @wordpress-plugin
 * Plugin Name:       WP E-Signature Attach PDF to Email
 * Plugin URI:        http://approveme.me/wp-digital-e-signature
 * Description:       Send Pdf to admin email address .  
 * Version:           1.1.2
 * Author:            Approve Me
 * Author URI:        http://approveme.me/
 * Text Domain:       esig-pdfemail
 * Domain Path:       /languages
 * License/Terms & Conditions: http://www.approveme.me/terms-conditions/
 * Privacy Policy: http://www.approveme.me/privacy-policy/
 */

 // If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

if ( ! class_exists( 'Esig_AddOn_Updater' ) ) 
                include(dirname(__FILE__) . '/includes/class-addon-updater.php');
                

    $esig_updater = new Esig_AddOn_Updater('Attach PDF to Email','6170',__FILE__,'1.1.2');




/*----------------------------------------------------------------------------*
 * Public-Facing Functionality
 *----------------------------------------------------------------------------*/

require_once( plugin_dir_path( __FILE__ ) . 'includes/esig-pdf-to-email.php');


/*
 * Register hooks that are fired when the plugin is activated or deactivated.
 * When the plugin is deleted, the uninstall.php file is loaded.
 */
 
register_activation_hook( __FILE__, array( 'ESIG_PDF_TO_EMAIL', 'activate' ) );
register_deactivation_hook( __FILE__, array( 'ESIG_PDF_TO_EMAIL', 'deactivate' ) );


//if (is_admin()) {
     
require_once( plugin_dir_path( __FILE__ ) . 'admin/esig-pdf-to-email-admin.php' );
add_action( 'plugins_loaded', array( 'ESIG_PDF_TO_EMAIL_Admin', 'get_instance' ) );

//}

