<?php
/**
 * @package   	      WP E-Signature - Document Templates
 * @contributors	  Kevin Michael Gray (Approve Me), Abu Shoaib (Approve Me)
 * @wordpress-plugin
 * Plugin Name:       WP E-Signature Document Templates
 * Plugin URI:        http://approveme.me/wp-digital-e-signature
 * Description:       Allows you to add esignature templates . 
 * Version:           1.1.2
 * Author:            Approve Me
 * Author URI:        http://approveme.me/
 * Text Domain:       esig-at
 * Domain Path:       /languages
 * License/Terms & Conditions: http://www.approveme.me/terms-conditions/
 * Privacy Policy: http://www.approveme.me/privacy-policy/
 */

 // If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

if ( ! class_exists( 'Esig_AddOn_Updater' ) ) 
                include(dirname(__FILE__) . '/includes/class-addon-updater.php');
                
    $esig_updater = new Esig_AddOn_Updater('Document Templates','3912',__FILE__,'1.1.2');




/*----------------------------------------------------------------------------*
 * Public-Facing Functionality
 *----------------------------------------------------------------------------*/

require_once( plugin_dir_path( __FILE__ ) . 'includes/esig-at.php' );


/*
 * Register hooks that are fired when the plugin is activated or deactivated.
 * When the plugin is deleted, the uninstall.php file is loaded.
 */
 
register_activation_hook( __FILE__, array( 'ESIG_AT', 'activate' ) );
register_deactivation_hook( __FILE__, array( 'ESIG_AT', 'deactivate' ) );


//if (is_admin()) {
     
	require_once( plugin_dir_path( __FILE__ ) . 'admin/esig-at-admin.php' );
	add_action( 'plugins_loaded', array( 'ESIG_AT_Admin', 'get_instance' ) );

//}

