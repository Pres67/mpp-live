<?php

function remove_website_row_wpse_94963() {
    if(!current_user_can('edit_users')){
        // hide only for non-admins
        echo "<script>jQuery(document).ready(function(){jQuery('#url').parents('tr').remove();});</script>";
        echo "<script>jQuery(document).ready(function(){jQuery('#description').parents('table').prev().remove();});</script>";
        echo "<script>jQuery(document).ready(function(){jQuery('#description').parents('tr').remove();});</script>";
        echo "<script>jQuery(document).ready(function(){jQuery('#rich_editing').parents('table').prev().remove();});</script>";
        echo "<script>jQuery(document).ready(function(){jQuery('#rich_editing').parents('table').remove();});</script>";
    }
}
add_action('admin_head-user-edit.php','remove_website_row_wpse_94963');
add_action('admin_head-profile.php','remove_website_row_wpse_94963');


function extra_user_profile_fields( $user ) {

    if (!current_user_can('edit_users')) {
        $readonly = " readonly='readonly'";
    } else{
        $readonly = "";
    }

    ?>
<h3><?php _e("Additional Contact Information", "blank"); ?></h3>
 <table class="form-table">
        <tr>
            <th><label for="phone"><?php _e("Phone Number"); ?></label></th>
            <td>
                <input type="text" name="phone" id="phone" value="<?php echo esc_attr( get_the_author_meta( 'phone', $user->ID ) ); ?>" class="regular-text"<?php echo $readonly; ?> /><br />
            </td>
        </tr>
</table>

    <h3><?php _e("Tenant Housing Information", "blank"); ?></h3>
    <p>If any of this information is incorrect, please contact your property manager.</p>

    <table class="form-table">
        <tr>
            <th><label for="complex"><?php _e("Complex"); ?></label></th>
            <td>
                <input type="text" name="complex" id="complex" value="<?php echo esc_attr( get_the_author_meta( 'complex', $user->ID ) ); ?>" class="regular-text"<?php echo $readonly; ?> /><br />
            </td>
        </tr>
        <tr>
            <th><label for="apartment"><?php _e("Apt"); ?></label></th>
            <td>
                <input type="text" name="apartment" id="apartment" value="<?php echo esc_attr( get_the_author_meta( 'apartment', $user->ID ) ); ?>" class="regular-text"<?php echo $readonly; ?> /><br />
            </td>
        </tr>
        <tr>
            <th><label for="rentalrate"><?php _e("Rental Rate"); ?></label></th>
            <td>
                <input type="text" name="rentalrate" id="rentalrate" value="<?php echo esc_attr( get_the_author_meta( 'rentalrate', $user->ID ) ); ?>" class="regular-text"<?php echo $readonly; ?> /><br />
            </td>
        </tr>
        <tr>
            <th><label for="deposit"><?php _e("Deposit"); ?></label></th>
            <td>
                <input type="text" name="deposit" id="deposit" value="<?php echo esc_attr( get_the_author_meta( 'deposit', $user->ID ) ); ?>" class="regular-text"<?php echo $readonly; ?> /><br />
            </td>
        </tr>
        <tr>
            <th><label for="lastmonthsrent"><?php _e("Last Months Rent"); ?></label></th>
            <td>
                <input type="text" name="lastmonthsrent" id="lastmonthsrent" value="<?php echo esc_attr( get_the_author_meta( 'lastmonthsrent', $user->ID ) ); ?>" class="regular-text"<?php echo $readonly; ?> /><br />
            </td>
        </tr>
        <tr>
            <th><label for="moveindate"><?php _e("Move In Date"); ?></label></th>
            <td>
                <input type="text" name="moveindate" id="moveindate" value="<?php echo esc_attr( get_the_author_meta( 'moveindate', $user->ID ) ); ?>" class="regular-text"<?php echo $readonly; ?> /><br />
            </td>
        </tr>
        <tr>
            <th><label for="contractend"><?php _e("Contract End Date"); ?></label></th>
            <td>
                <input type="text" name="contractend" id="contractend" value="<?php echo esc_attr( get_the_author_meta( 'contractend', $user->ID ) ); ?>" class="regular-text"<?php echo $readonly; ?> /><br />
            </td>
        </tr>
        <tr>
            <th><label for="notes"><?php _e("Notes"); ?></label></th>
            <td>
                <input type="text" name="notes" id="notes" value="<?php echo esc_attr( get_the_author_meta( 'notes', $user->ID ) ); ?>" class="regular-text"<?php echo $readonly; ?> /><br />
            </td>
        </tr>
    </table>
<?php }
add_action( 'show_user_profile', 'extra_user_profile_fields' );
add_action( 'edit_user_profile', 'extra_user_profile_fields' );


function save_extra_user_profile_fields( $user_id ) {

    if ( !current_user_can( 'edit_user', $user_id ) ) { return false; }

    update_user_meta( $user_id, 'phone', $_POST['phone'] );
    update_user_meta( $user_id, 'complex', $_POST['complex'] );
    update_user_meta( $user_id, 'apartment', $_POST['apartment'] );
    update_user_meta( $user_id, 'rentalrate', $_POST['rentalrate'] );
    update_user_meta( $user_id, 'deposit', $_POST['deposit'] );
    update_user_meta( $user_id, 'lastmonthsrent', $_POST['lastmonthsrent'] );
    update_user_meta( $user_id, 'moveindate', $_POST['moveindate'] ); 
    update_user_meta( $user_id, 'contractend', $_POST['contractend'] );
    update_user_meta( $user_id, 'notes', $_POST['notes'] );

}
add_action( 'personal_options_update', 'save_extra_user_profile_fields' );
add_action( 'edit_user_profile_update', 'save_extra_user_profile_fields' );

add_action('register_form','show_additional_fields');
add_action('register_post','check_fields',10,3);
add_action('user_register', 'register_extra_fields');

function show_additional_fields()
{?>
	<h3><?php _e("Additional Contact Information", "blank"); ?></h3>
 <table class="form-table">
        <tr>
            <th><label for="phone"><?php _e("Phone Number"); ?></label></th>
            <td>
                <input type="text" name="phone" id="phone" value="<?php echo esc_attr( get_the_author_meta( 'phone', $user->ID ) ); ?>" class="regular-text"<?php echo $readonly; ?> /><br />
            </td>
        </tr>
</table>

    <h3><?php _e("Tenant Housing Information", "blank"); ?></h3>
    <p>If any of this information is incorrect, please contact your property manager.</p>

    <table class="form-table">
        <tr>
            <th><label for="complex"><?php _e("Complex"); ?></label></th>
            <td>
                <input type="text" name="complex" id="complex" value="<?php echo esc_attr( get_the_author_meta( 'complex', $user->ID ) ); ?>" class="regular-text"<?php echo $readonly; ?> /><br />
            </td>
        </tr>
        <tr>
            <th><label for="apartment"><?php _e("Apt"); ?></label></th>
            <td>
                <input type="text" name="apartment" id="apartment" value="<?php echo esc_attr( get_the_author_meta( 'apartment', $user->ID ) ); ?>" class="regular-text"<?php echo $readonly; ?> /><br />
            </td>
        </tr>
        <tr>
            <th><label for="rentalrate"><?php _e("Rental Rate"); ?></label></th>
            <td>
                <input type="text" name="rentalrate" id="rentalrate" value="<?php echo esc_attr( get_the_author_meta( 'rentalrate', $user->ID ) ); ?>" class="regular-text"<?php echo $readonly; ?> /><br />
            </td>
        </tr>
        <tr>
            <th><label for="deposit"><?php _e("Deposit"); ?></label></th>
            <td>
                <input type="text" name="deposit" id="deposit" value="<?php echo esc_attr( get_the_author_meta( 'deposit', $user->ID ) ); ?>" class="regular-text"<?php echo $readonly; ?> /><br />
            </td>
        </tr>
        <tr>
            <th><label for="lastmonthsrent"><?php _e("Last Months Rent"); ?></label></th>
            <td>
                <input type="text" name="lastmonthsrent" id="lastmonthsrent" value="<?php echo esc_attr( get_the_author_meta( 'lastmonthsrent', $user->ID ) ); ?>" class="regular-text"<?php echo $readonly; ?> /><br />
            </td>
        </tr>
        <tr>
            <th><label for="moveindate"><?php _e("Move In Date"); ?></label></th>
            <td>
                <input type="text" name="moveindate" id="moveindate" value="<?php echo esc_attr( get_the_author_meta( 'moveindate', $user->ID ) ); ?>" class="regular-text"<?php echo $readonly; ?> /><br />
            </td>
        </tr>
        <tr>
            <th><label for="contractend"><?php _e("Contract End Date"); ?></label></th>
            <td>
                <input type="text" name="contractend" id="contractend" value="<?php echo esc_attr( get_the_author_meta( 'contractend', $user->ID ) ); ?>" class="regular-text"<?php echo $readonly; ?> /><br />
            </td>
        </tr>
        <tr>
            <th><label for="notes"><?php _e("Notes"); ?></label></th>
            <td>
                <input type="text" name="notes" id="notes" value="<?php echo esc_attr( get_the_author_meta( 'notes', $user->ID ) ); ?>" class="regular-text"<?php echo $readonly; ?> /><br />
            </td>
        </tr>
    </table>
<?php
}