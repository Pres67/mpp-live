<?php
$propid = $tenant_values['_mpp_property_value'][0];
$propertyname = get_the_title($propid);
$signed = get_post_meta($tenants[0]->ID, 'contract_signed');
if($signed[0] != 1){
?>
    <div class="section" style="margin-top:40px;">
                    <h1>Contract Details</h1>
                    <hr />
                    <h3>You have yet to sign a rental contract.  Please click <a href="http://www.myprovoproperty.com/rental-contract/">HERE</a> to sign the contract and addendum now.</h3>
<?php
} else {
?>

<div class="section" style="margin-top:40px;">
                <h1>Contract Details</h1>
                <hr />
                <p>To view all of the details of your contract, please use the link below:</p>
                <h2><a href="<?php echo wp_get_attachment_url($tenant_values['signed_contract'][0]); ?>" target="_blank">View/Download Contract </a></h2>

    <?php echo showDepositLedger($tenants[0]->ID, $deposit); ?>
</div>

<?php } ?>