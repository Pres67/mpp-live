<style>
    #image-list img{
        max-width:200px;
    }
    a{
        cursor:pointer;
    }
</style>
<div class="section" style="margin-top:40px; margin-bottom:50px;">

    <?php
    $propid = $tenant_values['_mpp_property_value'][0];
    $propertyname = get_the_title($propid);
    $signed = get_post_meta($tenants[0]->ID, 'contract_signed');
    $license = get_post_meta($tenants[0]->ID, 'drivers_license_copy');
    $additionalid = get_post_meta($tenants[0]->ID, 'additional_id');
    $tenstatus = get_post_meta($tenants[0]->ID, '_mpp_tenantstatus_value');


    $datetime1 = new DateTime($tenant_values['_mpp_startdate_value'][0]);
    $datetime2 = new DateTime('NOW');
    $interval = $datetime1->diff($datetime2);

    if($signed[0] != 1){
    ?>
    <div class="section" style="margin-top:40px;">
        <h1>Contract/Forms</h1>
        <hr />
        <h3>You have yet to sign a rental contract.  Please click <a target="_blank" href="http://www.myprovoproperty.com/rental-contract/?t=1">HERE</a> to sign the contract and addendum now.</h3>
        <?php
        } else {
            if((!$license[0] || !$additionalid[0]) && $tenstatus[0] == 1){ ?>
                <h1 style="color:red;">Missing Information</h1>
                <hr />
                <?php if(!$license[0]){ ?>
                <h3>Drivers License Copy</h3>
                <p>Please upload a copy of your drivers license:</p>
                <div style="width:200px;">
                    <form id="driverslicense" action="upload.php" method="post" enctype="multipart/form-data">
                        <input type="file" name="file" id="licenseupload">
                    </form>
                </div>
                <div id="response-driverslicense"></div>

            <?php }  if(!$additionalid[0]){ ?>
                <h3>Additional ID Copy</h3>
                <p>Please upload a copy of an additional form of ID (Student ID, Government ID, etc.):</p>
                <div style="width:200px;">
                    <form id="additionalid" action="upload.php" method="post" enctype="multipart/form-data">
                        <input type="file" name="file" id="additionalidupload">
                    </form>
                </div>
                <div id="response-additionalid"></div>

            <?php } } ?>

        <div class="section" style="margin-top:40px;">
            <h1>Your Contract</h1>
            <hr />
            <p>To view all of the details of your contract, click <a href="<?php echo wp_get_attachment_url($tenant_values['signed_contract'][0]); ?>" target="_blank">here.</a></p>
        </div>
            <div class="section" style="margin-top:40px;">
                <h1>Forms / Documents</h1>
                <hr />
                <?php if($interval->days < 6 && $interval->days > -1){
                echo '<h3>Check-In Evaluation</h3>';
                echo '<p>To complete your check-in evaluation, please <a href="/check-in-evaluation/">click here.</a> You have 5 days from check-in to complete this form.</p>';
                }?>
                <h3>Intent To Sell Form</h3>
                <p>If you intend to transfer or sell your contract, you will need to begin by filling out <a href="/intent-to-sell/"> this form.</a></p>
                <h3>Contract Transfer Form</h3>
                <p>Once your intent to sell has been approved and a suitable replacement tenant has signed a contract, you can complete your contract transfer by filling out <a href="/termination-of-lease-obligation/"> this form.</a></p>
            </div>

        <?php } ?>

    <div class="section" style="margin-top:40px;">
        <h1>Request Maintenance</h1>
        <hr />
        <p>If this is an emergency, please call your property manager. For all other issues, use the form below.  </p>
        <p>To submit a maintenance request, please <a href="http://www.myprovoproperty.com/maintenance-request/" > CLICK HERE. </a></p>
    </div>
</div>

    <div class="section" style="margin-top:40px; margin-bottom:50px;">
        <h1>Tenant Ledger</h1>
        <hr />
        <p>Here is a summary of the fees and payments on your account.  After a payment is made, please allow 24 hours for the transaction to appear in this summary.</p>
        <style>
            .payment{
                color:green;
            }
            .fee{
                color:red;
            }
            .rent{
                color:orange;
            }
            .sum{
                margin-top:35px;
            }
            .sumtitle{
                font-size:25px;
            }
        </style>
        <?php echo showDepositLedger($tenants[0]->ID, $deposit); ?>

        <?php echo showTenantLedger($tenants[0]->ID); ?>

        <?php

        include_once('payment.php');

        //show ledger
        $tenID = $tenants[0]->ID;
        $rent = checkTenantBalance($tenID);
        $deposit = checkTenantDeposit($tenID);
        $rate = get_post_meta($tenID, '_mpp_yrrate_value', true);

        if($rent <= 0){
            $payment = false;
            $message = 'Your current balance due is: $'.$rent.'. You can still make payments by eCheck or Credit Card using the buttons below.';
        }
        elseif(is_numeric($rent) && in_array($rent, $prices)){
            $rent2 = number_format($rent,2);
            $fee = number_format($rent * .035,2);
            $total = number_format(($rent + $fee),2);
            $etotal = number_format($rent + .50,2);
            $payment = true;
            $check = true;
            $message = 'Your current balance due is: $'.$rent.'. Please select a method below to make your payment';
        } elseif(is_numeric($rent)) {
            $rent2 = number_format($rent,2);
            $fee = number_format($rent * .035,2);
            $total = number_format(($rent + $fee),2);
            $etotal = number_format($rent + .50,2);
            $payment = true;
            $check = false;
            $message = 'Your current balance due is: $'.$rent.'.<br />';
        } else {
            $payment = false;
            $message = 'An error occured.  Please contact your property manager.';
        }

        ?>
        <div class="section" style="margin-top:40px;">


            <div class="payments">
                <h1>Make Payment</h1>
                <hr />
                <h3><?php echo $message; ?></h3>
                <div class="methods" style="margin-left:35px;">

<!--                    --><?php //if($check){ ?>
<!--                        <div class="method echeckpmt">-->
<!--                            <h2 class="methselect">eCheck Payment</h2>-->
<!--                            <hr />-->
<!--                            <div class="methodcontent" >-->
<!--                                <div class="subpayinfo">-->
<!--                                    Current Amount Due : $--><?php //echo $rent2; ?>
<!--                                </div>-->
<!--                                <div class="subpayinfo">-->
<!--                                    Convenience Fee : $0.50-->
<!--                                </div>-->
<!--                                <div class="subpayinfo" style="font-size:18px; padding-top:25px;">-->
<!--                                    Total Amount Due : $--><?php //echo $etotal ?>
<!--                                </div>-->
<!--                                <div class="subpayinfo" style="margin-top:50px;">-->
<!--                                    --><?php //echo display_pp_button($tenID, $rate, $rent, 'echeck'); ?>
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    --><?php //} else { ?>
                        <div class="method echeckpmt">
                            <h2 class="methselect" style="color:red;">eCheck Discontinued</h2>
                            <hr />
                            <div class="methodcontent" >
                                <div class="subpayinfo">
                                    The payment processor we used for eChecks with a $.50 fee was discontinued June 30, 2016.  We will be exploring alternate payment solutions for the future.  You can still pay with an eCheck through the Paypal link on the right (3.5% fee), or mail your payment to our office at:<br /><br />My Provo Property<br />PO BOX 446<br />Orem, UT 84059 <br/><br />As a reminder, any late fees will be asssessed based on when your mailed payment arrives, not when it was sent.
                                </div>
                            </div>
                        </div>
<!--                    --><?php //} ?>

                    <div class="method ccpayment">
                        <h2 class="methselect">Make Payment (eCheck or Credit Card)</h2>
                        <hr />
                        <div class="methodcontent">
                            <?php if($payment) { ?>

                            <div class="subpayinfo">
                                Current Rental Rate : $<?php echo $rent2; ?>
                            </div>
                            <div class="subpayinfo">
                                Convenience Fee : $<?php echo $fee; ?>
                            </div>
                            <div class="subpayinfo" style="font-size:18px; padding-top:25px;">
                                Total Rent Due : $<?php echo $total; ?>
                            </div>
                            <?php } ?>
                            <div class="subpayinfo" style="margin-top:50px;">
                                <?php echo display_pp_button($tenID, $rate, $rent, 'cc'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <script type="text/javascript">
            jQuery(document).ready(function() {
                var echeckrate = "<?php echo $etotal; ?>";
                var paypalrate = "<?php echo $total; ?>";

                var echeckoption = jQuery('div.echeckpmt option:contains('+ echeckrate +')');
                echeckoption.attr("selected",true);

                var paypaloption = jQuery('div.ccpayment option:contains('+ paypalrate +')');
                paypaloption.attr("selected",true);
            });
        </script>


    </div>