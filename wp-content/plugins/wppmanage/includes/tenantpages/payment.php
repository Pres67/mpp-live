<?php

$prices = array("289", "314", "319", "324", "329", "334", "339", "344", "299", "599", "300", "324", "326", "328", "330", "332", "334", "336", "1149", "449", "598");

function display_pp_button($tenid, $yrrrent, $rate, $method){

    if($method == 'cc'){
        $buttontext .= '<form name="_xclick" action="https://www.paypal.com/us/cgi-bin/webscr" method="post">
                <input type="hidden" name="cmd" value="_xclick">
                <input type="hidden" name="business" value="payments@myprovoproperty.com">
                <input type="hidden" name="currency_code" value="USD">
                <input type="hidden" name="item_name" value="MPP Rent">
                <input type="hidden" name="on1" value="Tenant ID">
                <input type="hidden" name="os1" value="'.$tenid.'">
                <input type="hidden" name="amount" value="'.$rate.'">
                <input type="hidden" name="tax_rate" value="3.5">
                <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_paynow_LG.gif" border="0" name="submit" alt="Pay Rent Now">
                </form>';
    } else {
        if($yrrrent == '598'){
            $buttontext ='<form method="post" target="_payByIpnWindow" action="https://ipn.intuit.com/payNow/start" id="payByIpnForm">
<input type="hidden" name="eId" value="aca2c394be63332b" /> <input type="hidden" name="uuId" value="b7afd79c-9492-4917-bf18-51bf348fd420" />
<input type="image" id="payByIpnImg" style="background-color:transparent;border:0 none;" src="https://ipn.intuit.com/images/payButton/btn_PayNow_ORG_LG.png" alt="Make payments for less with Intuit Payment Network." />
</form>';
        }
        elseif($yrrrent == "289"){
            $buttontext = '<form method="post" target="_payByIpnWindow" action="https://ipn.intuit.com/payNow/start" id="payByIpnForm">
            <input type="hidden" name="eId" value="aca2c394be63332b" />
                <div id="selectMultiplePriceDiv" style="display:none">
                    <select name="payButtonMultiplePriceCombo" style="margin-left: 3px; margin-bottom: 8px; width: 150px;">
                        <option value=47560>Rent $289.50</option>
                        <option value=47934>Late Rent $314.50</option>
                        <option value=47935>1 Day Late $319.50</option>
                        <option value=47936>2 Days Late $324.50</option>
                        <option value=47937>3 Days Late $329.50</option>
                        <option value=47938>4 Days Late $334.50</option>
                        <option value=47939>5 Days Late $339.50</option>
                        <option value=47940>6 Days Late $344.50</option>
                    </select>
                </div>
            <input type="hidden" name="uuId" value="aeb29ca8-3b2f-408a-b6e8-3322c7314a45" />
            <input type="image" id="payByIpnImg" style="background-color:transparent;border:0 none;" src="https://ipn.intuit.com/images/payButton/btn_PayNow_ORG_LG.png" alt="Make payments for less with Intuit Payment Network." />
            </form>';
        } elseif($yrrrent == "1149"){
            $buttontext = '<form method="post" target="_payByIpnWindow" action="https://ipn.intuit.com/payNow/start" id="payByIpnForm">
            <input type="hidden" name="eId" value="aca2c394be63332b" />
            <input type="hidden" name="uuId" value="9a830620-064e-40a7-a7af-d4634560d9c9" />
            <input type="image" id="payByIpnImg" style="background-color:transparent;border:0 none;" src="https://ipn.intuit.com/images/payButton/btn_PayNow_BLU_LG.png" alt="Make payments for less with Intuit Payment Network." />
            </form>';
        } elseif($yrrrent == "449") {
	    $buttontext = '<form method="post" target="_payByIpnWindow" action="https://ipn.intuit.com/payNow/start" id="payByIpnForm">  <input type="hidden" name="eId" value="aca2c394be63332b" /> <input type="hidden" name="uuId" value="fea40a43-62e3-4b82-9673-d879ee592d11" /> <input type="image" id="payByIpnImg" style="background-color:transparent;border:0 none;" src="https://ipn.intuit.com/images/payButton/btn_PayNow_ORG_LG.png" alt="Make payments for less with Intuit Payment Network." /></form>';
	} else {
            $buttontext = '<form method="post" target="_payByIpnWindow" action="https://ipn.intuit.com/payNow/start" id="payByIpnForm">
                            <input type="hidden" name="eId" value="aca2c394be63332b" />
                                <div id="selectMultiplePriceDiv" style="display:none;">
                                    <select name="payButtonMultiplePriceCombo" style="margin-left: 3px; margin-bottom: 8px; width: 150px;">
                                        <option value=46767>Rent $299.50</option>
                                        <option value=46768>Deposit + FL $599.50</option>
                                        <option value=46769>Deposit $300.50</option>
                                        <option value=46770>Late Rent $324.50</option>
                                        <option value=46771>1 Day Late $326.50</option>
                                        <option value=46772>2 Days Late $328.50</option>
                                        <option value=46773>3 Days Late $330.50</option>
                                        <option value=46774>4 Days Late $332.50</option>
                                        <option value=46775>5 Days Late $334.50</option>
                                        <option value=46776>6 Days Late $336.50</option>
                                    </select>
                                </div>
                            <input type="hidden" name="uuId" value="1e7d7f74-5a55-481e-b042-db7b38855901" />
                            <input type="image" id="payByIpnImg" style="background-color:transparent;border:0 none;" src="https://ipn.intuit.com/images/payButton/btn_PayNow_ORG_LG.png" alt="Make payments for less with Intuit Payment Network." />
                        </form>';
        }
    }

    return $buttontext;
}