<?php

// Todo: Move this into the admin folder, check all references
// Todo: consolidate all commands for init


add_action( 'init', 'wppmanage_add_post_types' );
add_action('admin_menu', 'wppmanage_custom_menu');
add_action('init', 'wppmanage_add_role');  //TODO:  Move this call to activation of plugin

function wppmanage_add_role(){

    remove_role( 'tenant' );
    remove_role( 'apartment_manager' );

    add_role('tenant', __( 'Tenant' ),
        array(
            'read'         => true,  // true allows this capability
        )
    );

    add_role( 'apartment_manager', __( 'Apartment Manager'), array(
        'read'         => true,  // true allows this capability
        'edit_posts'   => true,
        'delete_users' => true,
        'create_users' => true,
        'list_users' => true,
        'remove_users' =>true,
        'add_users' => true,
        'promote_users' => true,
        'edit_users' => true,
        'upload_files' => true,
    ));
}

function wppmanage_custom_menu(){

    if(current_user_can('edit_users')){
        $pagetitle = 'Manage Rentals';
    } else {
        $pagetitle = 'My Apartment';
    }

    add_menu_page('Property Management Settings', $pagetitle , 'read' , 'wppmanage_menu', 'wppmanage_landing', plugin_dir_url('wppmanage') . '/wppmanage/images/properties.png','2');
    add_submenu_page( 'wppmanage_menu', 'Settings', 'Ledger', 'edit_users', 'wppmanage_ledger', 'wppmanage_ledger_page');
    add_submenu_page( 'wppmanage_menu', 'Settings', 'Task Log', 'edit_users', 'wppmanage_log', 'wppmanage_log_page');

    function wppmanage_landing(){
        include_once('overview.php' );
    }
    function wppmanage_ledger_page(){
        include_once('ledger-old.php');
    }
    function wppmanage_log_page(){
        include_once('tasklog-old.php');
    }
}

function wppmanage_add_post_types() {

    $propertyLabels = array(
        'name' => __('Properties'),
        'singular_name' => __('Property'),
        'add_new' => __('Add Property'),
        'add_new_item' => __('Add New Property'),
        'edit_item' => __('Edit Property'),
        'new_item' => __('New Property'),
        'view_item' => __('View Property'),
        'search_items' => __('Search Properties'),
        'not_found' =>  __('No Properties found'),
        'not_found_in_trash' => __('No Properties found in trash'),
        'parent_item_colon' => __('Parent Property:'),
        'menu_name' => __('Properties')
    );

    $tenantLabels = array(
        'name' => __('Tenants'),
        'singular_name' => __('Tenant'),
        'add_new' => __('Add Tenant'),
        'add_new_item' => __('Add New Tenant'),
        'edit_item' => __('Edit Tenant'),
        'new_item' => __('New Tenant'),
        'view_item' => __('View Tenant'),
        'search_items' => __('Search Tenants'),
        'not_found' =>  __('No Tenants found'),
        'not_found_in_trash' => __('No Tenants found in trash'),
        'parent_item_colon' => __('Parent Tenant:'),
        'menu_name' => __('Tenants')
    );

    $transactionLabels = array(
        'name' => __('Transactions'),
        'singular_name' => __('Transaction'),
        'add_new' => __('Record Transaction'),
        'add_new_item' => __('Record New Transaction'),
        'edit_item' => __('Change Transaction'),
        'new_item' => __('New Transaction'),
        'view_item' => __('View Transaction'),
        'search_items' => __('Search Transactions'),
        'not_found' =>  __('No Transactions found'),
        'not_found_in_trash' => __('No Transactions found in trash'),
        'parent_item_colon' => __('Parent Transaction:'),
    );

    register_post_type('Tenants', array(
        'description' => "The Tenants you manage",
        'labels' => $tenantLabels,
        'public' => false,
        'show_ui' => true,
        'show_in_menu' => false,
        'capabilities' => array(
            'publish_posts' => 'edit_users',
            'edit_posts' => 'edit_users',
            'edit_post' => 'edit_users',
            'edit_others_posts' => 'edit_users',
            'read_post' => 'edit_users',
            'read_private_posts' => 'edit_users',
            'delete_posts' => 'edit_users',
            'delete_others_posts' => 'manage_options',
            'delete_post' => 'edit_users',

        ),
        'hierarchical' => false,
        'rewrite' => array("slug" => "tenants"),
        'supports' => array('title', 'page-attributes')
    ));

    register_post_type('Properties', array(
        'description' => "The Properties you manage",
        'labels' => $propertyLabels,
        'singular_label' => __('Property'),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => false,
        'add_new' => __('Add New', 'Property'),
        'capabilities' => array(
            'publish_posts' => 'edit_users',
            'edit_posts' => 'edit_users',
            'edit_others_posts' => 'edit_users',
            'delete_posts' => 'edit_users',
            'read_private_posts' => 'edit_users',
            'edit_post' => 'edit_users',
            'delete_post' => 'edit_users',
            'read_post' => 'edit_users',
        ),
        'rewrite' => array("slug" => "properties"),
        'has_archive' => true,
        'supports' => array('title', 'page-attributes', 'thumbnail', 'editor')
    ));

    register_post_type('Transactions', array(
        'description' => "Transactions Recorded",
        'labels' => $transactionLabels,
        'singular_label' => __('Transaction'),
        'public' => false,
        'show_ui' => true,
        'show_in_menu' => false,
        'capabilities' => array(
            'publish_posts' => 'edit_users',
            'edit_posts' => 'edit_users',
            'edit_others_posts' => 'edit_users',
            'delete_posts' => 'edit_users',
            'read_private_posts' => 'edit_users',
            'edit_post' => 'edit_users',
            'delete_post' => 'edit_users',
            'read_post' => 'edit_users',
        ),
        'hierarchical' => false,
        'rewrite' => array("slug" => "transactions"),
        'supports' => array('title', 'page-attributes')
    ));

}

//////////////////////////////////////////////////////////////
// Custom Tenant Taxonomy
/////////////////////////////////////////////////////////////
//
//add_action( 'init', 'tenant_tax' );
//
//function tenant_tax() {
//
//  $labels = array(
//    'name'              => _x( 'Tenant Categories', 'taxonomy general name' ),
//    'singular_name'     => _x( 'Tenant Category', 'taxonomy singular name' ),
//    'search_items'      => __( 'Search Tenant Categories' ),
//    'all_items'         => __( 'All Tenant Categories' ),
//    'parent_item'       => __( 'Parent Tenant Category' ),
//    'parent_item_colon' => __( 'Parent Tenant Category:' ),
//    'edit_item'         => __( 'Edit Tenant Category' ),
//    'update_item'       => __( 'Update Tenant Category' ),
//    'add_new_item'      => __( 'Add New Tenant Category' ),
//    'new_item_name'     => __( 'New Tenant Category' ),
//    'menu_name'         => __( 'Tenant Categories' ),
//  );
//  $args = array(
//    'labels' => $labels,
//    'hierarchical' => true,
//  );
//
//  register_taxonomy( 'tenant_status', 'tenants', $args );
//
//}


//////////////////////////////////////////////////////////////
// Meta Box
/////////////////////////////////////////////////////////////

$prefix = "_mpp_";

$property_details = array(
    "nickname" => array(
        "type" => "textfield",
        "name" => $prefix."nickname",
        "std" => "",
        "title" => __('Complex/Short Name'),
        "description" => __('')),
    "apt" => array(
        "type" => "textfield",
        "name" => $prefix."apt",
        "std" => "",
        "title" => __('Apt # (if applicable)'),
        "description" => __('')),
    "address" => array(
        "type" => "textfield",
        "name" => $prefix."address",
        "std" => "",
        "title" => __('Street Address'),
        "description" => __('')),
    "city" => array(
        "type" => "textfield",
        "name" => $prefix."city",
        "std" => "",
        "title" => __('City'),
        "description" => __('')),
    "state" => array(
        "type" => "textfield",
        "name" => $prefix."state",
        "std" => "",
        "title" => __('State'),
        "description" => __('')),
    "zip" => array(
        "type" => "textfield",
        "name" => $prefix."zip",
        "std" => "",
        "title" => __('Zip'),
        "description" => __(''))
);

$property_overview = array(

    "capacity" => array(
        "type" => "textfield",
        "name" => $prefix."capacity",
        "std" => "",
        "title" => __('Capacity'),
        "description" => __('')),
    "pgender" => array(
        "type" => "textfield",
        "name" => $prefix."pgender",
        "std" => "",
        "title" => __('Gender (M/F)'),
        "description" => __('')),
    "roomtype" => array(
        "type" => "textfield",
        "name" => $prefix."roomtype",
        "std" => "",
        "title" => __('Room Type'),
        "description" => __('Private, Shared, etc')),
    "utilities" => array(
        "type" => "textfield",
        "name" => $prefix."utilities",
        "std" => "",
        "title" => __('Utilities Tenant Pays'),
        "description" => __('Electricity, Internet, Etc.')),
    "contractlength" => array(
        "type" => "textfield",
        "name" => $prefix."contractlength",
        "std" => "",
        "title" => __('Contract Length'),
        "description" => __('')),
    "galleryid" => array(
        "type" => "textfield",
        "name" => $prefix."galleryid",
        "std" => "",
        "title" => __('Gallery ID'),
        "description" => __(''))
);

$property_pricing = array(

    "fwrent" => array(
        "type" => "textfield",
        "name" => $prefix."fwrent",
        "std" => "",
        "title" => __('Fall/Winter Rent'),
        "description" => __('')),
    "ssrent" => array(
        "type" => "textfield",
        "name" => $prefix."ssrent",
        "std" => "",
        "title" => __('Spring/Summer Rent'),
        "description" => __('')),
    "yrrent" => array(
        "type" => "textfield",
        "name" => $prefix."yrrent",
        "std" => "",
        "title" => __('Year Round Rent'),
        "description" => __('')),
    "pdeposit" => array(
        "type" => "textfield",
        "name" => $prefix."pdeposit",
        "std" => "",
        "title" => __('Deposit'),
        "description" => __(''))
);

$property_fees = array(

    "hoa" => array(
        "type" => "textfield",
        "name" => $prefix."hoa",
        "std" => "",
        "title" => __('HOA (monthly)'),
        "description" => __('')),
    "tax" => array(
        "type" => "textfield",
        "name" => $prefix."tax",
        "std" => "",
        "title" => __('Property Tax'),
        "description" => __(''))
);

$property_ad = array(

    "showad" => array(
        "type" => "checkbox",
        "name" => $prefix."showad",
        "std" => "",
        "title" => __('Enable Ad'),
        "description" => __('')),
    "adtitle" => array(
        "type" => "textfield",
        "name" => $prefix."adtitle",
        "std" => "",
        "title" => __('Ad Title'),
        "description" => __('')),
    "adtext" => array(
        "type" => "textarea",
        "name" => $prefix."adtext",
        "std" => "",
        "title" => __('Ad Text'),
        "description" => __('')),
    "adcontact" => array(
        "type" => "textarea",
        "name" => $prefix."adcontact",
        "std" => "",
        "title" => __('Ad Contact'),
        "description" => __('')),
    "available" => array(
        "type" => "textfield",
        "name" => $prefix."available",
        "std" => "",
        "title" => __('Date Available'),
        "description" => __('If this is set, it will override contract expiration date.')),
    "openings" => array(
        "type" => "textfield",
        "name" => $prefix."openings",
        "std" => "",
        "title" => __('# of Openings'),
        "description" => __(''))

);

$repairs = array(

    "repairs" => array(
        "type" => "textarea",
        "name" => $prefix."repairs",
        "std" => "",
        "title" => __('Repairs History'),
        "description" => __(''))

);

$tenant_overview = array(

    "first" => array(
        "type" => "textfield",
        "name" => $prefix."first",
        "std" => "",
        "title" => __('First Name'),
        "description" => __('')),
    "last" => array(
        "type" => "textfield",
        "name" => $prefix."last",
        "std" => "",
        "title" => __('Last Name'),
        "description" => __('')),
    "email" => array(
        "type" => "textfield",
        "name" => $prefix."email",
        "std" => "",
        "title" => __('Email'),
        "description" => __('')),
    "phone" => array(
        "type" => "textfield",
        "name" => $prefix."phone",
        "std" => "",
        "title" => __('Phone'),
        "description" => __('')),
    "gender" => array(
        "type" => "textfield",
        "name" => $prefix."gender",
        "std" => "",
        "title" => __('Gender (M/F)'),
        "description" => __('')),

    "college" => array(
        "type" => "textfield",
        "name" => $prefix."college",
        "std" => "",
        "title" => __('College Info'),
        "description" => __('College Name, ID#, etc.'))
);

$tenant_status = array(

    "tenantstatus" => array(
        "type" => "textfield",
        "name" => $prefix."tenantstatus",
        "std" => "",
        "title" => __('Tenant Status'),
        "description" => __('Status Values: <br /> 0 - Applicant <br /> A - Approved Applicant <br /> 1 - Future Tenant <br /> 2 - Current Tenant <br /> 3 - Prior Tenant')),
);

$tenant_financials = array(

    "deposit" => array(
        "type" => "textfieldsecure",
        "name" => $prefix."depositremaining",
        "std" => "",
        "title" => __('Deposit')),
    "balance" => array(
        "type" => "textfieldsecure",
        "name" => $prefix."currentbalance",
        "std" => "",
        "title" => __('Balance')),
);

$tenant_contract = array(

    "contracturl" => array(
        "type" => "textfieldsecure",
        "name" => $prefix."contracturl",
        "std" => "",
        "title" => __('Contract URL'),
        "description" => __('(Only appears for applicants)')),
);

$tenant_login = array(

    "username" => array(
        "type" => "textfieldsecure",
        "name" => $prefix."username",
        "std" => "",
        "title" => __('Username'),
        "description" => __('')),
    "password" => array(
        "type" => "textfieldsecure",
        "name" => $prefix."password",
        "std" => "",
        "title" => __('Temp Password'),
        "description" => __('To edit these fields, use the User interface.')),
);

$tenant_ledger = array(

    "balance" => array(
        "type" => "tenantledger",
        "name" => $prefix."balance",
        "std" => "",
        "title" => __(''),
        "description" => __(''))
);


$tenant_deposit_ledger = array(

    "balance" => array(
        "type" => "tenantdepositledger",
        "name" => $prefix."depositbalance",
        "std" => "",
        "title" => __(''),
        "description" => __(''))
);

$tenant_delete_button = array(

    "balance" => array(
        "type" => "tenantdeletebutton",
        "name" => $prefix."tenantdeletebutton",
        "std" => "",
        "title" => __(''),
        "description" => __(''))
);

$tenant_add_transaction = array(

    "pbalance" => array(
        "type" => "tenantaddtransaction",
        "name" => $prefix."pbalance",
        "std" => "",
        "title" => __(''),
        "description" => __(''))
);

$contract_details = array(

    "property" => array(
        "type" => "selection",
        "name" => $prefix."property",
        "std" => "",
        "title" => __('Property'),
        "description" => __('')),
    "startdate" => array(
        "type" => "textfield",
        "name" => $prefix."startdate",
        "std" => "",
        "title" => __('Start Date'),
        "description" => __('')),
    "enddate" => array(
        "type" => "textfield",
        "name" => $prefix."enddate",
        "std" => "",
        "title" => __('End Date'),
        "description" => __('')),
    "deposit" => array(
        "type" => "textfield",
        "name" => $prefix."deposit",
        "std" => "",
        "title" => __('Deposit')),
    "depositdate" => array(
        "type" => "textfield",
        "name" => $prefix."depositdate",
        "std" => "",
        "title" => __('Deposit Date'),
        "description" => __('')),
    "lmr" => array(
        "type" => "textfield",
        "name" => $prefix."lmr",
        "std" => "",
        "title" => __('Last Months Rent'),
        "description" => __('')),
    "fwrate" => array(
        "type" => "textfield",
        "name" => $prefix."fwrate",
        "std" => "",
        "title" => __('F/W Rate'),
        "description" => __('')),
    "ssrate" => array(
        "type" => "textfield",
        "name" => $prefix."ssrate",
        "std" => "",
        "title" => __('Sp/Su Rate'),
        "description" => __('')),
    "yrrate" => array(
        "type" => "textfield",
        "name" => $prefix."yrrate",
        "std" => "",
        "title" => __('Year-Round Rate'),
        "description" => __('')),
    "totalrent" => array(
        "type" => "textfield",
        "name" => $prefix."totalrent",
        "std" => "",
        "title" => __('Total Rent'),
        "description" => __('')),
    "paymentschedule" => array(
        "type" => "textarea",
        "name" => $prefix."paymentschedule",
        "std" => "",
        "title" => __('Payment Schedule'),
        "description" => __(''))
);

$transaction_details = array(

    "transactiontype" => array(
        "type" => "textfield",
        "name" => $prefix."transactiontype",
        "std" => "0",
        "title" => __('Transaction Type'),
        "description" => __('')),
    "propertyid" => array(
        "type" => "textfield",
        "name" => $prefix."propertyid",
        "std" => "",
        "title" => __('Property ID'),
        "description" => __('')),
    "tenantid" => array(
        "type" => "textfield",
        "name" => $prefix."tenantid",
        "std" => "",
        "title" => __('Tenant ID'),
        "description" => __('')),
    "amount" => array(
        "type" => "textfield",
        "name" => $prefix."amount",
        "std" => "",
        "title" => __('Amount'),
        "description" => __('')),
    "method" => array(
        "type" => "textfield",
        "name" => $prefix."method",
        "std" => "",
        "title" => __('Method'),
        "description" => __('')),
    "details" => array(
        "type" => "textarea",
        "name" => $prefix."details",
        "std" => "",
        "title" => __('Details'),
        "description" => __('')),
    "reason" => array(
        "type" => "textarea",
        "name" => $prefix."reason",
        "std" => "",
        "title" => __('Fee Purpose/Reason'),
        "description" => __('')),
    "transactiondate" => array(
        "type" => "textfield",
        "name" => $prefix."transactiondate",
        "std" => "",
        "title" => __('Transaction Date'),
        "description" => __(''))
);

$meta_box_groups = array($tenant_contract, $tenant_delete_button, $tenant_deposit_ledger, $property_details, $property_overview, $property_pricing, $property_fees, $property_ad, $repairs, $tenant_overview, $tenant_status, $tenant_financials, $tenant_login, $contract_details, $transaction_details, $tenant_ledger, $tenant_add_transaction);

function mpp_new_meta_box($post, $metabox) {

    $meta_boxes_inputs = $metabox['args']['inputs'];

    foreach($meta_boxes_inputs as $meta_box) {

        $meta_box_value = get_post_meta($post->ID, $meta_box['name'].'_value', true);
        if($meta_box_value == "") $meta_box_value = $meta_box['std'];

        echo'<div class="meta-field">';

        echo'<input type="hidden" name="'.$meta_box['name'].'_noncename" id="'.$meta_box['name'].'_noncename" value="'.wp_create_nonce( plugin_basename(__FILE__) ).'" />';

        echo'<p><strong>'.$meta_box['title'].'</strong></p>';

        if(isset($meta_box['type']) && $meta_box['type'] == 'tenantledger') {

            echo showTenantLedger($post->ID);

        } elseif(isset($meta_box['type']) && $meta_box['type'] == 'tenantdepositledger') {

            echo showDepositLedger($post->ID);
            echo '<input type="hidden" id="location" name="location" value="tenantpage">';

        } elseif(isset($meta_box['type']) && $meta_box['type'] == 'tenantaddtransaction') {


            $user = get_the_title();
            $selected0 = '';
            $selected1 = '';
            $selected2 = '';
            $selected3 = '';
            $selected4 = '';

            switch($meta_box_value){
                case '0':
                    $selected0 = 'selected';
                    break;
                case '1':
                    $selected1 = 'selected';
                    break;
                case '2':
                    $selected2 = 'selected';
                    break;
                case '3':
                    $selected3 = 'selected';
                    break;
                case '4':
                    $selected4 = 'selected';
                    break;
            }

            echo '<script type="text/javascript">
                    function isNumberKey(evt, obj) {

                        var charCode = (evt.which) ? evt.which : event.keyCode
                        var value = obj.value;
                        if (charCode == 45 && value== "") return true;
                        var dotcontains = value.indexOf(".") != -1;
                        if (dotcontains)
                            if (charCode == 46) return false;
                        if (charCode == 46) return true;
                        if (charCode > 31 && (charCode < 48 || charCode > 57))
                            return false;
                        return true;
                        }
                    </script>';

            echo '<input type="hidden" id="tenant_name" name="tenant_name" value="'.$user.'" />';
            echo '<label for="transactiondate">Date </label><input style="width:60%; float:right;" type="text" name="transactiondate" id="transactiondate" value="'. date('m/d/Y') .'" /><br />';
            echo '<div style="display:none;" class="" id="editid"></div>';
            echo '<div style="clear:both"></div>';
            echo '<label for="transactionamount">Amount </label><input style="width:60%; float:right;" type="text" class="dollarinput" name="transactionamount" id="transactionamount"  onKeyPress="return false;" /><br />';
            echo '<div style="clear:both"></div>';
            echo '<label for="transactiontype">Type </label>';
            echo '<select name="transactiontype" id="transactiontype" style="float:right;">';
            echo '<option value="" '. $selected0 .'>Please Select </option>';
            echo '<option value="deposit" '. $selected1 .'>Deposit </option>';
            echo '<option value="fee" '. $selected2 .'>Fee Due </option>';
            echo '<option value="rent" '. $selected3 .'>Rent Due </option>';
            echo '<option value="payment" '. $selected4 .'>Payment</option>';
            echo '</select>';
            echo '<div style="clear:both"></div>';
            echo '<span id="paymethod" style="display:none;"><label for="transactionmethod">Pay Method </label>';
            echo '<select name="transactionmethod" id="transactionmethod" style="float:right;">';
            echo '<option value="" '. $selected0 .'>Please Select </option>';
            echo '<option value="Paypal" '. $selected1 .'>Paypal</option>';
            echo '<option value="Intuit" '. $selected2 .'>Intuit</option>';
            echo '<option value="Check" '. $selected3 .'>Check</option>';
            echo '<option value="Other" '. $selected4 .'>Other</option>';
            echo '</select>';
            echo '<div style="clear:both"></div></span>';
            echo '<label for="transactiondescription">Description </label><br /><textarea rows="4" style="width:100%;" name="transactiondescription" id="transactiondescription" /></textarea><br />';
            echo '<button type="button" id="recordtransactionbutton">Record Transaction</button><span id="transactionstatus" class="transactionstatus"></span>';

        } elseif(isset($meta_box['type']) && $meta_box['type'] == 'checkbox') {

            if($meta_box_value == 'true') {
                $checked = "checked=\"checked\"";
            } elseif($meta_box['std'] == "true") {
                $checked = "checked=\"checked\"";
            } else {
                $checked = "";
            }

            echo'<p class="clearfix"><input type="checkbox" class="meta-radio" name="'.$meta_box['name'].'_value" id="'.$meta_box['name'].'_value" value="true" '.$checked.' /> ';
            echo'<label for="'.$meta_box['name'].'_value">'.$meta_box['description'].'</label></p>';

        } elseif(isset($meta_box['type']) && $meta_box['type'] == 'textarea')  {

            echo'<textarea rows="4" style="width:98%" name="'.$meta_box['name'].'_value" id="'.$meta_box['name'].'_value">'.$meta_box_value.'</textarea><br />';
            echo'<p><label for="'.$meta_box['name'].'_value">'.$meta_box['description'].'</label></p>';

        } elseif(isset($meta_box['type']) && $meta_box['type'] == 'textfieldsecure')  {

            $disabled = 'readonly';

            echo'<input style="width:70%"type="text" name="'.$meta_box['name'].'_value" id="'.$meta_box['name'].'_value" value="'.$meta_box_value.'" '.$disabled.'/><br />';
            echo'<p><label for="'.$meta_box['name'].'_value">'.$meta_box['description'].'</label></p>';

        } elseif(isset($meta_box['type']) && $meta_box['type'] == 'tenantdeletebutton')  {
            echo '<button style="background-color:red; color:#ffffff;" ><a href="" id="'.$post->ID.'" class="deletetenant" style="text-decoration:none; color:#ffffff;" type="button">DELETE TENANT</a></button><br/>';
            if(get_post_meta($post->ID, '_mpp_tenantstatus_value', true) == 'A'){
                echo '<button style="background-color:yellow; color:#ffffff;" ><a href="" id="'.$post->ID.'" class="unapprovetenant" style="text-decoration:none; color:#000;" type="button">UNAPPROVE TENANT</a></button>';
            } else {
                echo '<button style="background-color:green; color:#ffffff;" ><a href="" id="'.$post->ID.'" class="approvetenant" style="text-decoration:none; color:#ffffff;" type="button">APPROVE TENANT</a></button>';
            }
        } elseif(isset($meta_box['type']) && $meta_box['type'] == 'selection')  {

            $args = array('post_type' => 'properties', 'posts_per_page' => -1);
            $properties = get_posts($args);


            echo '<select name="'.$meta_box['name'].'_value">';
            echo '<option value="0">Select/No Property</option>';

            foreach($properties as $property){
                $selected = '';
                if($meta_box_value == $property->ID){
                    $selected = 'selected';
                }
                echo '<option value='.$property->ID.' '.$selected.'>'.get_the_title($property->ID).'</option>';
            }

            echo '</select>';

        } else {
            echo'<input style="width:70%"type="text" name="'.$meta_box['name'].'_value" id="'.$meta_box['name'].'_value" value="'.$meta_box_value.'" /><br />';
            echo'<p><label for="'.$meta_box['name'].'_value">'.$meta_box['description'].'</label></p>';
        }

        echo'</div>';

    } // end foreach

    echo'<br style="clear:both" />';

} // end meta boxes

function mpp_create_meta_box() {
    global $tenant_contract, $tenant_delete_button, $tenant_deposit_ledger, $property_details, $property_overview, $property_pricing, $property_fees, $property_ad, $repairs, $tenant_overview, $tenant_status, $tenant_financials, $tenant_login, $contract_details, $transaction_details, $tenant_ledger, $tenant_add_transaction;

    if ( function_exists('add_meta_box') ) {
        add_meta_box( 'new-meta-boxes-properties', __('Property Information'), 'mpp_new_meta_box', 'properties', 'normal', 'high', array('inputs'=>$property_details) );
        add_meta_box( 'new-meta-boxes-property-ad', __('Property Ad'), 'mpp_new_meta_box', 'properties', 'normal', 'high', array('inputs'=>$property_ad) );

        add_meta_box( 'new-meta-boxes-repairs-history', __('Repairs History'), 'mpp_new_meta_box', 'properties', 'normal', 'low', array('inputs'=>$repairs) );

        add_meta_box( 'new-meta-boxes-property-overview', __('Property Overview'), 'mpp_new_meta_box', 'properties', 'side', 'low', array('inputs'=>$property_overview) );
        add_meta_box( 'new-meta-boxes-property-fees', __('Property Fees'), 'mpp_new_meta_box', 'properties', 'side', 'low', array('inputs'=>$property_fees) );
        add_meta_box( 'new-meta-boxes-property-pricing', __('Property Pricing'), 'mpp_new_meta_box', 'properties', 'side', 'low', array('inputs'=>$property_pricing) );
        add_meta_box( 'new-meta-boxes-property-transaction', __('New Transaction'), 'mpp_new_meta_box', 'properties', 'side', 'high', array('inputs'=>$tenant_add_transaction) );

        add_meta_box( 'new-meta-boxes-tenant-transaction', __('Record Transaction'), 'mpp_new_meta_box', 'tenants', 'side', 'high', array('inputs'=>$tenant_add_transaction) );
        add_meta_box( 'new-meta-boxes-tenant-contract', __('Tenant Contract'), 'mpp_new_meta_box', 'tenants', 'side', 'high', array('inputs'=>$tenant_contract) );
        add_meta_box( 'new-meta-boxes-tenant-financials', __('Tenant Financials'), 'mpp_new_meta_box', 'tenants', 'side', 'high', array('inputs'=>$tenant_financials) );
        add_meta_box( 'new-meta-boxes-tenant-status', __('Tenant Status'), 'mpp_new_meta_box', 'tenants', 'side', 'low', array('inputs'=>$tenant_status) );

        add_meta_box( 'new-meta-boxes-tenant-details', __('Tenant Details'), 'mpp_new_meta_box', 'tenants', 'normal', 'high', array('inputs'=>$tenant_overview) );
        add_meta_box( 'new-meta-boxes-contract-details', __('Contract Details'), 'mpp_new_meta_box', 'tenants', 'normal', 'high', array('inputs'=>$contract_details) );
        add_meta_box( 'new-meta-boxes-tenant-ledger', __('Tenant Ledger'), 'mpp_new_meta_box', 'tenants', 'normal', 'high', array('inputs'=>$tenant_ledger) );
        add_meta_box( 'new-meta-boxes-tenant-deposit-ledger', __('Tenant Deposit Ledger'), 'mpp_new_meta_box', 'tenants', 'normal', 'high', array('inputs'=>$tenant_deposit_ledger) );
        add_meta_box( 'new-meta-boxes-tenant-delete-button', __('Tenant Actions'), 'mpp_new_meta_box', 'tenants', 'side', 'low', array('inputs'=>$tenant_delete_button) );
        add_meta_box( 'new-meta-boxes-tenant-login', __('Tenant Login'), 'mpp_new_meta_box', 'tenants', 'normal', 'low', array('inputs'=>$tenant_login) );


        add_meta_box( 'new-meta-boxes-transactions-details', __('Transaction Details'), 'mpp_new_meta_box', 'transactions', 'normal', 'high', array('inputs'=>$transaction_details) );

    }
}

function mpp_save_postdata( $post_id ) {
    global $post, $new_meta_boxes, $meta_box_groups;

    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) {
        return $post_id;
    }

    if( defined('DOING_AJAX') && DOING_AJAX ) { //Prevents the metaboxes from being overwritten while quick editing.
        return $post_id;
    }

    if( ereg('/\edit\.php', $_SERVER['REQUEST_URI']) ) { //Detects if the save action is coming from a quick edit/batch edit.
        return $post_id;
    }

    foreach($meta_box_groups as $group) {
        foreach($group as $meta_box) {

            if($meta_box['type'] == 'textfieldsecure'){
                break;
            }

            // Verify
            if(isset($_POST[$meta_box['name'].'_noncename'])){
                if ( !wp_verify_nonce( $_POST[$meta_box['name'].'_noncename'], plugin_basename(__FILE__) )) {
                    return $post_id;
                }
            }

            if ( isset($_POST['post_type']) && 'page' == $_POST['post_type'] ) {
                if ( !current_user_can( 'edit_page', $post_id ))
                    return $post_id;
            } else {
                if ( !current_user_can( 'edit_post', $post_id ))
                    return $post_id;
            }

            $data = "";
            if(isset($_POST[$meta_box['name'].'_value'])){
                $data = $_POST[$meta_box['name'].'_value'];
            }


            if(get_post_meta($post_id, $meta_box['name'].'_value') == "")
                add_post_meta($post_id, $meta_box['name'].'_value', $data, true);
            elseif($data != get_post_meta($post_id, $meta_box['name'].'_value', true))
                update_post_meta($post_id, $meta_box['name'].'_value', $data);
            elseif($data == "" || $data == $meta_box['std'] )
                delete_post_meta($post_id, $meta_box['name'].'_value', get_post_meta($post_id, $meta_box['name'].'_value', true));

        } // end foreach
    } // end foreach
} // end mpp_save_postdata

add_action('admin_menu', 'mpp_create_meta_box');
add_action('save_post', 'mpp_save_postdata'); 

add_filter( 'manage_edit-tenants_columns', 'wppm_edit_tenants_columns' ) ;
add_filter( 'manage_edit-properties_columns', 'wppm_edit_properties_columns' ) ;
add_filter( 'manage_edit-transactions_columns', 'wppm_edit_transactions_columns' ) ;


add_action('admin_head', 'tenant_column_width');

function tenant_column_width() {
    echo '<style type="text/css">';
    echo '.column-title {width:210px; }';
    echo '.column-property {width:74px; }';
    echo '.column-school {min-width:46px; }';
    echo '.column-phone {width:100px; }';
    echo '.column-progress { width:495px; }';
    echo '.column-email { width:210px; }';
    echo '.column-sex { width:30px;}';

    echo '.column-rate { width:50px;}';
    echo '.column-deposit { width:50px;}';
    echo '.column-left { width:50px;}';
    echo '.column-due { width:50px;}';
    echo '</style>';
}

function wppm_edit_tenants_columns( $columns ) {

    $columns = array(
        'cb' => '<input type="checkbox" />',
        'title' => __( 'Name' ),
        'property' => __( 'Property' ),
        'sex' => __( 'Sex'),
        'phone' => __(  'Phone'  ),
        'email' => __(  'Email'  ),
        'school' => __( 'School'),
        'movein' => __(  'In' ),
        'moveout' => __(  'End' ),
        'rate' => __(  'Rate' ),
        'deposit' => __(  'Dep' ),
        'left' => __(  'Left' ),
        'due' => __(  'Due' )
,	    'progress' => __( 'Progress' )
    );
    return $columns;
}

function wppm_edit_properties_columns( $columns ) {

    $columns = array(
        'cb' => '<input type="checkbox" />',
        'title' => __( 'Apartment' ),
        'address' => __( 'Address' ),
        'capacity' => __(  'Cap' ),
        'rent' => __(  'Rate' )
    );
    return $columns;
}

function wppm_edit_transactions_columns( $columns ) {
    $columns = array(
        'cb' => '<input type="checkbox" />',
        'title' => __( 'Tenant' ),
        'amount' => __( 'Amount' ),
        'type' => __(  'Type' ),
        'description' => __(  'Description' )
    );
    return $columns;
}

add_action( 'manage_tenants_posts_custom_column', 'wppm_manage_tenants_columns', 10, 2 );
add_action( 'manage_properties_posts_custom_column', 'wppm_manage_properties_columns', 10, 2 );
add_action( 'manage_transactions_posts_custom_column', 'wppm_manage_transactions_columns', 10, 2 );

function wppm_manage_tenants_columns( $column, $post_id ) {
    global $post;

    switch( $column ) {

        case 'property' :

            $propid = get_post_meta( $post_id, '_mpp_property_value', true );
            $propertyname = get_the_title($propid);

            if ( empty( $propertyname ) )
                echo __( 'Unknown' );

            else
                echo $propertyname;

            break;
        case 'sex' :
            $gender = get_post_meta( $post_id, '_mpp_gender_value', true );

            echo $gender;
            break;
        case 'school' :
            $school = get_post_meta( $post_id, '_mpp_college_value', true );

            echo $school;
            break;

        case 'email' :
            $email = get_post_meta( $post_id, '_mpp_email_value', true );

            echo $email;

            break;
        case 'phone' :
            $phone = get_post_meta( $post_id, '_mpp_phone_value', true );

            echo $phone;

            break;
		
        case 'moveout' :

            $moveout = get_post_meta( $post_id, '_mpp_enddate_value', true );


            if ( empty( $moveout ) )
                $moveout = '0';

            echo $moveout;

            break;

        case 'movein' :

            $movein = get_post_meta( $post_id, '_mpp_startdate_value', true );


            if ( empty( $movein ) )
                $movein = '0';

            echo $movein;

            break;

        case 'rate' :

            $tenant_values = get_post_meta( $post_id);

            if($tenant_values['_mpp_yrrate_value'][0]){
                $rent = $tenant_values['_mpp_yrrate_value'][0];
            } else {

                if(date('d') < 6){
                    $month = date('m');
                } else {
                    $month = date('m', strtotime('+1 month'));
                }

                if(($month > 0 && $month < 5) || ($month > 8)){
                    $rent = $tenant_values['_mpp_fwrate_value'][0];
                } else {
                    $rent = $tenant_values['_mpp_ssrate_value'][0];
                }
            }

            if(!$rent){
                $rent = 0;
            }

            echo $rent;

            break;
        case 'deposit':
            echo get_post_meta( $post_id, '_mpp_deposit_value', true);
            break;
        case 'left':

            $args = array(
                'post_type' => 'transactions',
                'post_status' => 'publish',
                'posts_per_page' => -1,
                'meta_query' => array(
                    'relation' => 'AND',
                    array(
                        'key' => '_mpp_transactiontype_value',
                        'value' => 'deposit'
                    ),
                    array(
                        'key' => '_mpp_tenantid_value',
                        'value' => $post_id
                    )
                ),
            );
            $deductions = get_posts($args);
            $sum = 0;
            foreach($deductions as $deduction){
                $amount = get_post_meta($deduction->ID, '_mpp_amount_value', true);
                $sum += $amount;
            }

            $left = $sum;

            if($left < 50){
                $showstring = '<span style="color:red;"><b>'.$left.'</b></span>';
            } else {
                $showstring = '<span style="color:green;">'.$left.'</span>';
            }
            echo $showstring;

            break;
        case 'due':

            $args = array(
                'post_type' => 'transactions',
                'post_status' => 'publish',
                'posts_per_page' => -1,
                'meta_query' => array(
                    'relation' => 'AND',
                    array(
                        'key' => '_mpp_transactiontype_value',
                        'value' => array('fee', 'rent'),
                    ),
                    array(
                        'key' => '_mpp_tenantid_value',
                        'value' => $post_id
                    )
                ),
            );

            $args2 = array(
                'post_type' => 'transactions',
                'post_status' => 'publish',
                'posts_per_page' => -1,
                'meta_query' => array(
                    'relation' => 'AND',
                    array(
                        'key' => '_mpp_transactiontype_value',
                        'value' => 'payment'
                    ),
                    array(
                        'key' => '_mpp_tenantid_value',
                        'value' => $post_id
                    )
                ),
            );

            $fees = get_posts($args);
            $payments = get_posts($args2);

            $feesum = 0;
            $pmtsum = 0;

            foreach($fees as $fee){
                $amount = get_post_meta($fee->ID, '_mpp_amount_value', true);
                $feesum += $amount;
            }

            foreach($payments as $pmt){
                $amount = get_post_meta($pmt->ID, '_mpp_amount_value', true);
                $pmtsum += $amount;
            }

            $balance = $feesum - $pmtsum;

            if($balance > 0){
                $showstring = '<span style="color:red;"><b>'.$balance.'</b></span>';
            } else {
               $showstring = '<span style="color:green;">'.$balance.'</span>';
            }
            echo $showstring;

            break;
	case 'progress' :

        $application = get_post_meta( $post_id, 'application', true );
        $toured = get_post_meta( $post_id, 'tour', true );
        $signed = get_post_meta( $post_id, 'contract_signed', true );
        $sentcopy = get_post_meta( $post_id, 'contract_sent', true );
        $deposit = get_post_meta( $post_id, 'deposit_paid', true );
        $fal = get_post_meta( $post_id, 'first_&_last_paid', true );
        $uploadcontract = get_post_meta( $post_id, 'signed_contract', true );
        $drivers = get_post_meta( $post_id, 'drivers_license_copy', true );
        $ssn = get_post_meta( $post_id, 'social_security_number', true );
        $movedin = get_post_meta( $post_id, 'moved_in', true );


        echo '<table><tr>';
	    echo '<td>APP <input disabled type="checkbox" ';
		if($application == '1'){
            echo 'checked';
        }
        echo ' /></td>';

        echo '<td>TOUR <input disabled type="checkbox" '; 
        if($toured == '1'){
            echo 'checked';
        }
        echo ' /></td>';

        echo '<td>SIGN <input disabled type="checkbox" ';
        if($signed == '1'){
            echo 'checked';
        }
        echo ' /></td>';

        echo '<td>SENT <input disabled type="checkbox" ';
        if($sentcopy == '1'){
            echo 'checked';
        }
        echo ' /></td>';

        echo '<td>DEP <input disabled type="checkbox" ';
        if($deposit == '1'){
            echo 'checked';
        }
        echo ' /></td>';

        echo '<td>F&L <input disabled type="checkbox" ';
        if($fal == '1'){
            echo 'checked';
        }
        echo ' /></td>';

     	echo '<td>IN <input disabled type="checkbox" ';
        if($movedin){
            echo 'checked';
        }
        echo ' /></td>';

        echo '<td>UPCON <input disabled type="checkbox" ';
        if($uploadcontract){
            echo 'checked';
        }
        echo ' /></td>';

        echo '<td>UPDL <input disabled type="checkbox" ';
        if($drivers){
            echo 'checked';
        }
        echo ' /></td>';

        echo '<td>SSN <input disabled type="checkbox" ';
        if($ssn){
            echo 'checked';
        }
        echo ' /></td>';
        echo '</tr></table>';

	    break;
    default :
            break;
    }
}

function wppm_manage_properties_columns( $column, $post_id ) {
    global $post;

    switch( $column ) {

        case 'address' :

            /* Get the post meta. */
            $address = get_post_meta( $post_id, '_mpp_address_value', true );

            if ( empty( $address ) )
                echo __( 'Unknown' );

            else
                echo $address;

            break;

        case 'capacity' :


            $args = array(
                'meta_query' => array(
                    array(
                        'key' => '_mpp_property_value',
                        'value' => $post_id
                    )
                ),
                'post_type' => 'tenants'
            );
            $tenants = get_posts($args);
            $contracts = count($tenants);

            $capacity = get_post_meta( $post_id, '_mpp_capacity_value', true );


            if ( empty( $capacity ) )
                $capactiy = '0';
            if ( empty($contracts ) )
                $contracts = '0';


            echo $contracts .' / '. $capacity;

            break;

        case 'rent' :

            $rent = get_post_meta( $post_id, '_mpp_yrrent_value', true );

            if ( empty( $rent ) )
                echo __( '0' );

            else
                echo $rent;

            break;

        default :
            break;
    }
}

function wppm_manage_transactions_columns( $column, $post_id ) {
    global $post;

    switch( $column ) {

        case 'amount' :

            /* Get the post meta. */
            $amount = get_post_meta( $post_id, '_mpp_amount_value', true );

            if ( empty( $amount ) )
                echo __( '--' );

            else
                echo $amount;

            break;

        case 'type' :

            $type = get_post_meta( $post_id, '_mpp_transactiontype_value', true );

            if ( empty( $type ) )
                echo __( '--' );

            else
                echo $type;

            break;

        case 'description' :

            $desc = get_post_meta( $post_id, '_mpp_reason_value', true );

            if ( empty( $desc ) )
                echo __( '--' );

            else
                echo $desc;

            break;

        default :
            break;
    }
}

?>
