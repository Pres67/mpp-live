<?php

// Todo: Move this into the admin folder, check all references

    function showProgress(){

        $application = get_post_meta( $post_id, 'application', true );
        $toured = get_post_meta( $post_id, 'tour', true );
        $signed = get_post_meta( $post_id, 'contract_signed', true );
        $sentcopy = get_post_meta( $post_id, 'contract_sent', true );
        $deposit = get_post_meta( $post_id, 'deposit_paid', true );
        $fal = get_post_meta( $post_id, 'first_&_last_paid', true );
        $uploadcontract = get_post_meta( $post_id, 'signed_contract', true );
        $drivers = get_post_meta( $post_id, 'drivers_license_copy', true );
        $ssn = get_post_meta( $post_id, 'social_security_number', true );
        $movedin = get_post_meta( $post_id, 'moved_in', true );

        echo '<table><tr>';
                echo '<td>APP <input disabled type="checkbox" ';
                    if($application == '1'){
                    echo 'checked';
                    }
                    echo ' /></td>';

                echo '<td>TOUR <input disabled type="checkbox" ';
                    if($toured == '1'){
                    echo 'checked';
                    }
                    echo ' /></td>';

                echo '<td>SIGN <input disabled type="checkbox" ';
                    if($signed == '1'){
                    echo 'checked';
                    }
                    echo ' /></td>';

                echo '<td>SENT <input disabled type="checkbox" ';
                    if($sentcopy == '1'){
                    echo 'checked';
                    }
                    echo ' /></td>';

                echo '<td>DEP <input disabled type="checkbox" ';
                    if($deposit == '1'){
                    echo 'checked';
                    }
                    echo ' /></td>';

                echo '<td>F&L <input disabled type="checkbox" ';
                    if($fal == '1'){
                    echo 'checked';
                    }
                    echo ' /></td>';

                echo '<td>IN <input disabled type="checkbox" ';
                    if($movedin){
                    echo 'checked';
                    }
                    echo ' /></td>';

                echo '<td>UPCON <input disabled type="checkbox" ';
                    if($uploadcontract){
                    echo 'checked';
                    }
                    echo ' /></td>';

                echo '<td>UPDL <input disabled type="checkbox" ';
                    if($drivers){
                    echo 'checked';
                    }
                    echo ' /></td>';

                echo '<td>SSN <input disabled type="checkbox" ';
                    if($ssn){
                    echo 'checked';
                    }
                    echo ' /></td>';
                echo '</tr></table>';

    }

    function get_deposit_balance($id, $num){

        //TODO:  move this is going to change to a transaction type
        $dep = 0;

        $args = array(
            'post_type' => 'transactions',
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'meta_query' => array(
                'relation' => 'AND',
                array(
                    'key' => '_mpp_transactiontype_value',
                    'value' => 'deposit'
                ),
                array(
                    'key' => '_mpp_tenantid_value',
                    'value' => $id
                )
            ),
        );
        $deductions = get_posts($args);
        $sum = 0;
        foreach($deductions as $deduction){
            $amount = get_post_meta($deduction->ID, '_mpp_amount_value', true);
            $sum = $sum + $amount;
        }

        $left = number_format($sum,2,".","");

        if($num == 'html'){
            if($left < 50){
                $showstring = '<span style="color:red;"><b>'.$left.'</b></span>';
            } else {
                $showstring = '<span style="color:green;">'.$left.'</span>';
            }
        } else {
            $showstring = $left;
        }
        return $showstring;
    }

    function get_balance($id, $num){

        $args = array(
            'post_type' => 'transactions',
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'meta_query' => array(
                'relation' => 'AND',
                array(
                    'key' => '_mpp_transactiontype_value',
                    'value' => array('fee', 'rent'),
                ),
                array(
                    'key' => '_mpp_tenantid_value',
                    'value' => $id
                )
            ),
        );

        $args2 = array(
            'post_type' => 'transactions',
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'meta_query' => array(
                'relation' => 'AND',
                array(
                    'key' => '_mpp_transactiontype_value',
                    'value' => 'payment'
                ),
                array(
                    'key' => '_mpp_tenantid_value',
                    'value' => $id
                )
            ),
        );

        $fees = get_posts($args);
        $payments = get_posts($args2);

        $feesum = 0;
        $pmtsum = 0;

        foreach($fees as $fee){
            $amount = get_post_meta($fee->ID, '_mpp_amount_value', true);
            $feesum += $amount;
        }

        foreach($payments as $pmt){
            $amount = get_post_meta($pmt->ID, '_mpp_amount_value', true);
            $pmtsum += $amount;
        }

        $balance = number_format($feesum - $pmtsum,2,".","");


        if($balance > 0){
            $showstring = '<span style="color:red;"><b>'.$balance.'</b></span>';
        } else {
            $showstring = '<span style="color:green;">'.$balance.'</span>';
        }
        if($num != 'html'){
            $showstring = $balance;
        }
        return $showstring;

    }

    function get_rental_rate($id){

        $tenant_meta = get_post_meta($id);

        if($tenant_meta['_mpp_yrrate_value'][0]){
            $rent = $tenant_meta['_mpp_yrrate_value'][0];
        } else {

            if(date('d') < 6){
                $month = date('m');
            } else {
                $month = date('m', strtotime('+1 month'));
            }

            if(($month > 0 && $month < 5) || ($month > 8)){
                $rent = $tenant_meta['_mpp_fwrate_value'][0];
            } else {
                $rent = $tenant_meta['_mpp_ssrate_value'][0];
            }
        }

        if(!$rent){
            $rent = 0;
        }

        return $rent;
    }

    function updateLog($tenant, $task, $message){
        $tenant = is_numeric($tenant)? get_the_title($tenant) : $tenant;
        $dir = plugin_dir_path( __FILE__ );
        $file = $dir . '/cronlog.txt';
        $txt =   '<tr>
                    <th scope="row">'. date('m/d/Y h:i:s a', time()) .'</th>
                    <td>'.$tenant.'</td>
                    <td>'.$task.'</td>
                    <td>'.$message.'</td>
                    </tr>';
        file_put_contents($file, $txt, FILE_APPEND);
    }

    function _mpp_activation() {
        wp_schedule_event(1446879600, 'daily', '_mpp_add_rent');
        wp_schedule_event(1446879600, 'daily', '_mpp_add_late_fees');
    }

    function _mpp_deactivation() {

        $dir = plugin_dir_path( __FILE__ );
        $file = $dir . '/includes/cronlog.txt';
        file_put_contents($file, ' ');
        wp_clear_scheduled_hook( '_mpp_add_rent' );
        wp_clear_scheduled_hook( '_mpp_add_late_fees' );

        remove_role( 'tenant' );
        remove_role( 'apartment_manager' );
    }

    function _mpp_check_fees() {
        $date = date('d');
        $args = array(
            'posts_per_page' => -1,
            'meta_query' => array(
                array(
                    'key' => '_mpp_tenantstatus_value',
                    'value' => '2',
                ),
            ),
            'post_type' => 'tenants',
            'post_status' => 'publish'
        );
        $tenants = get_posts($args);

        foreach($tenants as $tenant){

            $threshold = get_post_meta($tenant->ID, '_mpp_yrrate_value', true);

            if($date > 19 || $date < 6) {
                $threshold = $threshold * 2;
            }
            $owed = checkTenantBalance($tenant->ID);
            if($owed >= $threshold){
                $added = addFees($date, $tenant->ID);
                if(!empty($added)){
                    updateLog($tenant->ID, 'add_late_fees', $added);
                }
            }
        }
    }

    function _mpp_check_rent() {
        date_default_timezone_set('America/Denver');
        $date = date('d');

        if ('20' == $date) {
            addRent();
            updateLog('All Tenants', 'add_late_fees', 'Rents Added');
        }
    }

    function mpp_redirect() {
        global $wp;
        $plugindir = dirname( __FILE__ );

        //A Specific Custom Post Type
        if ($wp->query_vars["post_type"] == 'properties') {
            $templatefilename = '../templates/archive-properties.php';
            if (file_exists(TEMPLATEPATH . '/' . $templatefilename)) {
                $return_template = TEMPLATEPATH . '/' . $templatefilename;
            } else {
                $return_template = $plugindir . '/' . $templatefilename;
            }
            do_theme_redirect($return_template);
        }
    }

    function do_theme_redirect($url) {
        global $post, $wp_query;
        if (have_posts()) {
            include($url);
            die();
        } else {
            $wp_query->is_404 = true;
        }
    }

    function wppm_load_scripts() {

        wp_register_script( 'wppm_caret_js', plugins_url() . '/wppmanage/includes/js/jquery.caret.js' );
        wp_enqueue_script( 'wppm_caret_js' );

        wp_register_script( 'wppm_isotope_js', plugins_url() . '/wppmanage/includes/js/isotope/jquery.isotope.min.js' );
        wp_enqueue_script( 'wppm_isotope_js' );

        wp_register_script( 'wppm_js', plugins_url() . '/wppmanage/includes/js/wppm.js' );
        wp_enqueue_script( 'wppm_js' );

        wp_register_script( 'wppm_custom_js', plugins_url() . '/wppmanage/includes/js/transactions.js' );
        wp_enqueue_script( 'wppm_custom_js' );

        wp_register_script( 'wppm_sortable', plugins_url() . '/wppmanage/includes/js/sortable.js' );
        wp_enqueue_script( 'wppm_sortable' );

        wp_register_style( 'wppm_custom_css', plugins_url() . '/wppmanage/includes/css/styles.css' );
        wp_enqueue_style( 'wppm_custom_css' );

        wp_enqueue_script('jquery-ui-datepicker');
        wp_enqueue_style( 'jquery-datepicker', '//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css', true );
    }

    function mpp_enqueue_script() {
        wp_enqueue_style( 'core',  plugins_url() . '/wppmanage/includes/css/properties.css', false );
    }

    function wppm_admin_bar_custom( $wp_admin_bar ) {

        if(!current_user_can('edit_users')){

//            $wp_admin_bar->remove_menu('edit-profile');
//            $wp_admin_bar->remove_menu('my-account');

            $user_id = get_current_user_id();
            $current_user = wp_get_current_user();

            $my_account=$wp_admin_bar->get_node('my-account');
            $newtitle = str_replace( 'Howdy,', '', $my_account->title );
            $wp_admin_bar->add_node( array(
                'id' => 'my-account',
                'title' => $newtitle,
            ) );

//            if ( 0 != $user_id ) {
//                $howdy = sprintf( __('%1$s'), $current_user->display_name );
//
//                $wp_admin_bar->add_menu( array(
//                    'id'    => 'wppm_logout',
//                    'parent' => 'top-secondary',
//                    'title' => 'X',
//                    'href'  => wp_logout_url( $redirect )
//                ) );
//
//                $wp_admin_bar->add_menu( array(
//                    'id' => 'wppm-menu',
//                    'parent' => 'top-secondary',
//                    'title' => $howdy,
//                    'href' => ''
//                ) );
//            }
        }
    }

    function switch_to_url2( WP_User $user, $tenant ) {
        return wp_nonce_url( add_query_arg( array(
            'action'  => 'switch_to_user',
            'user_id' => $tenant,
        ), wp_login_url() ), "switch_to_user_{$tenant}" );
    }

    function admin_theme_style() {
        if(current_user_can('edit_users')){
//            wp_enqueue_style('mpp-admin-theme', plugins_url('/includes/css/mpp-admin.css', __FILE__));
        }
    }

    function mycontext_remove_help($old_help, $screen_id, $screen){
        $screen->remove_help_tabs();
        return $old_help;
    }

    function new_mail_from($old) {
        return 'website@myprovoproperty.com';
    }

    function new_mail_from_name($old) {
        return 'My Provo Property';
    }

    function custom_login_logo() {
        echo '<style type="text/css">
        h1 a { background-image: url("http://www.myprovoproperty.com/public/mppwhite.png") !important; }
        </style>';
    }

    function my_login_stylesheet() { ?>
        <link rel="stylesheet" id="custom_wp_admin_css"  href="<?php echo plugins_url() . '/wppmanage/includes/css/style-login.css'; ?>" type="text/css" media="all" />
    <?php }

    function wppm_vacancies_page(){
        //TODO: Remove or clean up this reference to auto adding a vacancies page

        if(!get_page_by_title('Vacancies')){

            $my_page = array(
                'post_title' => 'Vacancies',
                'post_content' => '',
                'post_status' => 'publish',
                'post_type' => 'page',
                'post_name' => 'wppm_vacancies'
            );
            $post_id = wp_insert_post($my_page);
        }
    }

    function go_home(){
        wp_redirect( home_url() );
        exit();
}

    function wppm_page_template( $page_template )
    {
        //TODO: cleanup or remove this function to give a specific template to a specific page
        if ( is_page( 'wppm_vacancies' ) ) {
            $page_template = dirname( __FILE__ ) . '/includes/vacancies.php';
        }
        return $page_template;
    }

    function create_user_after_application( $contact_form ) {

        $title = $contact_form->title;
        $posted_data = $contact_form->posted_data;

        if ( 'Rental Application' == $title ) {

            $submission = WPCF7_Submission::get_instance();
            if ( $submission ) {
                $posted_data = $submission->get_posted_data();
                $first = $posted_data['first-name'];
                $last = $posted_data['last-name'];
                $email = $posted_data['email'];
                $phone = $posted_data['cell-phone'];
                $studentid = $posted_data['studentid'];
                $property = $posted_data['property'];
            }

            $title = $first . ' ' . $last;
            $username = strtolower($first[0] . $last);

            $i = 1;
            while(username_exists( $username )){
                $username = $username . $i;
                $i++;
            }

            function unique_id($l = 8) {
                return substr(md5(uniqid(mt_rand(), true)), 0, $l);
            }

            $password = unique_id();
            $post = array(
                'post_title'     => $title,
                'post_status'    => 'publish',
                'post_type'      => 'tenants',
            );

            $tenant_post_id = wp_insert_post($post, false);

            if($tenant_post_id != 0){
                add_post_meta($tenant_post_id, '_mpp_first_value', $first, true);
                add_post_meta($tenant_post_id, '_mpp_last_value', $last, true);
                add_post_meta($tenant_post_id, '_mpp_email_value', $email, true);
                add_post_meta($tenant_post_id, '_mpp_phone_value', $phone, true);
                add_post_meta($tenant_post_id, '_mpp_college_value', $studentid, true);
                add_post_meta($tenant_post_id, '_mpp_tenantstatus_value', 0, true);
                add_post_meta($tenant_post_id, 'notes', $property, true);
                add_post_meta($tenant_post_id, '_mpp_username_value', $username, true);
                add_post_meta($tenant_post_id, '_mpp_password_value', $password, true);
                add_post_meta($tenant_post_id, 'application', 1, true);
            }

            $userdata = array(
                'user_login' =>  $username,
                'user_pass'  => $password,
                'user_email' => $email,
                'display_name' => $title,
                'nickname' => $title,
                'first_name' => $first,
                'last_name' => $last,
                'role' => 'tenant'
            );

            $newuser = wp_insert_user($userdata);

            if($newuser && $tenant_post_id){
                updateLog($tenant_post_id, 'Apply Tenant', 'Tenant and User Created');
            } elseif($newuser){
                updateLog($tenant_post_id, 'Apply Tenant', 'User Created - Tenant Error');
            } elseif($tenant_post_id){
                updateLog($tenant_post_id, 'Apply Tenant', 'Tenant Created - User Error');
            } else {
                updateLog($tenant_post_id, 'Apply Tenant', 'Nothing Created - Error');
            }
        }
    }

    function mpp_create_user($post_id) {

        $last = $_POST['_mpp_last_value'];
        $email = $_POST['_mpp_email_value'];
        $firstName = $_POST['_mpp_first_value'];
        $nicename = $firstName . ' ' . $last;

        if( ( $_POST['post_status'] == 'publish' ) && ( $_POST['original_post_status'] != 'publish' )) {

            $username = $_POST['_mpp_username_value'];
            $password = $_POST['_mpp_password_value'];

            $userdata = array(
                'user_login'    =>  $username,
                'user_pass'  => $password,
                'user_email' => $email,
                'display_name' => $nicename,
                'nickname' => $nicename,
                'first_name' => $firstName,
                'last_name' => $last,
                'role' => 'tenant'
            );

            wp_insert_user($userdata);

        } else {

            $username = get_post_meta($post_id, '_mpp_username_value',true);
            $user = get_user_by('login', $username);
            $userdata = array(
                'ID' => $user->ID,
                'user_email' => $email,
                'display_name' => $nicename,
                'nickname' => $nicename,
                'first_name' => $firstName,
                'last_name' => $last,
                'role' => 'tenant'
            );
            wp_update_user($userdata);
        }
    }

    function wppm_redirect_dashboard () {
        global $current_user, $menu;
        get_currentuserinfo();

        if( ! in_array( 'administrator', $current_user->roles ) ) {
            reset( $menu );
            $page = key( $menu );
            while( ( __( 'Dashboard' ) != $menu[$page][0] ) && next( $menu ) ) {
                $page = key( $menu );
            }
            if( __( 'Dashboard' ) == $menu[$page][0] ) {
                unset( $menu[$page] );
            }
            reset($menu);
            $page = key($menu);
            while ( ! $current_user->has_cap( $menu[$page][1] ) && next( $menu ) ) {
                $page = key( $menu );
            }
            if ( preg_match( '#wp-admin/?(index.php)?$#', $_SERVER['REQUEST_URI'] ) &&
                ( 'index.php' != $menu[$page][2] ) ) {
                wp_redirect( get_option( 'siteurl' ) . '/wp-admin/admin.php?page=wppmanage_menu');
            }
        }
    }

    function wpse_remove_footer()
    {
        add_filter( 'admin_footer_text',    '__return_false', 11 );
        add_filter( 'update_footer',        '__return_false', 11 );
    }

    function hide_update_notice() {
        if ( !current_user_can( 'manage_options' ) ) :
            remove_action( 'admin_notices', 'update_nag', 3 );
        endif;
    }

    function remove_contact_menu () {
        if(!current_user_can('manage_options')){

            global $menu;
            $restricted = array(__('Contact'));
            end ($menu);
            while (prev($menu)){
                $value = explode(' ',$menu[key($menu)][0]);
                if(in_array($value[0] != NULL?$value[0]:"" , $restricted)){unset($menu[key($menu)]);}
            }
        }
    }

    function remove_admin_bar_links() {
        global $wp_admin_bar;
        if(!current_user_can('manage_options')){
            $wp_admin_bar->remove_menu('wp-logo');          // Remove the WordPress logo
            $wp_admin_bar->remove_menu('about');            // Remove the about WordPress link
            $wp_admin_bar->remove_menu('wporg');            // Remove the WordPress.org link
            $wp_admin_bar->remove_menu('documentation');    // Remove the WordPress documentation link
            $wp_admin_bar->remove_menu('support-forums');   // Remove the support forums link
            $wp_admin_bar->remove_menu('feedback');         // Remove the feedback link
//            $wp_admin_bar->remove_menu('site-name');        // Remove the site name menu
            $wp_admin_bar->remove_menu('view-site');        // Remove the view site link
            $wp_admin_bar->remove_menu('updates');          // Remove the updates link
            $wp_admin_bar->remove_menu('comments');         // Remove the comments link
            $wp_admin_bar->remove_menu('new-content');      // Remove the content link
            $wp_admin_bar->remove_menu('w3tc');             // If you use w3 total cache remove the performance link
        }
    }


    function remove_menus(){
        if(!current_user_can('manage_options')){
            remove_menu_page( 'index.php' );                  //Dashboard
            remove_menu_page( 'edit.php' );                   //Posts
            remove_menu_page( 'edit.php?post_type=page' );    //Pages
            remove_menu_page( 'edit-comments.php' );          //Comments
            remove_menu_page( 'themes.php' );                 //Appearance
            remove_menu_page( 'plugins.php' );                //Plugins
            remove_menu_page( 'tools.php' );                  //Tools
            remove_menu_page( 'options-general.php' );        //Settings
            remove_menu_page( 'upload.php' );                 //Media
            remove_menu_page( 'link-manager.php' );                 //Media
            remove_menu_page( 'profile.php' );                 //Media

        }
        if(!current_user_can('edit_users')){
            remove_menu_page( 'users.php' );                  //Users
        }
    }

    function my_login_redirect( $redirect_to, $request, $user ){
            global $user;
        if( isset( $user->roles ) && is_array( $user->roles ) ) {
                return 'wp-admin/admin.php?page=wppmanage_menu';
        }
        else {
            return $redirect_to;
        }
    }

    function clean_profile_page() {
        if(!current_user_can('manage_options')){
            // hide only for non-admins
            echo "<script>jQuery(document).ready(function(){jQuery('#url').parents('tr').remove();});</script>";
            echo "<script>jQuery(document).ready(function(){jQuery('#description').parents('table').prev().remove();});</script>";
            echo "<script>jQuery(document).ready(function(){jQuery('#description').parents('tr').remove();});</script>";
            echo "<script>jQuery(document).ready(function(){jQuery('#color-picker').parents('table').prev().remove();});</script>";
            echo "<script>jQuery(document).ready(function(){jQuery('#color-picker').parents('table').remove();});</script>";
            echo "<script>jQuery(document).ready(function(){jQuery('#user_login').parents().siblings('h3').remove();});</script>";
            echo "<script>jQuery(document).ready(function(){jQuery('#user_login').parents('table').remove();});</script>";
        }
    }

    function getMailTemplate($template, $data){

        $mailContent = '';

        switch($template){
            case 'late_rent':
                $mailContent ='
                    <html>
                    <head>
                        <title>'.date("M", $data['date']).' Late Rent</title>
                    </head>
                    <body>
                    <p>'.$data['name'].',</p>
                    <p>As of today, '.date("F", $data['date']).' 6th, we have not received your rent for '.date("F", $data['date']).'.  As indicated in your contract, a $25 late fee has been applied to your account, and beginning tomorrow, there will be an additional $5 fee added per day until your rent has been paid.</p>
                    <p>You can pay your rent online at www.myprovoproperty.com, drop a check off at our home office, or mail a check to PO Box 446, Orem, UT 84059.  Be aware that if you mail a check, you will be charged for any late days based upon when the check is RECEIVED - not based upon when it is sent.</p>
                    <p>Please make your payment as soon as possible to avoid further fees.  Let me know if you have any questions.</p>

                    <p>Thanks,</p>

                    <p>Adam<br />MyProvoProperty</p>
                    </body>
                    </html>';
                break;
            default:
                break;
        }
        return $mailContent;
    }

    function sendEmail($data, $template = "default"){

        $message = getMailTemplate($template, $data);
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .= 'To: '.$data['name'].' <'.$data['to'].'>' . "\r\n";
        $headers .= 'From: My Provo Property <adam@myprovoproperty.com>' . "\r\n";
        $headers .= 'Cc: adam@myprovoproperty.com' . "\r\n";

        mail($data['to'], $data['subject'], $message, $headers);
    }

    //Update Rents
    function checkTenantDeposit($id){

        $args = array(
            'post_type' => 'transactions',
            'posts_per_page' => -1,
            'meta_key'         => '_mpp_tenantid_value',
            'meta_value'       => $id
        );
        $transactions = get_posts($args);

        $deposit = 0;
        foreach($transactions as $transaction){
            $tmeta = get_post_meta($transaction->ID);
            if($tmeta['_mpp_transactiontype_value'][0] == 'deposit'){
                $deposit = $deposit + $tmeta['_mpp_amount_value'][0];
            }
        }

        return $deposit;
    }

    function checkTenantBalance($id){

        $args = array(
            'post_type' => 'transactions',
            'posts_per_page' => -1,
            'meta_key'         => '_mpp_tenantid_value',
            'meta_value'       => $id
        );
        $transactions = get_posts($args);

        $balance = 0;
        foreach($transactions as $transaction){
            $tmeta = get_post_meta($transaction->ID);

//            if($total == 'all'){
                if($tmeta['_mpp_transactiontype_value'][0] == 'rent' || $tmeta['_mpp_transactiontype_value'][0] == 'fee'){
                    $balance = $balance + $tmeta['_mpp_amount_value'][0];
                }
                if($tmeta['_mpp_transactiontype_value'][0] == 'payment'){
                    $balance = $balance - $tmeta['_mpp_amount_value'][0];
                }
//            }
//            else {
//                if($tmeta['_mpp_transactiontype_value'][0] == $total){
//                    $balance = $balance + $tmeta['_mpp_amount_value'][0];
//                }
//                if($tmeta['_mpp_transactiontype_value'][0] == 'payment'){
//                    $balance = $balance - $tmeta['_mpp_amount_value'][0];
//                }
//            }
        }
        return $balance;
    }

    function addRent(){
        $args = array(
            'posts_per_page' => -1,
            'meta_query' => array(
                array(
                    'key' => '_mpp_tenantstatus_value',
                    'value' => '2',
                )
            ),
            "orderby" => 'meta_value_num',
            "meta_key" => '_mpp_property_value',
            "order" => 'DESC',
            'post_type' => 'tenants'
        );
        $tenants = get_posts($args);

        foreach($tenants as $tenant){

            $tenant_values = get_post_meta($tenant->ID);

            $month = date('M', strtotime('+1 month'));
            $tdate = date('Y', strtotime('+1 month')) . date('m', strtotime('+1 month')) . '01';

            $post = array(
                'post_type' => 'transactions',
                'post_status' => 'publish',
                'post_title' => get_the_title($tenant->ID)
            );
            $newtransaction = wp_insert_post($post, false);

            if($newtransaction != 0){
                add_post_meta($newtransaction, '_mpp_transactiondate_value', $tdate, true);
                add_post_meta($newtransaction, '_mpp_amount_value', $tenant_values['_mpp_yrrate_value'][0], true);
                add_post_meta($newtransaction, '_mpp_reason_value', $month . ' Rent', true);
                add_post_meta($newtransaction, '_mpp_tenantid_value', $tenant->ID, true);
                add_post_meta($newtransaction, '_mpp_transactiontype_value', 'rent', true);
                after_transaction_change($newtransaction);
            }
        }
    }

    function showTenantLedger($tenID){
        $ledgerText = '<div class="ledger">
        <h2>Rent, Fees, and Payments</h2>
        <div class="sorting">
            <table id="sort-by" class="divided" style="background-color: white; border-collapse: collapse;">
                <tr style="font-weight:bold;">
                    <td width="200">Name</td>
                    <td width="100">Date</td>
                    <td width="100">Amount</td>
                    <td width="400">Description</td>
                    <td width="100">Type</td>
                    <td width="100"></td>
                </tr>
            </table>
        </div>
        <table class="divided">';
        $args = array(
            'post_type' => 'transactions',
            'posts_per_page' => -1,
            'meta_key' => '_mpp_transactiondate_value',
            'orderby' => 'meta_value_num',
            'order' => 'ASC',
            'meta_query' => array(
                'relation' => 'AND',
                array(
                    'key' => '_mpp_tenantid_value',
                    'value' => $tenID,
                    'compare' => '='
                ),
                array(
                    'key' => '_mpp_transactiontype_value',
                    'value' => array('fee','rent','payment'),
                    'compare' => 'IN'
                )
            )
        );
        $payments = get_posts($args);

        $rent = checkTenantBalance($tenID);

        foreach($payments as $payment){

            $starter = '';
            $meta = get_post_meta($payment->ID);
            $type = $meta['_mpp_transactiontype_value'][0];
            if($type =='payment'){
                $starter = "-";
            }

            $tenant = get_post($meta['_mpp_tenantid_value'][0]);
            $tenantmeta = get_post_meta($tenant->ID);
            $property = get_post($tenantmeta['_mpp_property_value'][0]);
            $nonce_delete = wp_nonce_url(get_bloginfo("url") . "/wp-admin/post.php?action=delete&amp;post=" . $payment->ID, "delete-post_" . $payment->ID);


            $ledgerText .='<tr class=" '. $meta['_mpp_tenantid_value'][0].' '.$tenantmeta['_mpp_property_value'][0].' '. $type.'">
            <td width="200" class="tenanme">' . $tenant->post_title . '</td>
            <td width="100" class="date">' . date("m/d/Y", strtotime($meta['_mpp_transactiondate_value'][0])) . '<span style="display:none;" class="calcdate">'.$meta['_mpp_transactiondate_value'][0].'</span></td>
            <td width="100" class="amount">'.$starter.'$' . number_format($meta['_mpp_amount_value'][0],2) . '</td>
            <td width="400" class="description">' . $meta['_mpp_reason_value'][0] . '</td>
            <td width="100" class="type">' . ucfirst($type) . '</td>
            <td width="100">';

            if(current_user_can("edit_users")){
                $ledgerText .= '<div class="linker" style="float:left; margin-right:10px; cursor:pointer;">
                                <a class="edittransaction" id="' . $payment->ID . '"><img style="width:12px;" src="'. plugins_url() . '/wppmanage/images/Modify.png" /></a>
                            </div>
                            <div class="linker" style="cursor:pointer;">
                                <a class="deletetransaction" tenid="'.$tenID.'" delurl="'. $nonce_delete .'">
                                    <img style="width:12px;" src="'. plugins_url() . '/wppmanage/images/delete-icon.gif" />
                                </a>
                            </div>';

            }


            $ledgerText .='
            </td>
            </tr>';
        }

        $ledgerText .= '</table>
        <div class="sorting">
            <table id="sort-by" style="background-color: white;">
                <tr style="font-weight:bold;">
                    <td width="200"></td>
                    <td width="100">Balance :</td>
                    <td width="100">$'.number_format($rent,2).'</td>
                    <td width="400"></td>
                    <td width="100"></td>
                    <td width="100"></td>
                </tr>
            </table>
        </div>
        </div>';

        return $ledgerText;

    }

    function generate_contract_url($post_id){
        $meta = get_post_meta($post_id);
        $baseurl = 'http://www.myprovoproperty.com/rental-contract/';
        $url = '?studentname=' . urlencode($meta['_mpp_first_value'][0]) . ' ' . urlencode($meta['_mpp_last_value'][0]);

        if($meta['_mpp_startdate_value'][0]){
            $time = strtotime($meta['_mpp_startdate_value'][0]);
            $startday = date('d',$time);
            $startmonth = date('F',$time);
            $startyear = date('Y',$time);
        }
        if($meta['_mpp_enddate_value'][0]){
            $time = strtotime($meta['_mpp_enddate_value'][0]);
            $endday = date('d',$time);
            $endmonth = date('F',$time);
            $endyear = date('Y',$time);
        }
        if($meta['_mpp_depositdate_value'][0]){
            $time = strtotime($meta['_mpp_depositdate_value'][0]);
            $depday = date('d',$time);
            $depmonth = date('F',$time);
            $depyear = date('Y',$time);
        }
        if($meta['_mpp_property_value'][0]){
            $propmeta = get_post_meta($meta['_mpp_property_value'][0]);
            $address = $propmeta['_mpp_address_value'][0];
            $complex = get_the_title($meta['_mpp_property_value'][0]);
            $bedroom = $propmeta['_mpp_roomtype_value'][0];
            $city = $propmeta['_mpp_city_value'][0];
            $capacity = $propmeta['_mpp_capacity_value'][0];
            $facility = $complex;
        }

                //for value fields the key is the name in the back end, the value is the field name in the form and the default value if not indicated
        $valuefields = array(
            "capacity" => array("capacity", $capacity),
            "city" => array("city", $city),
            "bedroom" => array("bedroom", $bedroom),
            "complex" => array("complex", $complex),
            "address" => array("address", $address),
            "facility" => array("facility", $facility),

            "startday" => array("tenancyday", $startday),
            "startmonth" => array("tenancymonth", $startmonth),
            "startyear" => array("tenancyyear", $startyear),

            "endday" => array("tenancyendday", $endday),
            "endmonth" => array("tenancyendmonth", $endmonth),
            "endyear" => array("tenancyendyear", $endyear),

            "totalrent" => array("totalrent", "3588"),
            "yrrate" => array("monthlyrent", "330"),
            "paymentschedule" => array("paymentschedule"),

            "depday" => array("depositday", $depday),
            "depmonth" => array("depositmonth", $depmonth),
            "depyear" => array("deposityear", $depyear),

            "latefee" => array("latefee", "25"),
            "dailylatefee" => array("dailylatefee", "5"),
            "deposit" => array("depositamount", "250"),
            "phone" => array("studentphone"),
        );
        $first = true;

        foreach($valuefields as $key => $value) {
            $tenvalue = $meta['_mpp_' . $key . '_value'][0];
            if($tenvalue){
                $url .= '&' . $value[0] . '=' . rawurlencode($tenvalue);
            } elseif(!empty($value[1])) {
                $url .= '&' . $value[0] . '=' . rawurlencode($value[1]);
            }
            $first = false;
        }

        return $baseurl . $url;
    }

    function after_transaction_change($post_id){
        if(get_post_type($post_id) == 'transactions'){
            $meta = get_post_meta($post_id);
            $id = $meta['_mpp_tenantid_value'][0];
            $dep = get_deposit_balance($id, 'num');
            $bal = get_balance($id, 'num');
            update_post_meta($id, '_mpp_depositremaining_value', $dep);
            update_post_meta($id, '_mpp_currentbalance_value', $bal);
        }
        if(get_post_type($post_id) == 'tenants'){

            $url = generate_contract_url($post_id);
            update_post_meta($post_id, '_mpp_contracturl_value', $url);
        }
    }

    function before_transaction_delete($postid){
        if(get_post_type($postid) == 'transactions'){
            $meta = get_post_meta($postid);
            $id = $meta['_mpp_tenantid_value'][0];
            $dep = get_deposit_balance($id, 'num');
            $bal = get_balance($id, 'num');
            update_post_meta($id, '_mpp_depositremaining_value', $dep);
            update_post_meta($id, '_mpp_currentbalance_value', $bal);
        }
    }

    function showDepositLedger($tenID){

        $args = array(
            'post_type' => 'transactions',
            'posts_per_page' => -1,
            'meta_key' => '_mpp_transactiondate_value',
            'orderby' => 'meta_value_num',
            'order' => 'ASC',
            'meta_query' => array(
                'relation' => 'AND',
                array(
                    'key' => '_mpp_tenantid_value',
                    'value' => $tenID,
                    'compare' => '='
                ),
                array(
                    'key' => '_mpp_transactiontype_value',
                    'value' => 'deposit',
                    'compare' => '='
                )
            )
        );
        $deposittransactions = get_posts($args);

        $ledgerString = '
            <div class="depositinformation">
                <h2>Deposit Summary</h2>

                <div class="sorting">
                    <table id="sort-by" style="background-color: white;">
                        <tr style="font-weight:bold;">
                            <td width="200">Name</td>
                            <td width="100">Date</td>
                            <td width="100">Amount</td>
                            <td width="500">Description</td>
                            <td width="100"></td>
                        </tr>
                    </table>
                </div>';

        $ledgerString .= '<table class="divided">';

        foreach($deposittransactions as $deposittransaction){

                $meta = get_post_meta($deposittransaction->ID);
                $type = $meta['_mpp_transactiontype_value'][0];
                $tenant = get_post($meta['_mpp_tenantid_value'][0]);
                $tenantmeta = get_post_meta($tenant->ID);
                $nonce_delete = wp_nonce_url(get_bloginfo("url") . "/wp-admin/post.php?action=delete&amp;post=" . $deposittransaction->ID, "delete-post_" . $deposittransaction->ID);


            $ledgerString.= '<tr class=" '. $meta['_mpp_tenantid_value'][0].' '.$tenantmeta['_mpp_property_value'][0].' '. $type.'">';
                $ledgerString.= '<td width="200" class="tenname">' . $tenant->post_title . '</td>';
                $ledgerString.= '<td width="100" class="date">' . date("m/d/Y", strtotime($meta['_mpp_transactiondate_value'][0])) . '<span style="display:none;" class="calcdate">'.$meta['_mpp_transactiondate_value'][0].'</span></td>';
                $ledgerString.= '<td width="100" class="amount">$' . number_format($meta['_mpp_amount_value'][0],2) . '</td>';
                $ledgerString.= '<td width="500" class="description">' . $meta['_mpp_reason_value'][0] . '</td>';
                $ledgerString.= '<td width="100">';

                if(current_user_can("edit_users")){
                    $ledgerString .= '<div class="linker" style="float:left; margin-right:10px; cursor:pointer;">
                                <a class="edittransaction" id="' . $deposittransaction->ID . '"><img style="width:12px;" src="'. plugins_url() . '/wppmanage/images/Modify.png" /></a>
                            </div>
                            <div class="linker" style="cursor:pointer;">
                                <a class="deletetransaction" tenid="'.$tenID.'" delurl="'. $nonce_delete .'">
                                    <img style="width:12px;" src="'. plugins_url() . '/wppmanage/images/delete-icon.gif" />
                                </a>
                            </div>';
                }

                $ledgerString .= '


                </td>';

                $ledgerString.= '</tr>';
            }

        $ledgerString .= '</table>';
        $ledgerString .= '
            <div class="sorting">
                <table id="sort-by" style="background-color: white;">
                    <tr style="font-weight:bold;">
                        <td width="200"></td>
                        <td width="100">Balance : </td>
                        <td width="100">$' . number_format(checkTenantDeposit($tenID),2) .'</td>
                        <td width="500"></td>
                        <td width="100"></td>
                    </tr>
                </table>
            </div>';
        $ledgerString .= '</div>';

        return $ledgerString;
    }

    function showTenantList($status){
        $tenantlist = '
            <div id="tenantlist">
                    <table class="tenantlist" style="background-color:#fff;">
                        <tr>
                            <td width="25"></td>
                            <td width="125">Name</td>
                            <td width="100">Property</td>
                            <td width="100">Phone</td>
                            <td width="225">Email</td>
                            <td width="50">Dep</td>
                            <td width="50">Bal</td>
                        </tr>';

                        $args = array(
                            'post_type' => 'tenants',
                            'post_status' => 'publish',
                            'posts_per_page' => -1,
                            'orderby' => 'title',
                            'order' => 'asc',
                            'meta_key' => '_mpp_tenantstatus_value',
                            'meta_value' => $status
                        );
                        $tenants = get_posts($args);

                        foreach ($tenants as $tenant) {

                            $tenant_meta = get_post_meta($tenant->ID);
                            $tenantlist .=
                            $tenantlist .= '<tr>';
                            $tenantlist .= '<td width="25"><a href="' . admin_url() . 'post.php?post=' . $tenant->ID . '&action=edit"><img style="width:12px;" src="' . plugins_url() . '/wppmanage/images/Modify.png" /></a></td>';
                            $tenantlist .= '<td width="125" class="name">' . $tenant->post_title . '</td>';
                            $tenantlist .= '<td width="100" class="property">' . get_the_title($tenant_meta['_mpp_property_value'][0]) . '</td>';
                            $tenantlist .= '<td width="100" class="phone">' . $tenant_meta['_mpp_phone_value'][0] . '</td>';
                            $tenantlist .= '<td width="225" class="email">' . $tenant_meta['_mpp_email_value'][0] . '</td>';
                            $tenantlist .= '<td width="50" class="left">' . get_deposit_balance($tenant->ID, 'html') . '</td>';
                            $tenantlist .= '<td width="50" class="due">' . get_balance($tenant->ID, 'html') . '</td>';

                            $tenantlist .= '</td>
                                    </tr>
                                    <tr class="tenantdetails">
                                    <td colspan="8">
                                    <div class="tenantinfo">
                                    </div>

                                    ' . showDepositLedger($tenant->ID) . '<br /><hr />' . showTenantLedger($tenant->ID) . ' <br /><hr /><button type="button" id="' . $tenant->ID . '" class="actionbutton finances">Print Deposit Summary</button>

                                    </td>
                                    </td></tr><div style="margin-bottom:25px;"></div>';

                        }

            $tenantlist .= '</table></div>';

        return $tenantlist;
    }

    function addFees($date, $tenantid){
        $updatetext = '';
        date_default_timezone_set('America/Denver');

        //if we have already added rents for the next month, check late fees based on last month
        $rents = ($date > 19 || $date < 6)? 2 : 1;

        $args = array(
            'posts_per_page' => $rents,
            'meta_query' => array(
                'relation' => 'AND',
                array(
                    'key' => '_mpp_transactiontype_value',
                    'value' => 'rent',
                ),
                array(
                    'key' => '_mpp_tenantid_value',
                    'value' => $tenantid
                )
            ),
            "meta_key" => '_mpp_transactiondate_value',
            "orderby" => 'meta_value_num',
            "order" => 'DESC',
            'post_type' => 'transactions',
            'post_status' => 'publish'
        );
        $lastrent = get_posts($args);

        $count = 0;

        foreach($lastrent as $rent){

            //skips the most recent rent charge if it is for next month (or not late yet) unless it is the only charge (for a new tenant, for example)
            if($rents == 2 && $count == 0 && count($lastrent) > 1){
                $count = 1;
                continue;
            }

            $lastdate = get_post_meta($rent->ID, '_mpp_transactiondate_value', true);
            $now = time();
            $last = strtotime($lastdate);
            $datediff = $now - $last;
            $daysdiff =  floor($datediff/(60*60*24));
            $dayslate = $daysdiff - 4;


            if($dayslate > 0 && $dayslate < 32){

                $fees = 25 + (($dayslate - 1) * 5);

                $transactiondate = date('Ymd');
                $type = 'fee';
                $description = 'Late fees for ' . date('M', $last);
                $id = $tenantid;
                $name = get_the_title($tenantid);

                $args = array(
                    'posts_per_page' => 1,
                    'post_type' => 'transactions',
                    'post_status' => 'publish',
                    'meta_query' => array(
                        'relation' => 'AND',
                        array(
                            'key' => '_mpp_reason_value',
                            'value' => $description
                        ),
                        array(
                            'key' => '_mpp_tenantid_value',
                            'value' => $tenantid
                        )
                    ),
                    'post_title' => $name
                );
                $existingfee = get_posts($args);

                if(count($existingfee) > 0){
                    $updatetext = 'Late Fee Updated';

                    foreach($existingfee as $fee){
                        $existid = $fee->ID;
                        update_post_meta($existid, '_mpp_amount_value', $fees);
                        update_post_meta($existid, '_mpp_transactiondate_value', $transactiondate);
                        after_transaction_change($existid);
                    }

                } else {
                    //insert new fee post
                    $updatetext = 'Late Fee Added';
                    $post = array(
                        'post_type' => 'transactions',
                        'post_status' => 'publish',
                        'post_title' => $name
                    );
                    $newtransaction = wp_insert_post($post, false);

                    if($newtransaction != 0){
                        add_post_meta($newtransaction, '_mpp_transactiondate_value', $transactiondate, true);
                        add_post_meta($newtransaction, '_mpp_amount_value', $fees, true);
                        add_post_meta($newtransaction, '_mpp_reason_value', $description, true);
                        add_post_meta($newtransaction, '_mpp_tenantid_value', $id, true);
                        add_post_meta($newtransaction, '_mpp_transactiontype_value', $type, true);
                        after_transaction_change($newtransaction);

                        //send email about late rent
                        if($_SERVER['HTTP_HOST'] == 'www.myprovoproperty.com' || $_SERVER['HTTP_HOST'] == 'myprovoproperty.com') {

                            $mailData = array(
                                "to" => get_post_meta($tenantid, '_mpp_email_value', true),
                                "subject" => date('F', $last) . ' Late Rent',
                                "name" => $name,
                                "date" => $last
                            );
                            sendEmail($mailData, 'late_rent');
                        }

                    }
                }
            }
            return $updatetext;
        }
        return $updatetext;
    }

class PageTemplater {

    protected $plugin_slug;
    private static $instance;
    protected $templates;

    public static function get_instance() {
        if( null == self::$instance ) {
            self::$instance = new PageTemplater();
        }
        return self::$instance;
    }

    //TODO: Bring all templates to this structure (archive-properties)
    private function __construct() {
        $this->templates = array();
        add_filter('page_attributes_dropdown_pages_args',array( $this, 'register_project_templates' ));
        add_filter('wp_insert_post_data',array( $this, 'register_project_templates' ));
        add_filter('template_include', array( $this, 'view_project_template'));

        // Add your templates to this array.
        $this->templates = array(
            '../templates/securedtemplate.php'     => 'Secured Default',
        );
    }

    public function register_project_templates( $atts ) {
        $cache_key = 'page_templates-' . md5( get_theme_root() . '/' . get_stylesheet() );
        $templates = wp_get_theme()->get_page_templates();
        if ( empty( $templates ) ) { $templates = array(); }
        wp_cache_delete( $cache_key , 'themes');
        $templates = array_merge( $templates, $this->templates );
        wp_cache_add( $cache_key, $templates, 'themes', 1800 );
        return $atts;
    }

    public function view_project_template( $template ) {
        global $post;
        if (!isset($this->templates[get_post_meta(
            $post->ID, '_wp_page_template', true
        )] ) ) {
            return $template;
        }
        $file = plugin_dir_path(__FILE__). get_post_meta(
                $post->ID, '_wp_page_template', true
            );
        if( file_exists( $file ) ) {
            return $file;
        }
        else { echo $file; }
        return $template;
    }
}


// TODO: Abstract out displays of transaction form
// TODO: Abstract out ledger views

