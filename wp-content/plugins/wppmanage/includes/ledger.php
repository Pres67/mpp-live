<?php
$current_user = wp_get_current_user();
if(!current_user_can('edit_users')) die("Access Denied");

//Filter Setup
$args = array(
     'post_type' => 'tenants',
    'post_status' => 'publish',
    'posts_per_page' => -1,
    'orderby' => 'title',
    'order' => 'asc'
);
$tenants = get_posts($args);
$args = array(
    'post_type' => 'properties',
    'post_status' => 'publish',
    'posts_per_page' => -1,
    'orderby' => 'title',
    'order' => 'asc'
);
$properties = get_posts($args);
?>
<div class="popedit" id="popedit">
</div>
<div class="popcontent" id="popcontent">
</div>
<style>
    #container{
        padding-left:1px;
    }
    #popedit, #popcontent {
        display:none;
    }
    .popedit{
        position:absolute;
        top:0;
        left:-10px;
        z-index:1;
    }
    .popcontent{
        z-index:2;
        position:absolute;
        max-width:300px;
    }
    input{
        width:100%;
        margin:0px;
        padding:0px;
    }
    th{
        cursor:pointer;
        padding:10px;
        background-color:#fff;
    }
    th.reset{
        background-color:#000;
    }
    th.reset a{
        padding:10px;
        color:#fff;
        text-decoration: none;
    }
    .sorting td{
        padding-left:5px;
        background-color:#fff;
    }
    .sorting td a{
        display:block;
        padding-top:10px;
        padding-bottom:10px;
        text-decoration: none;
    }
    .isotopeitem div{
        padding:5px 1px 5px 5px;
        background-color:#fff;
        float:left;
        border:1px solid #f1f1f1;
        display:table-cell;
    }
    .isotopeitem div.clearall{
        padding:0;
        border:none;
    }
    .isotopeitem div a{
        display:block;
        padding-top:10px;
        padding-bottom:10px;
        text-decoration: none;
    }
    .isotopeitem div.linker{
        background-color: transparent;
        padding-bottom:0;
        padding-top:3px;
    }
    .isotopeitem div.linker a{
        padding:3px;
    }
    .arrows{
        float:right;
    }
    .sorting td .arrows a{
        padding:0;
    }
    .sorting td .arrows a:focus, #filters td a:focus{
        box-shadow:none;
    }
    .toparrow{
        padding: 5px;
    }
    .bottomarrow{
        padding: 5px;
    }
    .trans .sortname{
        margin-top:5px;
    }
    .sortname{
        float:left;
        margin-top:18px;
    }
    .sortable.active{
        background-color:yellow;
    }
    #filters td a{
        text-decoration: none;
        display:block;
        padding: 3px 3px;
    }
    #filters td a:hover{
        background-color:#eaeaea;
    }
    .searchbox.ledger{
        height: 37px;
        margin-top: 3px;
        margin-left: 3px;
        width: 400px;
        margin-bottom: 15px;
        padding:8px;
    }
    .edittransaction, .deletetransaction{
        cursor:pointer;
    }
    .maintitles{
        width:500px;
    }
    #poststuff .postbox-container.recordform{
        margin-top:147px; float:right; width: 282px;
    }
    #poststuff #post-body.columns-2.translist{
        float:right; margin-right:25px;
    }

    @media (max-width: 1420px) {
        #poststuff .postbox-container.recordform{
            clear:both;
            float:none;
            margin-top:0;
            width:100%;
        }
        #poststuff #post-body.columns-2.translist{
            clear:both;
            float:none;
            width:100%;
        }
    }

    @media (max-width: 1050px) {
        .property{
            display:none !important;
        }
    }

    @media (max-width: 815px) {
        .description{
            display:none !important;
        }
    }

</style>

<div style="width:900px; float:left;">
    <?php  $transactionURL = admin_url( 'admin.php?page=wppmanage_ledger', 'http' );  ?>
    <h2>Recent Transactions <span style="font-size:9px;"><a href="<?php echo $transactionURL; ?>">(SEE ALL)</a></span></h2>
<div style="float:left;">
        <div id="post-body-content" style="width:auto;">

            <div class="sorting trans">
                <table id="sort-by">
                    <tr>
                        <td width="150">
                            <div class="sortname">Name</div>
                        </td>
                        <td width="100" class="property">  <div class="sortname">Property</div>
                        </td>
                        <td width="100">  <div class="sortname">Date</div>
                        </td>
                        <td width="75">  <div class="sortname">Amt</div>
                        </td>
                        <td width="200" class="description">  <div class="sortname">Desc</div>
                        </td>
                        <td width="105">  <div class="sortname">Type</div>
                        </td>
                        <td colspan="2" width="50" align="center" class="sortable"><a href=""><span style="font-size:7px;">CLEAR</span></a></td>
                    </tr>
                </table>
            </div>

            <div id="container">
                <?php
                    $args = array(
                    'post_type' => 'transactions',
                    'post_status' => 'publish',
                    'posts_per_page' => 10,
                    'orderby' => 'modified'
                    );
                    $payments = get_posts($args);

                    foreach($payments as $payment){

                    $meta = get_post_meta($payment->ID);
                    $type = $meta['_mpp_transactiontype_value'][0];
                    $nonce_delete = wp_nonce_url(get_bloginfo("url") . "/wp-admin/post.php?action=delete&amp;post=" . $payment->ID, "delete-post_" . $payment->ID);

                    $tenant = get_post($meta['_mpp_tenantid_value'][0]);
                    $tenantmeta = get_post_meta($tenant->ID);
                    $property = get_post($tenantmeta['_mpp_property_value'][0]);

                            echo '<div class="isotopeitem '. $meta['_mpp_tenantid_value'][0].' '.$tenantmeta['_mpp_property_value'][0].' '. $type.'">';
                            echo '<div style="width:150px;" class="name">' . $tenant->post_title . '</div>';
                            echo '<div style="width:100px" class="property">'.get_the_title($property->ID) .'</div>';
                            echo '<div style="width:100px" class="date">' . date("m/d/Y", strtotime($meta['_mpp_transactiondate_value'][0])) . '<span style="display:none;" class="calcdate">'.$meta['_mpp_transactiondate_value'][0].'</span></div>';
                            echo '<div style="width:75px" class="amount">$' . number_format($meta['_mpp_amount_value'][0],2,".","") . '</div>';
                            echo '<div style="width:200px" class="description">' . $meta['_mpp_reason_value'][0] . '&nbsp; </div>';
                            echo '<div style="width:105px" class="type">' . ucfirst($type) . '</div>'; ?>
                            <div class="linker">
                                <a class="edittransaction" id="<?php echo $payment->ID; ?>">
                                    <img style="width:12px;" src="<?php echo plugins_url() . '/wppmanage/images/Modify.png'; ?>" />
                                </a>
                            </div>
                            <div class="linker">
                                <a class="deletetransaction" tenid="<?php echo $tenant->ID ?>" delurl="<?php echo $nonce_delete ?>">
                                    <img style="width:12px;" src="<?php echo plugins_url() . '/wppmanage/images/delete-icon.gif'; ?>" />
                                </a>
                            </div>
                            <div class="clearall"></div>
                        </div>
                 <?php } ?>

            </div>
    </div>
</div> <!-- wrap -->