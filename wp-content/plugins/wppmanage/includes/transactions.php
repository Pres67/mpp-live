<?php include_once '../../../../wp-load.php';

if( (isset($_POST['type']) && $_POST['type'] == "intuit") || (isset($_GET['type']) && $_GET['type'] == "intuit")  ){

    $body = $_POST["body"];
    $date = $_POST["date"];
    $subject = $_POST["subject"];
    $body = strip_tags($body);

    //get tenant name into first and last from subject
    $pullname = substr($subject, 0, -18);
    $nameelements = explode(" ", $pullname);

    //pull tenant email from message body
    $tenantemail = '';
    preg_match('/[A-Za-z0-9_-]+@[A-Za-z0-9_-]+\.([A-Za-z0-9_-][A-Za-z0-9_]+)/', $body, $tenantemail);

    //pull payment amount from message body
    $amt = '';
    preg_match('/(\$[0-9,]+(\.[0-9]{2})?)/', $body, $amt);
    $amount = (float)(str_replace(',', '', substr($amt[0], 1))) - .50;

    //set date for transaction occurence
    date_default_timezone_set('America/Denver');
    $phpdate = new DateTime($date);
    $transactiondate = $phpdate->format("Ymd");

    //find by email first
    $args = array(
        'post_type' => 'tenants',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'meta_query' => array(
            array(
                'key'     => '_mpp_email_value',
                'value'   => $tenantemail[0],
                'compare' => '=',
            ),
        ),
    );
    $tenant = get_posts($args);

    if(empty($tenant)){
        //find by full name
        $args = array(
            'post_type' => 'tenants',
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'meta_query' => array(
                'relation' => 'AND',
                array(
                    'key'     => '_mpp_first_value',
                    'value'   => trim($nameelements[0]),
                    'compare' => '=',
                ),
                array(
                    'key'     => '_mpp_last_value',
                    'value'   => trim($nameelements[1]),
                    'compare' => '=',
                ),
            ),
        );
        $tenant = get_posts($args);
    }

    //pull only last name in case parent paid
    if(empty ($tenant)){
        $args = array(
            'post_type' => 'tenants',
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'meta_key' => '_mpp_last_value',
            'meta_value' => trim($nameelements[1])
        );
        $tenant = get_posts($args);

        //if multiple results or no results, empty out the tenant variable
        if(count($tenants) != 1){
            $tenants = '';
        }
    }

    if(!empty($tenant)){
        $name = get_the_title($tenant[0]->ID);
        $type = 'payment';
        $description = 'Rent Payment';
        $method = 'Intuit';

        $data = array(
            'name' => $name,
            'transactiondate' => $transactiondate,
            'amount' => $amount,
            'method' => $method,
            'description' => $description,
            'tenID' => $tenant[0]->ID,
            'type' => $type,
        );
        processPayment($data);
        //Add logging for automated actions
        updateLog($tenant[0]->ID, 'IPN_trans', 'Intuit Payment Recorded');
        echo 'Transaction Added';
    } else {
        updateLog('FAILED', 'IPN_trans', 'No Tenant Match');
        echo 'Transaction Failed to Add';
    }

} elseif(isset($_GET['type']) && $_GET['type'] == "paypal"){

    //TODO: Move this to a more "hidden" or "administrative" location and change the endpoint in paypal

    $raw_post_data = file_get_contents('php://input');
    $raw_post_array = explode('&', $raw_post_data);
    $myPost = array();
    foreach ($raw_post_array as $keyval) {
        $keyval = explode ('=', $keyval);
        if (count($keyval) == 2)
            $myPost[$keyval[0]] = urldecode($keyval[1]);
    }

    $req = 'cmd=_notify-validate';
    if(function_exists('get_magic_quotes_gpc')) {
        $get_magic_quotes_exists = true;
    }
    foreach ($myPost as $key => $value) {
        if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
            $value = urlencode(stripslashes($value));
        } else {
            $value = urlencode($value);
        }
        $req .= "&$key=$value";
    }

    $ch = curl_init('https://www.paypal.com/cgi-bin/webscr');
    curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
    curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));


    if( !($res = curl_exec($ch)) ) {
        curl_close($ch);
        exit;
    }
    curl_close($ch);

    if (strcmp ($res, "VERIFIED") == 0) {

        date_default_timezone_set('America/Denver');

        $tenID = $_POST['option_selection1'];
        $total_paid = $_POST['mc_gross'];
        $tax = $_POST['tax'];
        $date = $_POST['payment_date'];

        $transactiondate = date("Ymd");
        $amount = $total_paid - $tax;
        $type = 'payment';
        $description = 'Rent Payment';
        $name = get_the_title($tenID);
        $method = 'Paypal';

        $data = array(
            'name' => $name,
            'transactiondate' => $transactiondate,
            'amount' => $amount,
            'method' => $method,
            'description' => $description,
            'tenID' => $tenID,
            'type' => $type,
        );

        processPayment($data);

        //Add logging for automated actions
        updateLog($tenID, 'IPN_trans', 'Paypal Payment Recorded');
    } else if (strcmp ($res, "INVALID") == 0) {
        updateLog('FAILED', 'IPN_trans', $res);
    }

}

function processPayment($data){

    $post = array(
        'post_type' => 'transactions',
        'post_status' => 'publish',
        'post_title' => $data['name']
    );
    $newtransaction = wp_insert_post($post, false);

    update_post_meta($newtransaction, '_mpp_transactiondate_value', $data['transactiondate']);
    update_post_meta($newtransaction, '_mpp_amount_value', $data['amount']);
    update_post_meta($newtransaction, '_mpp_method_value', $data['method']);
    update_post_meta($newtransaction, '_mpp_reason_value', $data['description']);
    update_post_meta($newtransaction, '_mpp_tenantid_value', $data['tenID']);
    update_post_meta($newtransaction, '_mpp_transactiontype_value', $data['type']);
    after_transaction_change($newtransaction);
}
?>