<div class="wrap" id="cron-gui">
    <div id="icon-tools" class="icon32"><br /></div>
    <h2><?php _e( 'MPP\'s Automatic Jobs', 'cron-view' ); ?></h2>

    <?php
    $schedules = wp_get_schedules();
    $cron = _get_cron_array();
    date_default_timezone_set('America/Denver'); ?>

    <h3><?php _e('Events', 'cron-view'); ?></h3>

    <table class="widefat fixed divided">
        <thead>
        <tr>
            <th scope="col"><?php _e('Next due (MST)', 'cron-view'); ?></th>
            <th scope="col"><?php _e('Schedule', 'cron-view'); ?></th>
            <th scope="col"><?php _e('Hook', 'cron-view'); ?></th>
            <th scope="col"><?php _e('Arguments', 'cron-view'); ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ( $cron as $timestamp => $cronhooks ) { ?>
            <?php foreach ( (array) $cronhooks as $hook => $events ) { ?>
                <?php if(substr($hook, 0, 4) == '_mpp'){ ?>
                    <?php foreach ( (array) $events as $event ) { ?>
                        <tr>
                            <th scope="row"><?php echo $event[ 'date' ]; ?> <?php echo date("m/d/y g:i a",$timestamp); ?></th>
                            <td>
                                <?php
                                if ( $event[ 'schedule' ] ) {
                                    echo $schedules [ $event[ 'schedule' ] ][ 'display' ];
                                } else {
                                    ?><em><?php _e('One-off event', 'cron-view'); ?></em><?php
                                }
                                ?>
                            </td>
                            <td><?php echo substr($hook, 5); ?></td>
                            <td><?php if ( count( $event[ 'args' ] ) ) { ?>
                                    <ul>
                                        <?php foreach( $event[ 'args' ] as $key => $value ) { ?>
                                            <strong>[<?php echo $key; ?>]:</strong> <?php echo $value; ?>
                                        <?php } ?>
                                    </ul>
                                <?php } ?></td>
                        </tr>
                    <?php } ?>
                <?php }?>
            <?php } ?>
        <?php } ?>
        </tbody>
    </table>

    <h3>Change History</h3>

    <table class="widefat fixed divided">
        <thead>
        <tr>
            <th scope="col">Run Date (MST)</th>
            <th scope="col">Entity</th>
            <th scope="col">Hook</th>
            <th scope="col">Result</th>
        </tr>
        </thead>
        <tbody>
        <?php  $dir = plugin_dir_path( __FILE__ );  $file = $dir . 'cronlog.txt';  include($file); ?>
        </tbody>
    </table>
</div>