jQuery(document).ready(function() {

    if(jQuery('#_mpp_tenantstatus_value').val() > 1){
        jQuery('#new-meta-boxes-tenant-contract').hide();
    }

    var url = window.location.toString();
    var spliturl = url.split("/wp-admin/");
    var jsurl = spliturl[0] + '/wp-content/plugins/wppmanage/includes/actions.php';

    jQuery('.tenantlist th.name').click(function(){
        jQuery(this).parent().next().toggle();
    });
 
    jQuery('.tenantlist th span#showcollege').click(function(){
        jQuery('td.college').toggle();
        jQuery('td.email').toggle();
    });

    jQuery('.showprogress').click(function(){
        jQuery('.progress').toggle();
        jQuery('.futuretenants .email').toggle();
        jQuery('.futuretenants .phone').toggle();
        jQuery('.futuretenants .property').toggle();
        jQuery('.futuretenants .due').toggle();
        jQuery('.futuretenants .left').toggle();
    });

    jQuery('.finances').click(function(){
        var id = this.id;

        request = jQuery.ajax({
            url:jsurl,
            type: "post",
            data: {action:"depositledger", id:id}
        });
        request.done(function (response, textStatus, jqXHR){
            var newWindow = window.open();
            newWindow.document.write(response);
        });
    });

    function updateLedger(popup){

        if(document.getElementById('location')){
            var location = document.getElementById('location').value;
        }

        if(location == 'overview'){
            request = jQuery.ajax({
                url:jsurl,
                type: "post",
                data: {action:"updateledger", count:10}
            });
            request.done(function (response, textStatus, jqXHR){
                var $newI = jQuery(response);

                $popback =  document.getElementById('container');
                $popback.innerHTML = '';

                jQuery('#container').isotope( 'insert', $newI );
            });
        } else if(location =='tenantpage') {
            request = jQuery.ajax({
                url:jsurl,
                type: "post",
                data: {action:"updateledger"}
            });
            request.done(function (response, textStatus, jqXHR){
                window.location.href = window.location.href;
            });
        } else {
            request = jQuery.ajax({
                url:jsurl,
                type: "post",
                data: {action:"updateledger"}
            });
            request.done(function (response, textStatus, jqXHR){
                var $newI = jQuery(response);

                $popback =  document.getElementById('container');
                $popback.innerHTML = '';

                clearFields();

                jQuery('#container').isotope( 'insert', $newI );
            });
        }
    }

    function clearFields(prefix){
        var emethodSelect = document.getElementById("paymethod");
            emethodSelect.style.display = "none";
            jQuery('#tenant_name option:first').attr('selected', true);
            jQuery('#transactionamount').val('');
            jQuery('#transactionmethod').prop('selectedIndex', 0);
            jQuery('#transactiontype').prop('selectedIndex', 0);
            jQuery('#transactiondescription').val('');
    }

    jQuery('#sort-by a').click(function(){
        // get href attribute, minus the '#'
        var sortName = jQuery(this).attr('href').slice(1);
        jQuery('#container').isotope({ sortBy : sortName });
        return false;
    });

    jQuery('.sortable a').click(function(){
        jQuery('.sortable').removeClass('active');
        jQuery(this).parents('.sortable').toggleClass('active');
    });

    jQuery('.expandable').click(function(){
        if(jQuery(this).hasClass('dropped')){
            jQuery(this).find('tbody').toggle();
        } else {
            jQuery(this).parents('#filters').children('.expandable').removeClass('dropped');
            jQuery(this).parents('#filters').children('.expandable').find('tbody').hide();
            jQuery(this).find('tbody').toggle();
            jQuery(this).addClass('dropped');
        }
    });

    jQuery(window).resize(function() {
        if (jQuery('#popedit').is(':visible')){
            centerPopup();
        }
    });

    jQuery('#popedit').click(function() {
        closePopup();
    });

    jQuery(document).on('click', '#canceledittransactionbutton', function() {
        closePopup();
    });

    jQuery(document).on('click', '.deletetenant', function() {
        event.preventDefault();
        if(confirm('Delete this tenant?')){
            var id = this.id;

            request = jQuery.ajax({
                url:jsurl,
                type: "post",
                data: {action:"deletetenant", id:id}
            });
            request.done(function (response, textStatus, jqXHR){
                window.location.href = spliturl[0] + "/wp-admin/admin.php?page=wppmanage_menu";
            });
        }
    });

    jQuery(document).on('click', '.approvetenant', function() {
        event.preventDefault();
        if(confirm('Approve this tenant?')){
            var id = this.id;

            request = jQuery.ajax({
                url:jsurl,
                type: "post",
                data: {action:"approvetenant", id:id}
            });
            request.done(function (response, textStatus, jqXHR){
                window.location.reload();
            });
        }
    });

    jQuery(document).on('click', '.unapprovetenant', function() {
        event.preventDefault();
        if(confirm('Unapprove this tenant?')){
            var id = this.id;

            request = jQuery.ajax({
                url:jsurl,
                type: "post",
                data: {action:"unapprovetenant", id:id}
            });
            request.done(function (response, textStatus, jqXHR){
                window.location.reload();
            });
        }
    });

    jQuery(document).on('click', '.deletetransaction', function() {

        event.preventDefault();
        var del = confirm('Delete this transaction?');

        if(del == true){
            var delurl = jQuery(this).attr("delurl");
            var postid = jQuery(this).attr("tenid");

            request = jQuery.ajax({
                url:delurl
            });
            request.done(function (response, textStatus, jqXHR){
                updateLedger();

                request = jQuery.ajax({
                    url:jsurl,
                    type: "post",
                    data: {action:"updatebalances", id:postid}
                });
                request.done(function (response, textStatus, jqXHR){
                });

            });
        }
    });

    jQuery(document).on('click', '#addnewtransaction', function() {

        event.preventDefault();

        $popback =  document.getElementById('popedit');
        $popcont = document.getElementById('popcontent');
        $mainarea = document.getElementById('maincontent');
        openPopup();
    });

    function showUploadedItem (source, variable) {
        var list = document.getElementById("image-list-" + variable),
            li   = document.createElement("li"),
            img  = document.createElement("img");
        img.src = source;
        li.appendChild(img);
        list.appendChild(li);
    }

    var input = document.getElementById("licenseupload");
    if (input) {
        if (input.addEventListener) {
            tenid = jQuery("input[name*='os1']").val();
            input.addEventListener("change", function (evt) {

                formdata = false;

                if (window.FormData) {
                    formdata = new FormData();
                }


                var i = 0, len = this.files.length, img, reader, file;

                document.getElementById("response-driverslicense").innerHTML = "Uploading . . ."

                for ( ; i < len; i++ ) {
                    file = this.files[i];

                    if (!!file.type.match(/image.*/)) {
                        if ( window.FileReader ) {
                            reader = new FileReader();
                            reader.onloadend = function (e) {
                                showUploadedItem(e.target.result, "driverslicense");
                            };
                            reader.readAsDataURL(file);
                        }
                        if (formdata) {
                            formdata.append("images[]", file);
                        }
                    }
                }

                if (formdata) {
                    jQuery.ajax({
                        url:jsurl + '?action=uploadlicense&tenid=' + tenid,
                        type: "POST",
                        data: formdata,
                        processData: false,
                        contentType: false,
                        success: function (res) {
                            document.getElementById("response-driverslicense").innerHTML = res;
                            if(res == 'License Copy Successfully Uploaded.  Thank you.'){
                            	jQuery("#driverslicense").fadeOut();
				}
                        }
                    });
                }

            }, false);
        }
    }

    var input = document.getElementById("additionalidupload");
    if (input) {
        if (input.addEventListener) {
            tenid = jQuery("input[name*='os1']").val();
            input.addEventListener("change", function (evt) {

                formdata = false;

                if (window.FormData) {
                    formdata = new FormData();
                }


                var i = 0, len = this.files.length, img, reader, file;

                document.getElementById("response-additionalid").innerHTML = "Uploading . . ."

                for ( ; i < len; i++ ) {
                    file = this.files[i];

                    if (!!file.type.match(/image.*/)) {
                        if ( window.FileReader ) {
                            reader = new FileReader();
                            reader.onloadend = function (e) {
                                showUploadedItem(e.target.result, "additionalid");
                            };
                            reader.readAsDataURL(file);
                        }
                        if (formdata) {
                            formdata.append("images[]", file);
                        }
                    }
                }

                if (formdata) {
                    jQuery.ajax({
                        url:jsurl + '?action=uploadid&tenid=' + tenid,
                        type: "POST",
                        data: formdata,
                        processData: false,
                        contentType: false,
                        success: function (res) {
                            document.getElementById("response-additionalid").innerHTML = res;                            
				if(res == 'Additional ID Successfully Uploaded.  Thank you.'){
                            jQuery("#additionalid").fadeOut();
				}
                        }
                    });
                }

            }, false);
        }
    }

//    jQuery(document).on('change', '#licenseupload', function(evt) {
//        formdata = new FormData();
//
//        var i = 0, len = this.files.length, img, reader, file;
//        document.getElementById("response").innerHTML = "Uploading . . ."
//        for ( ; i < len; i++ ) {
//            file = this.files[i];
//
//            if (!!file.type.match(/image.*/)) {
//                if ( window.FileReader ) {
//                    reader = new FileReader();
//                    reader.onloadend = function (e) {
//                        showUploadedItem(e.target.result);
//                    };
//                    reader.readAsDataURL(file);
//                }
//                if (formdata) {
//                    formdata.append("images[]", file);
//                }
//            }
//        }
//
//        if (formdata) {
//            $.ajax({
//                url: "upload.php",
//                type: "POST",
//                data: formdata,
//                processData: false,
//                contentType: false,
//                success: function (res) {
//                    document.getElementById("response").innerHTML = res;
//                }
//            });
//        }
//
//    });



    jQuery(document).on('click', '.edittransaction', function() {

        event.preventDefault();

        $popback =  document.getElementById('popedit');
        $popcont = document.getElementById('popcontent');
        $mainarea = document.getElementById('maincontent');

        var id = this.id;
        openPopup(id);
    });

    function centerPopup(){
        $height = window.innerHeight;
        $width = window.innerWidth;

        if($width < 700){
            $popback.setAttribute("style","display:block; height:" + $height + "px; width:" + $width + "px");
            $popcont.setAttribute("style","display:block; top:0px; left:0px");
        } else {
            $popback.setAttribute("style","display:block; height:" + $height + "px; width:" + $width + "px");
            $popcont.setAttribute("style","display:block; top:" + ((($height/2)-250)+ jQuery(window).scrollTop()) + "px; left:" + (($width/2)-300)+ "px");
        }
    }

    function openPopup(id){

        if(id){
            request = jQuery.ajax({
                url:jsurl,
                type: "post",
                data: {action:"gettransaction", id:id}
            });
            request.done(function (response, textStatus, jqXHR){
                $popcont.innerHTML = response;
                jQuery('#etransactiondate').datepicker({
                    dateFormat: 'm/d/yy',
                    changeMonth: true,
                    changeYear: true,
                    numberOfMonths: 2,
                    showButtonPanel: true,
                    showOn: "button",
                    buttonImage: spliturl[0] + '/wp-content/plugins/wppmanage/images/iconCalendar.gif',
                    buttonImageOnly: true,
                    buttonText: "Cal"
                });

                var emethodSelect = document.getElementById("epaymethod");
                var etypeSelect = document.getElementById("etransactiontype");
                if(etypeSelect && emethodSelect){
                    if(etypeSelect.value == "payment"){
                        emethodSelect.style.display = "block";
                    } else {
                        emethodSelect.style.display = "none";
                    }
                    etypeSelect.onchange = function(){
                        if(etypeSelect.value == "payment"){
                            emethodSelect.style.display = "block";
                        } else {
                            emethodSelect.style.display = "none";
                        }
                    };
                }
            });

            $mainarea.style.opacity = .2;
            centerPopup();
        } else {
            request = jQuery.ajax({
                url:jsurl,
                type: "post",
                data: {action:"newtransaction", id:id}
            });
            request.done(function (response, textStatus, jqXHR){
                $popcont.innerHTML = response;

                var emethodSelect = document.getElementById("paymethod");
                var etypeSelect = document.getElementById("transactiontype");
                if(etypeSelect && emethodSelect){
                    if(etypeSelect.value == "payment"){
                        emethodSelect.style.display = "block";
                    } else {
                        emethodSelect.style.display = "none";
                    }
                    etypeSelect.onchange = function(){
                        if(etypeSelect.value == "payment"){
                            emethodSelect.style.display = "block";
                        } else {
                            emethodSelect.style.display = "none";
                        }
                    };
                }

                var tenantselector = document.getElementById("tenantselector");
                var aptselector = document.getElementById("aptselector");
                var tenantname = document.getElementById("tenant_name");
                var propid = document.getElementById("property_id");


                if(tenantselector && aptselector && tenantname && propid){

                    tenantname.onchange = function(){
                        if(tenantname.value != ""){
                            aptselector.style.display = "none";
                        } else {
                            aptselector.style.display = "block";
                        }
                    };
                    propid.onchange = function(){
                        if(propid.value != ""){
                            tenantselector.style.display = "none";
                        } else {
                            tenantselector.style.display = "block";
                        }
                    };
                }
            });
                $mainarea.style.opacity = .2;
                centerPopup();
        }
    }

    function closePopup(){
        if($mainarea && $popback && $popcont){
            $mainarea.style.opacity = 1;
            $popback.style.display = "none";
            $popcont.innerHTML = "";
            $popcont.style.display = "none";
        }
    }

    // initialize isotope
    var $container = jQuery('#container');
    var qsRegex;

    $container.isotope({
        itemSelector : '.isotopeitem',
        layoutMode: 'fitRows',
        animationEngine: 'css',
        getSortData : {
            name : function ( $elem ) {
                return $elem.find('.name').text();
            },
            property : function ( $elem ) {
                return $elem.find('.property').text();
            },
            amount : function ( $elem ) {
                return parseFloat($elem.find('.amount').text().slice(1));
            },
            date : function ( $elem ) {
                return parseFloat($elem.find('.calcdate').text());
            },
            description : function ( $elem ) {
                return $elem.find('.description').text();
            },
            type : function ( $elem ) {
                return $elem.find('.type').text();
            },
            named : function ( $elem ) {
                return $elem.find('.name').text();
            },
            propertyd : function ( $elem ) {
                return $elem.find('.property').text();
            },
            amountd : function ( $elem ) {
                return parseFloat($elem.find('.amount').text().slice(1));
            },
            dated : function ( $elem ) {
                return parseFloat($elem.find('.calcdate').text());
            },
            descriptiond : function ( $elem ) {
                return $elem.find('.description').text();
            },
            typed : function ( $elem ) {
                return $elem.find('.type').text();
            }
        },
        filter: function() {
            return qsRegex ? jQuery(this).text().match( qsRegex ) : true;
        }
    });

    function debounce( fn, threshold ) {
        var timeout;
        return function debounced() {
            if ( timeout ) {
                clearTimeout( timeout );
            }
            function delayed() {
                fn();
                timeout = null;
            }
            timeout = setTimeout( delayed, threshold || 100 );
        }
    }

    var $quicksearch = jQuery('#quicksearch').keyup( debounce( function() {
        qsRegex = new RegExp( $quicksearch.val(), 'gi' );
        $container.isotope();
    }, 200 ) );

    // setup filtering
    var $optionSets = jQuery('#options .option-set'),
        $optionLinks = $optionSets.find('a');

    $optionLinks.click(function(){

        var $this = jQuery(this);
        var options = {};
        var $optionSet = $this.parents('.option-set');
        var filterDiv = jQuery('#nofilter');

        newvalue = $this.attr('data-option-value');

        if(newvalue == '.all'){

            //Remove all filters
            $optionSet.find('.selected').each(function(index){
                jQuery(this).removeClass('selected');
            });
            options['filter'] = '';

            //Reset all filter main names
            jQuery('table.sublist th').each(function(){
                $filterName = jQuery(this)[0].id;
                $filterNameClean = $filterName.replace(/_/g, ' ');
                document.getElementById($filterName).innerHTML = $filterNameClean;
                document.getElementById($filterName).style.background = "#ffffff";
            });

        } else {

            var $filterType = $this.parents('.sublist').children('thead').children('tr').children('th')[0].id;

            //Hide this expanded menu
            $this.parents('tbody').toggle();

            //Remove all active filters from the current set
            $this.parents('.sublist').find('.selected').each(function(index){
                jQuery(this).removeClass('selected');
            });

            //Replace filter with correct title & add to select list
            if(newvalue == '.typeall'){
                $filterTypeClean = $filterType.replace(/_/g, ' ');
                document.getElementById($filterType).innerHTML = $filterTypeClean;
                document.getElementById($filterType).style.background = "#ffffff";

            } else {
                var textvalue = $this[0].text;
                document.getElementById($filterType).innerHTML = textvalue;
                document.getElementById($filterType).style.background = "yellow";

                //Add this filter to list of selected (if not already there)
                if ( !$this.hasClass('selected') ) {
                    $this.addClass('selected');
                }
            }
        }

        //Build list of all active filters
        var selectedlist = '';
        $optionSet.find('.selected').each(function(index){
            selectedlist += jQuery(this).attr('data-option-value');
        });
        value = selectedlist;

        value = value === 'false' ? false : value;
        options[ 'filter' ] = value;

        //Toggle reset option based on filters
        if(selectedlist == ''){
            filterDiv.hide();
            filterDiv.find('tbody').hide();
        } else{
            filterDiv.show();
            filterDiv.find('tbody').show();
        }

        $container.isotope( options );

        return false;
    });

    if(document.getElementById('location')){
        var location = document.getElementById('location').value;
    }
    var url = window.location.toString();
    var spliturl = url.split("/wp-admin/");
    var jsurl = spliturl[0] + '/wp-content/plugins/wppmanage/includes/actions.php';

    jQuery( "#transactiondate" ).datepicker({
        dateFormat: 'm/d/yy',
        changeMonth: true,
        changeYear: true,
        numberOfMonths: 2,
        showButtonPanel: true,
        showOn: "button",
        buttonImage: spliturl[0] + '/wp-content/plugins/wppmanage/images/iconCalendar.gif',
        buttonImageOnly: true,
        buttonText: "Cal"
    });

    jQuery('a.dynamicbtn').click(function(){
        jQuery('a.dynamicbtn').removeClass('selected');
        jQuery(this).addClass('selected');
        jQuery('.dynamic').hide();
        jQuery('#content-' + jQuery(this).attr('id')).fadeIn();
    });

    var methodSelect = document.getElementById("paymethod");
    var typeSelect = document.getElementById("transactiontype");
    if(typeSelect && methodSelect){
        typeSelect.onchange = function(){
            if(typeSelect.value == "payment"){
                methodSelect.style.display = "block";
            } else {
                methodSelect.style.display = "none";
            }
        };
    }

    if(location == 'ledger' || location == 'overview'){

        var tenantselector = document.getElementById("tenantselector");
        var aptselector = document.getElementById("aptselector");
        var tenantname = document.getElementById("tenant_name");
        var propid = document.getElementById("property_id");

        if(tenantselector && aptselector && tenantname && propid){

            tenantname.onchange = function(){
                if(tenantname.value != ""){
                    aptselector.style.display = "none";
                } else {
                    aptselector.style.display = "block";
                }
            };
            propid.onchange = function(){
                if(propid.value != ""){
                    tenantselector.style.display = "none";
                } else {
                    tenantselector.style.display = "block";
                }
            };
        }
    }

    jQuery(document).on('click', '#recordtransactionbutton', function() {
        recordTransaction();
    });

    jQuery(document).on('click', '#edittransactionbutton', function() {
        recordTransaction('e');
    });

    jQuery(document).on('click', '.financialsummary', function() {
        showFinancialSummary(this.id);
    });

    function showFinancialSummary(ele){
        tenid = ele.substring(9);

        if(tenid){
            request = jQuery.ajax({
                url:jsurl,
                type: "post",
                data: {action:"showfinancialsummary",id:tenid}
            });
            //TODO: Update ledger dynamically
            var message = document.getElementById("financialsummary-" + tenid);
            request.done(function (response, textStatus, jqXHR){
               message.innerHTML= response;
            });
            request.fail(function (jqXHR, textStatus, errorThrown){
                message.innerHTML= "Payment Not Added";
            });
            event.preventDefault();
        }

    }

    function recordTransaction(prefix){

        prefix = (typeof prefix === "undefined") ? "" : prefix;

        var data = setValues(prefix);

        if(data){ processTransaction(data, prefix); }

        function setValues(pre){

            var entity ='';
            var id = '';
            var name = '';
            var postid = document.getElementById(pre + 'editid').innerHTML;
            var tenantname = document.getElementById(pre + "tenant_name");
            var propid = document.getElementById("property_id");

            if(location == 'ledger' || location == 'overview'){

                var tenant = tenantname.value.split(".");

                if(jQuery('#property_id').val()){
                    entity = "properties";
                    id = propid.value;
                    name = document.getElementById(pre + 'tenant_name').value;
                } else {
                    entity = "tenants";
                    id = tenant[0];
                    name = tenant[1];
                    if(name == null || name == '' || !name){
                        alert("Please enter a tenant or property");
                        return false;
                    }
                }
            } else {
                entity = document.getElementById(pre + 'post_type').value;
                id = document.getElementById(pre + 'post_ID').value;
                name = document.getElementById(pre + 'tenant_name').value;
            }

            var date = document.getElementById(pre + 'transactiondate').value;

            var amount = document.getElementById(pre + 'transactionamount').value;
            if(amount == null || amount == '' || !amount){
                alert("Please enter a correct amount");
                return false;
            }
            var type =  document.getElementById(pre + 'transactiontype').value;
            if(type == 0){
                alert("Please select a correct type");
                return false;
            }
            if(entity == "properties" && type == "payment") {
                alert("Payments cannot be applied to properties");
                return false;
            }
            var method =  document.getElementById(pre + 'transactionmethod').value;
            if(type == "payment" && method == 0){
                alert("Please select a correct method");
                return false;
            }
            var description = document.getElementById(pre + 'transactiondescription').value;

            var data = {amount:amount,method:method,type:type,description:description,id:id,name:name,entity:entity,date:date,postid:postid};

            return data;
        }

        function processTransaction(data, prefix){

            request = jQuery.ajax({
                url:jsurl,
                type: "post",
                data: {action:"addtransaction",amount:data.amount,method:data.method,type:data.type,description:data.description,id:data.id,name:data.name,entity:data.entity,date:data.date,postid:data.postid}
            });

            //TODO: Update ledger dynamically

            var emessage = document.getElementById("etransactionstatus");
            var message = document.getElementById("transactionstatus");

            request.done(function (response, textStatus, jqXHR){
                var popup = false;
                if(jQuery('#popup').val()){
                    popup = true;
                }

                if(jQuery('#etransactionstatus').val()){
                    emessage.className = message.className + " success";
                    emessage.innerHTML= "<br /> Transaction Edited Successfully";
                } else if(jQuery('#transactionstatus').val()){
                    message.className = message.className + " success";
                    message.innerHTML= "<br /> Transaction Added Successfully";
                }

                if(location == 'ledger' || location == 'overview'){
                    if(location == 'ledger'){
                        updateLedger();
                    } else {
                        updateLedger(popup);
                    }
                }

                if(prefix == 'e' || popup){
                    closePopup();
                } else {
                    clearFields();
                }

            });
            request.fail(function (jqXHR, textStatus, errorThrown){

                message.className = message.className + " failure";
                message.innerHTML= "Payment Not Added";

                alert('There was an error adding your transaction.  Please contact support.');
            });
//        request.always(function () {
//            // reenable the inputs
//            alert('always');
//        });

            // prevent default posting of form
            event.preventDefault();

        }
    }
});
