jQuery(document).ready(function() {

    /*
    *  Qualify dollar inputs for amount fields
    */

    jQuery(document).on('keypress', '.dollarinput', function(event) {
        var pos = jQuery(jQuery(this)).caret();
        var charCode = (event.which) ? event.which : event.keyCode;
        var value = jQuery(this).val();
        var character = String.fromCharCode(charCode);
        var newValue = [value.slice(0, pos), character, value.slice(pos)].join('');

        //if there is a decimal with two digits after it DONT LET ME ADD MORE
        if( !(value.charAt(value.length - 3) == "." && pos > value.length - 3) && (!isNaN(character)) || (charCode == 46 && !(value.indexOf(".") != -1)) || (charCode == 45 && !(value.indexOf("-") != -1) && (value== "" || pos == 0)) ){
            jQuery(this).val(newValue);
            jQuery(this).caret(pos+1);
        } else {
            jQuery(this).val(value);
            jQuery(this).caret(pos);
        }
    });
});