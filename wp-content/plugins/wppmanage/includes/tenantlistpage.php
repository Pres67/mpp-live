<?php
$current_user = wp_get_current_user();
if (!current_user_can('edit_users')) die("Access Denied");

?>
<script type="text/javascript">
    function isNumberKey(evt, obj) {

        var charCode = (evt.which) ? evt.which : event.keyCode
        var value = obj.value;
        if (charCode == 45 && value == "") return true;
        var dotcontains = value.indexOf(".") != -1;
        if (dotcontains)
            if (charCode == 46) return false;
        if (charCode == 46) return true;
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
</script>
<style>
    #popedit, #popcontent {
        display: none;
    }

    .popedit {
        position: absolute;
        top: 0;
        left: -10px;
        z-index: 1;
    }

    .popcontent {
        z-index: 2;
        position: absolute;
        max-width: 300px;
    }

    input {
        width: 100%;
        margin: 0px;
        padding: 0px;
    }

    th {
        cursor: pointer;
        padding: 10px;
        background-color: #fff;
    }

    th.reset {
        background-color: #000;
    }

    th.reset a {
        padding: 10px;
        color: #fff;
        text-decoration: none;
    }

    .sorting td {
        padding-left: 5px;
        background-color: #fff;
    }

    .sorting td a {
        display: block;
        padding-top: 10px;
        padding-bottom: 10px;
        text-decoration: none;
    }

    .isotopeitem td {
        padding-left: 5px;
        background-color: #fff;
    }

    .isotopeitem td a {
        display: block;
        padding-top: 10px;
        padding-bottom: 10px;
        text-decoration: none;
    }

    .arrows {
        float: right;
    }

    .sorting td .arrows a {
        padding: 0;
    }

    .sorting td .arrows a:focus, #filters td a:focus {
        box-shadow: none;
    }

    .toparrow {
        padding: 5px;
    }

    .bottomarrow {
        padding: 5px;
    }

    .sortname {
        float: left;
        margin-top: 18px;
    }

    .sortable.active {
        background-color: yellow;
    }

    #filters td a {
        text-decoration: none;
        display: block;
        padding: 3px 3px;
    }

    #filters td a:hover {
        background-color: #eaeaea;
    }

    .searchbox {
        height: 37px;
        margin-top: 3px;
        margin-left: 3px;
        width: 700px;
        margin-bottom: 15px;
        padding: 8px;
    }

    td.linker a {
        padding: 10px;
    }

    .tenantdetails {
        display: none;
        padding:15px;
    }
    .tenantdetailarea{
        padding:20px !important;
        background-color:#eaeaea;
        border:1px solid black;
    }

    .ratetitle {
        float: left;
    }

    .tenantinfo input {
        float: left;
        width: auto;
    }

    .tenantlist {
        border-collapse: collapse;
    }

    .tenantlist td {
        background-color: #fff;
        padding: 5px;
    }

    .tenantlist tr {
        border-bottom: 1px solid #eaeaea;
    }

    .actionbutton {
        background-color: blue;
        color: #fff;
    }
    .progress{
        display:none;
        font-size:10px;
    }
</style>

<div style="width:725px; min-height:1000px; margin-bottom: 55px; float:left; margin-right:25px;">
    <?php $tenantURL = admin_url('edit.php?post_type=tenants', 'http'); ?>

    <h2>Current Tenants <span style="font-size:9px;"><a href="<?php echo $tenantURL; ?>">(SEE ALL TENANTS)</a></span>
    </h2>

    <div id="tenantlist">
        <table class="tenantlist" style="background-color:#fff;">
            <tr>
                <td width="25"></td>
                <td width="125">Name</td>
                <td width="100">Property</td>
                <td width="100">Phone</td>
                <td width="225">Email/College <span id="showcollege" style="cursor:pointer;">(click to switch)</span></td>
                <td width="50">Dep</td>
                <td width="50">Bal</td>
            </tr>

            <?php
            //Filter Setup
            $args = array(
                'post_type' => 'tenants',
                'post_status' => 'publish',
                'posts_per_page' => -1,
                'orderby' => 'meta_value_num',
                'meta_key' => '_mpp_property_value',
                'order' => 'asc',
            );
            $tenants = get_posts($args);

            foreach ($tenants as $tenant) {

                $tenant_meta = get_post_meta($tenant->ID);

               if($tenant_meta['_mpp_tenantstatus_value'][0] == '2'){

                $currenttenants .=
                 '<tr>'
                 . '<td width="25" style="cursor:pointer;"><a href="' . admin_url() . 'post.php?post=' . $tenant->ID . '&action=edit"><img style="width:12px;" src="' . plugins_url() . '/wppmanage/images/Modify.png" /></a></td>'
                 . '<td width="125" class="name" >' . $tenant->post_title . '</td>'
                 . '<td width="100" class="property">' . get_the_title($tenant_meta['_mpp_property_value'][0]) . '</td>'
                 . '<td width="100" class="phone">' . $tenant_meta['_mpp_phone_value'][0] . '</td>'
                 . '<td width="225" class="email">' . $tenant_meta['_mpp_email_value'][0] . '</td>'
                 . '<td width="225" class="college" style="display:none;">' . $tenant_meta['_mpp_college_value'][0] . '</td>'
                 . '<td width="50" class="left">' . get_deposit_balance($tenant->ID, 'html') . '</td>'
                 . '<td width="50" class="due">' . get_balance($tenant->ID, 'html') . '</td>'
                 . '</td>
                        </tr>
                        <tr class="tenantdetails">
                        <td colspan="8" class="tenantdetailarea">
                        <div class="tenantinfo">
                        </div>

                        <button type="button" id="' . $tenant->ID . '" class="actionbutton finances">Print Deposit Summary</button><hr /><br />' . showDepositLedger($tenant->ID) . '<br /><hr />' . showTenantLedger($tenant->ID) . ' <br />

                        </td>
                        </td></tr><div style="margin-bottom:25px;"></div>';

               } else {

                   $futuretenants .=
                   '<tr class="futuretenants">'
                   . '<td width="25"><a href="' . admin_url() . 'post.php?post=' . $tenant->ID . '&action=edit"><img style="width:12px;" src="' . plugins_url() . '/wppmanage/images/Modify.png" /></a></td>'
                   . '<td width="125" class="name" style="cursor:pointer;">' . $tenant->post_title . '</td>'
                   . '<td width="100" class="property">' . get_the_title($tenant_meta['_mpp_property_value'][0]) . '</td>'
                   . '<td width="100" class="phone">' . $tenant_meta['_mpp_phone_value'][0] . '</td>'
                   . '<td width="225" class="email">' . $tenant_meta['_mpp_email_value'][0] . '</td>'
                   . '<td width="50" class="left">' . get_deposit_balance($tenant->ID, 'html') . '</td>'
                    .'<td width="50" class="due">' . get_balance($tenant->ID, 'html') . '</td>'
                    .'<td class="progress" colspan="7">
                    APP: <input type="checkbox"'. ($tenant_meta['application'][0] == 1 ? "checked":"") .'></input>
                    TOUR: <input type="checkbox"'. ($tenant_meta['tour'][0] == 1 ? "checked":"") .'></input>
                    SIGN: <input type="checkbox"'. ($tenant_meta['contract_signed'][0] == 1 ? "checked":"") .'></input>
                    SENT: <input type="checkbox"'. ($tenant_meta['contract_sent'][0] == 1 ? "checked":"") .'></input>
                    DEP: <input type="checkbox"'. ($tenant_meta['deposit_paid'][0] == 1 ? "checked":"") .'></input>
                    F&L: <input type="checkbox"'. ($tenant_meta['first_&_last_paid'][0] == 1 ? "checked":"") .'></input>
                    IN: <input type="checkbox"'. ($tenant_meta['moved_in'][0] == 1 ? "checked":"") .'></input>
                    UPCON: <input type="checkbox"'. (!empty($tenant_meta['signed_contract'][0]) ? "checked":"") .'></input>
                    UPDL: <input type="checkbox"'. (!empty($tenant_meta['drivers_license_copy'][0]) ? "checked":"") .'></input>
                    SSN: <input type="checkbox"'. (!empty($tenant_meta['social_security_number'][0]) ? "checked":"") .'></input>
                        </td>'
                   . '</tr>'
                   . '
                            <tr class="tenantdetails">
                            <td colspan="8">
                            <div class="tenantinfo">
                            </div>

                            <button type="button" id="' . $tenant->ID . '" class="actionbutton finances">Print Deposit Summary</button><hr /><br />' . showDepositLedger($tenant->ID) . '<br /><hr />' . showTenantLedger($tenant->ID) . ' <br />

                            </td>
                            </td></tr>';

               }
            }

            echo $currenttenants;
            ?>
        </table>
    </div>

    <h2>Future Tenants</h2>

    <button class="showprogress">Show Tenant Progress</button>

    <div id="tenantlist">
        <table class="tenantlist futuretenants style="background-color:#fff;">
            <tr>
                <td width="25"></td>
                <td width="125">Name</td>
                <td width="100" class="property">Property</td>
                <td width="100" class="phone">Phone</td>
                <td width="225" class="email">Email</td>
                <td width="50" class="left">Dep</td>
                <td width="50" class="due">Bal</td>
                <td width="600" class="progress">Progress</td>
            </tr>

            <?php
            echo $futuretenants;
            ?>
        </table>
    </div>
</div>
<div>


</div>
