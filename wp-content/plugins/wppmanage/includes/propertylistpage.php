<?php
$current_user = wp_get_current_user();
if (!current_user_can('edit_users')) die("Access Denied");

?>
<style>
    #popedit, #popcontent {
        display: none;
    }

    .popedit {
        position: absolute;
        top: 0;
        left: -10px;
        z-index: 1;
    }

    .popcontent {
        z-index: 2;
        position: absolute;
        max-width: 300px;
    }

    input {
        width: 100%;
        margin: 0px;
        padding: 0px;
    }

    th {
        cursor: pointer;
        padding: 10px;
        background-color: #fff;
    }

    th.reset {
        background-color: #000;
    }

    th.reset a {
        padding: 10px;
        color: #fff;
        text-decoration: none;
    }

    .sorting td {
        padding-left: 5px;
        background-color: #fff;
    }

    .sorting td a {
        display: block;
        padding-top: 10px;
        padding-bottom: 10px;
        text-decoration: none;
    }

    .isotopeitem td {
        padding-left: 5px;
        background-color: #fff;
    }

    .isotopeitem td a {
        display: block;
        padding-top: 10px;
        padding-bottom: 10px;
        text-decoration: none;
    }

    .arrows {
        float: right;
    }

    .sorting td .arrows a {
        padding: 0;
    }

    .sorting td .arrows a:focus, #filters td a:focus {
        box-shadow: none;
    }

    .toparrow {
        padding: 5px;
    }

    .bottomarrow {
        padding: 5px;
    }

    .sortname {
        float: left;
        margin-top: 18px;
    }

    .sortable.active {
        background-color: yellow;
    }

    #filters td a {
        text-decoration: none;
        display: block;
        padding: 3px 3px;
    }

    #filters td a:hover {
        background-color: #eaeaea;
    }

    .searchbox {
        height: 37px;
        margin-top: 3px;
        margin-left: 3px;
        width: 700px;
        margin-bottom: 15px;
        padding: 8px;
    }

    td.linker a {
        padding: 10px;
    }

    .tenantdetails {
        display: none;
        padding:15px;
    }
    .tenantdetailarea{
        padding:20px !important;
        background-color:#eaeaea;
        border:1px solid black;
    }

    .ratetitle {
        float: left;
    }

    .tenantinfo input {
        float: left;
        width: auto;
    }

    .tenantlist {
        border-collapse: collapse;
    }

    .tenantlist td {
        background-color: #fff;
        padding: 5px;
    }

    .tenantlist tr {
        border-bottom: 1px solid #eaeaea;
    }

    .actionbutton {
        background-color: blue;
        color: #fff;
    }
</style>

<div style="width:725px; min-height:1000px; margin-bottom: 55px; float:left; margin-right:25px;">
    <h2>Properties <span style="font-size:9px;"></span>
    </h2>

    <div id="tenantlist">
        <table class="tenantlist" style="background-color:#fff;">
            <tr>
                <td width="25"></td>
                <td width="125">Unit</td>
                <td width="200">Address</td>
                <td width="100">Cap</td>
                <td width="225">Rate</td>
            </tr>

            <?php
            //Filter Setup
            $args = array(
                'post_type' => 'properties',
                'post_status' => 'publish',
                'posts_per_page' => -1,
                'orderby' => 'title',
                'order' => 'asc',
            );
            $properties = get_posts($args);

            foreach ($properties as $property) {

                $property_meta = get_post_meta($property->ID);

                echo '<tr>';
                echo '<td width="25"><a href="' . admin_url() . 'post.php?post=' . $property->ID . '&action=edit"><img style="width:12px;" src="' . plugins_url() . '/wppmanage/images/Modify.png" /></a></td>';
                echo '<td width="125" class="propname">' . $property->post_title . '</td>';
                echo '<td width="200" class="property">' . $property_meta['_mpp_address_value'][0] . ',  '. $property_meta['_mpp_city_value'][0] .',  '. $property_meta['_mpp_state_value'][0] .' ' . $property_meta['_mpp_zip_value'][0]. '</td>';
                echo '<td width="100" class="phone">' . $property_meta['_mpp_capacity_value'][0] . '</td>';
                echo '<td width="225" class="email">' . $property_meta['_mpp_yrrent_value'][0] . '</td>';

                echo '</td>
                        </tr>
                        <tr class="tenantdetails">
                        <td colspan="8" class="tenantdetailarea">
                        <div class="tenantinfo">
                        </div>

                        <button type="button" id="' . $property->ID . '" class="actionbutton finances">Print Deposit Summary</button><hr /><br />' . showDepositLedger($property->ID) . '<br /><hr />' . showTenantLedger($property->ID) . ' <br />

                        </td>
                        </td></tr><div style="margin-bottom:25px;"></div>';

            }
            ?>
        </table>
    </div>

</div>