<style>
    .subinfo{
        width:250px;
       margin-left:50px;
       margin-top: 20px;
        font-weight:bold;
        text-align:right;
    }
    .method{
        padding-left:1%;
        width:48%;
        float:left;
        text-align: center;
    }
    .method h2{
        cursor:pointer;
    }
    .clear{
        float:none;
        clear:both;
        height:25px;
        width:100%;
    }
    #contentcontainernav{
        width:90%;
        border-bottom:1px solid black;
    }
   #contentcontainernav li {
    display:inline;
    font-weight:bold;
    cursor:pointer;
    color: #76daff;
    }
    #contentcontainernav li a:hover{
        background-color: #d3d3d3;
    }
    #contentcontainernav li.selected {
        background-color: #cccccc;
        color:#000000;
    }
    #contentcontainernav li.selected:after {
        content: "";
        position: absolute;
        top: 100%;
        left: 50%;
        z-index: 1000;
        border-top: 8px solid #ccc;
        border-left: 8px solid transparent;
        border-right: 8px solid transparent;
        margin-left: -8px;
    }
    #contentcontainernav li a {
        text-decoration: none;
        padding: 0.25em 1em;
        color: #000;
    }
    #contentcontainernav li a.selected {
        background-color: #cccccc;
        color:#000000;
    }
    .dynamic{
        width:95%;
        height:95%;
    }
    table.divided{
        border-collapse: collapse;
    }
    table.divided tr{
        border-bottom:  1px solid #eaeaea;
    }
    /* start css for tenant page */
    #popedit, #popcontent {
        display: none;
    }

    .popedit {
        position: absolute;
        top: 0;
        left: -10px;
        z-index: 1;
    }

    .popcontent {
        z-index: 2;
        position: absolute;
        max-width: 300px;
    }

    input {
        width: 100%;
        margin: 0px;
        padding: 0px;
    }

    th {
        cursor: pointer;
        padding: 10px;
        background-color: #fff;
    }

    th.reset {
        background-color: #000;
    }

    th.reset a {
        padding: 10px;
        color: #fff;
        text-decoration: none;
    }

    .sorting td {
        padding-left: 5px;
        background-color: #fff;
    }

    .sorting td a {
        display: block;
        padding-top: 10px;
        padding-bottom: 10px;
        text-decoration: none;
    }

    .isotopeitem td {
        padding-left: 5px;
        background-color: #fff;
    }

    .isotopeitem td a {
        display: block;
        padding-top: 10px;
        padding-bottom: 10px;
        text-decoration: none;
    }

    .arrows {
        float: right;
    }

    .sorting td .arrows a {
        padding: 0;
    }

    .sorting td .arrows a:focus, #filters td a:focus {
        box-shadow: none;
    }

    .toparrow {
        padding: 5px;
    }

    .bottomarrow {
        padding: 5px;
    }

    .sortname {
        float: left;
        margin-top: 18px;
    }

    .sortable.active {
        background-color: yellow;
    }

    #filters td a {
        text-decoration: none;
        display: block;
        padding: 3px 3px;
    }

    #filters td a:hover {
        background-color: #eaeaea;
    }

    .searchbox {
        height: 37px;
        margin-top: 3px;
        margin-left: 3px;
        width: 700px;
        margin-bottom: 15px;
        padding: 8px;
    }

    td.linker a {
        padding: 10px;
    }

    .tenantdetails {
        display: none;
        padding:15px;
    }
    .tenantdetailarea{
        padding:20px !important;
        background-color:#eaeaea;
        border:1px solid black;
    }

    .ratetitle {
        float: left;
    }

    .tenantinfo input {
        float: left;
        width: auto;
    }

    .tenantlist {
        border-collapse: collapse;
    }

    .tenantlist td {
        background-color: #fff;
        padding: 5px;
    }

    .tenantlist tr {
        border-bottom: 1px solid #eaeaea;
    }

    .actionbutton {
        background-color: blue;
        color: #fff;
    }
    .progress{
        display:none;
        font-size:10px;
    }
/* start ledger css */
    #container{
        padding-left:1px;
    }
    #popedit, #popcontent {
        display:none;
    }
    .popedit{
        position:absolute;
        top:0;
        left:-10px;
        z-index:1;
    }
    .popcontent{
        z-index:2;
        position:absolute;
        max-width:300px;
    }
    input{
        width:100%;
        margin:0px;
        padding:0px;
    }
    th{
        cursor:pointer;
        padding:10px;
        background-color:#fff;
    }
    th.reset{
        background-color:#000;
    }
    th.reset a{
        padding:10px;
        color:#fff;
        text-decoration: none;
    }
    .sorting td{
        padding-left:5px;
        background-color:#fff;
    }
    .sorting td a{
        display:block;
        padding-top:10px;
        padding-bottom:10px;
        text-decoration: none;
    }
    .isotopeitem div{
        padding:5px 1px 5px 5px;
        background-color:#fff;
        float:left;
        border:1px solid #f1f1f1;
        display:table-cell;
    }
    .isotopeitem div.clearall{
        padding:0;
        border:none;
    }
    .isotopeitem div a{
        display:block;
        padding-top:10px;
        padding-bottom:10px;
        text-decoration: none;
    }
    .isotopeitem div.linker{
        background-color: transparent;
        padding-bottom:0;
        padding-top:3px;
    }
    .isotopeitem div.linker a{
        padding:3px;
    }
    .arrows{
        float:right;
    }
    .sorting td .arrows a{
        padding:0;
    }
    .sorting td .arrows a:focus, #filters td a:focus{
        box-shadow:none;
    }
    .toparrow{
        padding: 5px;
    }
    .bottomarrow{
        padding: 5px;
    }
    .trans .sortname{
        margin-top:5px;
    }
    .sortname{
        float:left;
        margin-top:18px;
    }
    .sortable.active{
        background-color:yellow;
    }
    #filters td a{
        text-decoration: none;
        display:block;
        padding: 3px 3px;
    }
    #filters td a:hover{
        background-color:#eaeaea;
    }
    .searchbox.ledger{
        height: 37px;
        margin-top: 3px;
        margin-left: 3px;
        width: 400px;
        margin-bottom: 15px;
        padding:8px;
    }
    .edittransaction, .deletetransaction{
        cursor:pointer;
    }
    .maintitles{
        width:500px;
    }
    #poststuff .postbox-container.recordform{
        margin-top:147px; float:right; width: 282px;
    }
    #poststuff #post-body.columns-2.translist{
        float:right; margin-right:25px;
    }

    @media (max-width: 1420px) {
        #poststuff .postbox-container.recordform{
            clear:both;
            float:none;
            margin-top:0;
            width:100%;
        }
        #poststuff #post-body.columns-2.translist{
            clear:both;
            float:none;
            width:100%;
        }
    }

    @media (max-width: 1050px) {
        .property{
            display:none !important;
        }
    }

    @media (max-width: 815px) {
        .description{
            display:none !important;
        }
    }
/* css for properties list */
    #popedit, #popcontent {
        display: none;
    }

    .popedit {
        position: absolute;
        top: 0;
        left: -10px;
        z-index: 1;
    }

    .popcontent {
        z-index: 2;
        position: absolute;
        max-width: 300px;
    }

    input {
        width: 100%;
        margin: 0px;
        padding: 0px;
    }

    th {
        cursor: pointer;
        padding: 10px;
        background-color: #fff;
    }

    th.reset {
        background-color: #000;
    }

    th.reset a {
        padding: 10px;
        color: #fff;
        text-decoration: none;
    }

    .sorting td {
        padding-left: 5px;
        background-color: #fff;
    }

    .sorting td a {
        display: block;
        padding-top: 10px;
        padding-bottom: 10px;
        text-decoration: none;
    }

    .isotopeitem td {
        padding-left: 5px;
        background-color: #fff;
    }

    .isotopeitem td a {
        display: block;
        padding-top: 10px;
        padding-bottom: 10px;
        text-decoration: none;
    }

    .arrows {
        float: right;
    }

    .sorting td .arrows a {
        padding: 0;
    }

    .sorting td .arrows a:focus, #filters td a:focus {
        box-shadow: none;
    }

    .toparrow {
        padding: 5px;
    }

    .bottomarrow {
        padding: 5px;
    }

    .sortname {
        float: left;
        margin-top: 18px;
    }

    .sortable.active {
        background-color: yellow;
    }

    #filters td a {
        text-decoration: none;
        display: block;
        padding: 3px 3px;
    }

    #filters td a:hover {
        background-color: #eaeaea;
    }

    .searchbox {
        height: 37px;
        margin-top: 3px;
        margin-left: 3px;
        width: 700px;
        margin-bottom: 15px;
        padding: 8px;
    }

    td.linker a {
        padding: 10px;
    }

    .tenantdetails {
        display: none;
        padding:15px;
    }
    .tenantdetailarea{
        padding:20px !important;
        background-color:#eaeaea;
        border:1px solid black;
    }

    .ratetitle {
        float: left;
    }

    .tenantinfo input {
        float: left;
        width: auto;
    }

    .tenantlist {
        border-collapse: collapse;
    }

    .tenantlist td {
        background-color: #fff;
        padding: 5px;
    }

    .tenantlist tr {
        border-bottom: 1px solid #eaeaea;
    }

    .actionbutton {
        background-color: blue;
        color: #fff;
    }
</style>
<script type="text/javascript">
        jQuery(document).ready(function() {
            jQuery('a.dynamicbtn').click(function(){
                jQuery('a.dynamicbtn').removeClass('selected');
                jQuery(this).addClass('selected');
                   jQuery('.dynamic').hide();
                   jQuery('#content-' + jQuery(this).attr('id')).fadeIn();
            });




        });

        function isNumberKey(evt, obj) {

            var charCode = (evt.which) ? evt.which : event.keyCode
            var value = obj.value;
            if (charCode == 45 && value == "") return true;
            var dotcontains = value.indexOf(".") != -1;
            if (dotcontains)
                if (charCode == 46) return false;
            if (charCode == 46) return true;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
</script>

<?php

$current_user = wp_get_current_user();

if(current_user_can('edit_users')){


    $args = array(
        'post_type' => 'tenants',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'orderby' => 'title',
        'order' => 'asc'
    );
    $tenants = get_posts($args);
    $args = array(
        'post_type' => 'properties',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'orderby' => 'title',
        'order' => 'asc'
    );
    $properties = get_posts($args);

    $tasklogURL = admin_url( 'admin.php?page=wppmanage_log', 'http' );
    $schedules = wp_get_schedules();
    $cron = _get_cron_array();
    date_default_timezone_set('America/Denver');

    $user_position = 'Apartment Manager';
    if(current_user_can('manage_options')){
        $user_position = 'Administrator ';
    }

    echo '
    <h2>Welcome '.$user_position.'</h2><h4>If you are not an MPP Administrator and do not have permission to view this page, please leave immediately. This information is confidential and private. </h4>';
    echo '<input type="hidden" id="location" name="location" value="overview">';

    echo '<button type="button" id="addnewtransaction" class="actionbutton">Record Transaction</button>';
    echo '<div class="popedit" id="popedit">
            </div>
            <div class="popcontent" id="popcontent">
            </div>';

    echo '<div id="maincontent">';
    ?>
    <div class="popedit" id="popedit">
    </div>
    <div class="popcontent" id="popcontent">
    </div>
    <div id="tenantlists" style="width:725px; min-height:1000px; margin-bottom: 55px; float:left; margin-right:25px;">
        <?php $tenantURL = admin_url('edit.php?post_type=tenants', 'http'); ?>

        <h2>Current Tenants <span style="font-size:9px;"><a href="<?php echo $tenantURL; ?>">(SEE ALL TENANTS)</a></span>
        </h2>

        <div id="tenantlist">
            <table class="tenantlist sortable" style="background-color:#fff;">
                <tr>
                    <td width="40"></td>
                    <th width="125">Name</th>
                    <th width="100">Property</th>
                    <th width="100">Phone</th>
                    <th width="225" class="sorttable_nosort">Email/College <span id="showcollege" style="cursor:pointer;">(click to switch)</span></th>
                    <th width="60">Dep</th>
                    <th width="50">Bal</th>
                </tr>

                <?php
                foreach ($tenants as $tenant) {

                    $tenant_meta = get_post_meta($tenant->ID);

                    $current_user = wp_get_current_user();
                    $user = get_user_by('email', $tenant_meta['_mpp_email_value'][0]);
                    if($user){
                        $switchurlsource = switch_to_url2($current_user, $user->ID);
                        $switchurl = '<a style="padding-left:2px;" href="'.$switchurlsource.'"><img style="width:12px;" src="' . plugins_url() . '/wppmanage/images/becomeuser.png" /></a>';
                    } else {
                        $switchurl = '';
                    }

                    if($tenant_meta['_mpp_tenantstatus_value'][0] == '2'){

                        $currenttenants .=
                            '<tr>'
                            . '<td width="40" style="cursor:pointer;"><a href="' . admin_url() . 'post.php?post=' . $tenant->ID . '&action=edit"><img style="width:12px;" src="' . plugins_url() . '/wppmanage/images/Modify.png" /></a>' . $switchurl . '</td>'
                            . '<td width="125" class="name" style="cursor:pointer;">' . $tenant->post_title . '</td>'
                            . '<td width="100" class="property">' . get_the_title($tenant_meta['_mpp_property_value'][0]) . '</td>'
                            . '<td width="100" class="phone">' . $tenant_meta['_mpp_phone_value'][0] . '</td>'
                            . '<td width="225" class="email">' . $tenant_meta['_mpp_email_value'][0] . '</td>'
                            . '<td width="225" class="college" style="display:none;">' . $tenant_meta['_mpp_college_value'][0] . '</td>'
                            . '<td width="50" class="left" id="dep-' .$tenant->ID .'" tenid="' .$tenant->ID .'">' . $tenant_meta['_mpp_depositremaining_value'][0] . '</td>'
                            . '<td width="50" class="due" id="bal-' .$tenant->ID .'" tenid="' .$tenant->ID .'">' . $tenant_meta['_mpp_currentbalance_value'][0] . '</td>'
                            . '</td>
                            </tr>
                            <tr class="tenantdetails">
                            <td colspan="8" class="tenantdetailarea">
                            <div class="tenantinfo">
                            </div>
                                <button type="button" id="' . $tenant->ID . '" class="actionbutton finances">Print Deposit Summary</button><button type="button" id="finances-' . $tenant->ID . '" class="actionbutton financialsummary">Show Financials Summary</button><hr /><br />
                                <div id="financialsummary-' . $tenant->ID .'"></div>
                                </td>
                                </td></tr><div style="margin-bottom:25px;">
                            </div>';

                    } elseif($tenant_meta['_mpp_tenantstatus_value'][0] == '1') {

                        $futuretenants .=
                            '<tr class="futuretenants">'
                            . '<td width="40"><a href="' . admin_url() . 'post.php?post=' . $tenant->ID . '&action=edit"><img style="width:12px;" src="' . plugins_url() . '/wppmanage/images/Modify.png" /></a>' . $switchurl . '</td>'
                            . '<td width="125" class="name" style="cursor:pointer;">' . $tenant->post_title . ' </td>'
                            . '<td width="100" class="property">' . get_the_title($tenant_meta['_mpp_property_value'][0]) . '</td>'
                            . '<td width="100" class="phone">' . $tenant_meta['_mpp_phone_value'][0] . '</td>'
                            . '<td width="225" class="email">' . $tenant_meta['_mpp_email_value'][0] . '</td>'
                            . '<td width="225" class="college" style="display:none;">' . $tenant_meta['_mpp_college_value'][0] . '</td>'
                            . '<td width="50" class="left" id="dep-' .$tenant->ID .'" tenid="' .$tenant->ID .'">' . $tenant_meta['_mpp_depositremaining_value'][0] . '</td>'
                            .'<td width="50" class="due" id="bal-' .$tenant->ID .'" tenid="' .$tenant->ID .'">' . $tenant_meta['_mpp_currentbalance_value'][0] . '</td>'
                            .'<td class="progress" colspan="7">
                        APP: <input type="checkbox"'. ($tenant_meta['application'][0] == 1 ? "checked":"") .'></input>
                        TOUR: <input type="checkbox"'. ($tenant_meta['tour'][0] == 1 ? "checked":"") .'></input>
                        SIGN: <input type="checkbox"'. ($tenant_meta['contract_signed'][0] == 1 ? "checked":"") .'></input>
                        SENT: <input type="checkbox"'. ($tenant_meta['contract_sent'][0] == 1 ? "checked":"") .'></input>
                        DEP: <input type="checkbox"'. ($tenant_meta['deposit_paid'][0] == 1 ? "checked":"") .'></input>
                        F&L: <input type="checkbox"'. ($tenant_meta['first_&_last_paid'][0] == 1 ? "checked":"") .'></input>
                        IN: <input type="checkbox"'. ($tenant_meta['moved_in'][0] == 1 ? "checked":"") .'></input>
                        UPCON: <input type="checkbox"'. (!empty($tenant_meta['signed_contract'][0]) ? "checked":"") .'></input>
                        UPDL: <input type="checkbox"'. (!empty($tenant_meta['drivers_license_copy'][0]) ? "checked":"") .'></input>
                        SSN: <input type="checkbox"'. (!empty($tenant_meta['social_security_number'][0]) ? "checked":"") .'></input>
                            </td>'
                            . '</tr>'
                            . '
                                <tr class="tenantdetails">
                                <td colspan="8">
                                <div class="tenantinfo">
                                </div>
                                <button type="button" id="' . $tenant->ID . '" class="actionbutton finances">Print Deposit Summary</button><button type="button" id="finances-' . $tenant->ID . '" class="actionbutton financialsummary">Show Financials Summary</button><hr /><br />
                                <br />
                                <div id="financialsummary-' . $tenant->ID .'"></div>
                                </td>
                                </td></tr>';
                    } elseif($tenant_meta['_mpp_tenantstatus_value'][0] == '3' && ($tenant_meta['_mpp_currentbalance_value'][0] > 0 || $tenant_meta['_mpp_depositremaining_value'][0] > 0)) {
                        $pasttenants .=
                            '<tr class="futuretenants">'
                            . '<td width="40"><a href="' . admin_url() . 'post.php?post=' . $tenant->ID . '&action=edit"><img style="width:12px;" src="' . plugins_url() . '/wppmanage/images/Modify.png" /></a>' . $switchurl . '</td>'
                            . '<td width="125" class="name" style="cursor:pointer;">' . $tenant->post_title . '</td>'
                            . '<td width="100" class="property">' . get_the_title($tenant_meta['_mpp_property_value'][0]) . '</td>'
                            . '<td width="100" class="phone">' . $tenant_meta['_mpp_phone_value'][0] . '</td>'
                            . '<td width="225" class="email">' . $tenant_meta['_mpp_email_value'][0] . '</td>'
                            . '<td width="225" class="college" style="display:none;">' . $tenant_meta['_mpp_college_value'][0] . '</td>'
                            . '<td width="50" class="left" id="dep-' .$tenant->ID .'" tenid="' .$tenant->ID .'">' . $tenant_meta['_mpp_depositremaining_value'][0] . '</td>'
                            .'<td width="50" class="due" id="bal-' .$tenant->ID .'" tenid="' .$tenant->ID .'">' . $tenant_meta['_mpp_currentbalance_value'][0] . '</td>'
                            . '</tr>'
                            . '
                                <tr class="tenantdetails">
                                <td colspan="8">
                                <div class="tenantinfo">
                                </div>
                                <button type="button" id="' . $tenant->ID . '" class="actionbutton finances">Print Deposit Summary</button><button type="button" id="finances-' . $tenant->ID . '" class="actionbutton financialsummary">Show Financials Summary</button><hr /><br />
                                <br />
                                <div id="financialsummary-' . $tenant->ID .'"></div>
                                </td>
                                </td></tr>';
                    } elseif( $tenant_meta['_mpp_tenantstatus_value'][0] == '0' || $tenant_meta['_mpp_tenantstatus_value'][0] == 'A') {
                        if($tenant_meta['_mpp_tenantstatus_value'][0] == 'A'){ $approved =  'approved'; } else { $approved = '';}
                        $applicants .=
                            '<tr class="applicants '. $approved .'" >'
                            . '<td width="40"><a href="' . admin_url() . 'post.php?post=' . $tenant->ID . '&action=edit"><img style="width:12px;" src="' . plugins_url() . '/wppmanage/images/Modify.png" /></a>' . $switchurl . '</td>'
                            . '<td width="125" class="name" style="cursor:pointer;">' . $tenant->post_title . ' </td>'
                            . '<td width="100" class="property">' . get_the_title($tenant_meta['_mpp_property_value'][0]) . '</td>'
                            . '<td width="100" class="phone">' . $tenant_meta['_mpp_phone_value'][0] . '</td>'
                            . '<td width="225" class="email">' . $tenant_meta['_mpp_email_value'][0] . '</td>'
                            . '<td width="225" class="college" style="display:none;">' . $tenant_meta['_mpp_college_value'][0] . '</td>'
                            . '<td width="50" class="left" id="dep-' .$tenant->ID .'" tenid="' .$tenant->ID .'">' . $tenant_meta['_mpp_depositremaining_value'][0] . '</td>'
                            .'<td width="50" class="due" id="bal-' .$tenant->ID .'" tenid="' .$tenant->ID .'">' . $tenant_meta['_mpp_currentbalance_value'][0] . '</td>'
                            .'<td class="progress" colspan="7">
                        APP: <input type="checkbox"'. ($tenant_meta['application'][0] == 1 ? "checked":"") .'></input>
                        TOUR: <input type="checkbox"'. ($tenant_meta['tour'][0] == 1 ? "checked":"") .'></input>
                        SIGN: <input type="checkbox"'. ($tenant_meta['contract_signed'][0] == 1 ? "checked":"") .'></input>
                        SENT: <input type="checkbox"'. ($tenant_meta['contract_sent'][0] == 1 ? "checked":"") .'></input>
                        DEP: <input type="checkbox"'. ($tenant_meta['deposit_paid'][0] == 1 ? "checked":"") .'></input>
                        F&L: <input type="checkbox"'. ($tenant_meta['first_&_last_paid'][0] == 1 ? "checked":"") .'></input>
                        IN: <input type="checkbox"'. ($tenant_meta['moved_in'][0] == 1 ? "checked":"") .'></input>
                        UPCON: <input type="checkbox"'. (!empty($tenant_meta['signed_contract'][0]) ? "checked":"") .'></input>
                        UPDL: <input type="checkbox"'. (!empty($tenant_meta['drivers_license_copy'][0]) ? "checked":"") .'></input>
                        SSN: <input type="checkbox"'. (!empty($tenant_meta['social_security_number'][0]) ? "checked":"") .'></input>
                            </td>'
                            . '</tr>'
                            . '
                                <tr class="tenantdetails">
                                <td colspan="8">
                                <div class="tenantinfo">
                                </div>
                                <button type="button" id="' . $tenant->ID . '" class="actionbutton finances">Print Deposit Summary</button><button type="button" id="finances-' . $tenant->ID . '" class="actionbutton financialsummary">Show Financials Summary</button><button type="button" tenid='.$tenant->ID.' id="approve-' . $tenant->ID . '" class="actionbutton approvetenant">Approve</button><button tenid='.$tenant->ID.' type="button" id="delete-' . $tenant->ID . '" class="actionbutton deletetenant">Delete</button><hr /><br />
                                <br />
                                <div id="financialsummary-' . $tenant->ID .'"></div>
                                </td>
                                </td></tr>';
                    }
                }

                echo $currenttenants;
                ?>
            </table>
        </div>

        <h2>Future Tenants</h2>

        <button class="showprogress">Show Tenant Progress</button>

        <div id="tenantlist">
            <table class="tenantlist sortable futuretenants style="background-color:#fff;">
            <tr>
                <th width="25"></th>
                <th width="125">Name</th>
                <th width="100" class="property">Property</th>
                <th width="100" class="phone">Phone</th>
                <th width="225">Email/College <span id="showcollege" style="cursor:pointer;">(click to switch)</span></th>
                <th width="60" class="left">Dep</th>
                <th width="50" class="due">Bal</th>
                <th width="600" class="progress">Progress</th>
            </tr>

            <?php
            echo $futuretenants;
            ?>
            </table>
        </div>

        <h2>Past Tenants</h2>

        <div id="tenantlist">
            <table class="tenantlist sortable pasttenants style="background-color:#fff;">
            <tr>
                <th width="25"></th>
                <th width="125">Name</th>
                <th width="100" class="property">Property</th>
                <th width="100" class="phone">Phone</th>
                <th width="225" class="sorttable_nosort">Email/College <span id="showcollege" style="cursor:pointer;">(click to switch)</span></th>
                <th width="60" class="left">Dep</th>
                <th width="50" class="due">Bal</th>
                <th width="600" class="progress">Progress</th>
            </tr>

            <?php
            echo $pasttenants;
            ?>
            </table>
        </div>

        <h2>Applicants</h2>

        <button class="showprogress">Show Tenant Progress</button>

        <div id="tenantlist">
            <table class="tenantlist  sortable futuretenants style="background-color:#fff;">
            <tr>
                <th width="25"></th>
                <th width="125">Name</th>
                <th width="100" class="property">Property</th>
                <th width="100" class="phone">Phone</th>
                <th width="225">Email/College <span id="showcollege" style="cursor:pointer;">(click to switch)</span></th>
                <th width="60" class="left">Dep</th>
                <th width="50" class="due">Bal</th>
                <th width="600" class="progress">Progress</th>
            </tr>

            <?php
            echo $applicants;
            ?>
            </table>
        </div>
    </div>
    <!-- START CODE FOR LEDGER -->
    <div style="width:900px; float:left;">
        <?php  $transactionURL = admin_url( 'admin.php?page=wppmanage_ledger', 'http' );  ?>
        <h2>Recent Transactions <span style="font-size:9px;"><a href="<?php echo $transactionURL; ?>">(SEE ALL)</a></span></h2>
        <div style="float:left;">
            <div id="post-body-content" style="width:auto;">

                <div class="sorting trans">
                    <table id="sort-by">
                        <tr>
                            <th width="150">
                                <div class="sortname">Name</div>
                            </th>
                            <th width="100" class="property">  <div class="sortname">Property</div>
                            </th>
                            <th width="100">  <div class="sortname">Date</div>
                            </th>
                            <th width="75">  <div class="sortname">Amt</div>
                            </th>
                            <th width="200" class="description">  <div class="sortname">Desc</div>
                            </th>
                            <th width="105">  <div class="sortname">Type</div>
                            </th>
                            <th colspan="2" width="50" align="center" class="sortable"><a href=""><span style="font-size:7px;">CLEAR</span></a></th>
                        </tr>
                    </table>
                </div>

                <div id="container">
                    <?php
                    $args = array(
                        'post_type' => 'transactions',
                        'post_status' => 'publish',
                        'posts_per_page' => 10,
                        'orderby' => 'modified'
                    );
                    $payments = get_posts($args);

                    foreach($payments as $payment){

                    $meta = get_post_meta($payment->ID);
                    $type = $meta['_mpp_transactiontype_value'][0];
                    $nonce_delete = wp_nonce_url(get_bloginfo("url") . "/wp-admin/post.php?action=delete&amp;post=" . $payment->ID, "delete-post_" . $payment->ID);

                    $tenant = get_post($meta['_mpp_tenantid_value'][0]);
                    $tenantmeta = get_post_meta($tenant->ID);
                    $property = get_post($tenantmeta['_mpp_property_value'][0]);

                    echo '<div class="isotopeitem '. $meta['_mpp_tenantid_value'][0].' '.$tenantmeta['_mpp_property_value'][0].' '. $type.'">';

                    echo '<div style="width:150px;" class="name">' . $tenant->post_title . '</div>';
                    echo '<div style="width:100px" class="property">'.get_the_title($property->ID) .'</div>';
                    echo '<div style="width:100px" class="date">' . date("m/d/Y", strtotime($meta['_mpp_transactiondate_value'][0])) . '<span style="display:none;" class="calcdate">'.$meta['_mpp_transactiondate_value'][0].'</span></div>';
                    echo '<div style="width:75px" class="amount">$' . number_format($meta['_mpp_amount_value'][0],2,".","") . '</div>';
                    echo '<div style="width:200px" class="description">' . $meta['_mpp_reason_value'][0] . '&nbsp; </div>';
                    echo '<div style="width:105px" class="type">' . ucfirst($type) . '</div>'; ?>
                    <div class="linker">
                        <a class="edittransaction" id="<?php echo $payment->ID; ?>">
                            <img style="width:12px;" src="<?php echo plugins_url() . '/wppmanage/images/Modify.png'; ?>" />
                        </a>
                    </div>
                    <div class="linker">
                        <a class="deletetransaction" tenid="<?php echo $tenant->ID ?>" delurl="<?php echo $nonce_delete ?>">
                            <img style="width:12px;" src="<?php echo plugins_url() . '/wppmanage/images/delete-icon.gif'; ?>" />
                        </a>
                    </div>
                    <div class="clearall"></div>
                </div>
                <?php } ?>

            </div>
        </div>
    </div>
    <div style="clear:both;"></div>
    <!-- START CODE FOR TASKLOG -->
    <h2><?php _e( 'MPP\'s Recent Jobs', 'cron-view' ); ?>  <span style="font-size:9px;"><a href="<?php echo $tasklogURL; ?>">(SEE ALL)</a></span></h2>

    <table class="widefat fixed divided" style="width:800px;">
        <thead>
        <tr>
            <th scope="col">Run Date (MST)</th>
            <th scope="col">Entity</th>
            <th scope="col">Hook</th>
            <th scope="col">Result</th>
        </tr>
        </thead>
        <tbody>
        <?php  $dir = plugin_dir_path( __FILE__ );  $file = $dir . 'cronlog.txt'; $contents = file_get_contents($file);
        //            $short = substr($contents,-1100); echo $short;
        $entries = explode("<tr>", $contents);
        $entries = array_reverse($entries);
        $counter = 0;
        for($x = 0; $x <= 5; $x++) {
            echo $entries[$x];
        }

        ?>
        </tbody>
    </table>

    <!-- START CODE FOR PROPERTY LIST -->
    <div style="width:725px; min-height:1000px; margin-bottom: 55px; float:left; margin-right:25px;">
        <h2>Properties <span style="font-size:9px;"></span>
        </h2>

        <div id="tenantlist">
            <table class="tenantlist" style="background-color:#fff;">
                <tr>
                    <td width="25"></td>
                    <td width="125">Unit</td>
                    <td width="200">Address</td>
                    <td width="100">Cap</td>
                    <td width="225">Rate</td>
                </tr>

                <?php
                foreach ($properties as $property) {

                    $property_meta = get_post_meta($property->ID);

                    echo '<tr>';
                    echo '<td width="25"><a href="' . admin_url() . 'post.php?post=' . $property->ID . '&action=edit"><img style="width:12px;" src="' . plugins_url() . '/wppmanage/images/Modify.png" /></a></td>';
                    echo '<td width="125" class="propname">' . $property->post_title . '</td>';
                    echo '<td width="200" class="property">' . $property_meta['_mpp_address_value'][0] . ',  '. $property_meta['_mpp_city_value'][0] .',  '. $property_meta['_mpp_state_value'][0] .' ' . $property_meta['_mpp_zip_value'][0]. '</td>';
                    echo '<td width="100" class="phone">' . $property_meta['_mpp_capacity_value'][0] . '</td>';
                    echo '<td width="225" class="rate">' . $property_meta['_mpp_yrrent_value'][0] . '</td>';

                    echo '</td>
                        </tr>
                        <tr class="tenantdetails">
                        <td colspan="8" class="tenantdetailarea">
                        <div class="tenantinfo">
                        </div>

                        <button type="button" id="' . $property->ID . '" class="actionbutton finances">Print Deposit Summary</button><hr /><br />' . showDepositLedger($property->ID) . '<br /><hr />' . showTenantLedger($property->ID) . ' <br />

                        </td>
                        </td></tr><div style="margin-bottom:25px;"></div>';

                }
                ?>
            </table>
        </div>

    </div>
</div>

<?php

} else {

    $uname = $current_user -> user_login;
    $args = array(
        'meta_query' => array(
            array(
                'key' => '_mpp_username_value',
                'value' => $uname
            )
        ),
        'post_type' => 'tenants'
    );
    $tenants = get_posts($args);
    $tenant_values = get_post_meta($tenants[0]->ID);
    ?>

    <div style="background-color:#ffffff; height:80px;">
        <h1 style="font-size:20px; padding-top:30px; padding-left:10px;">Welcome  <?php echo $current_user->display_name; ?></h1>
    </div>

<!--    <div id="contentcontainernav">-->
<!--        <ul>-->
<!--            <li><a id="1" class="dynamicbtn selected">Overview</a></li>-->
<!--            <li><a id="4" class="dynamicbtn ">Contract Details</a></li>-->
<!--            <li><a id="2" class="dynamicbtn ">Payments</a></li>-->
<!--            <li><a id="5" class="dynamicbtn ">Ledger/Payments</a></li>-->
<!--            <li><a id="6" class="dynamicbtn ">Maintenance Request</a></li>-->

<!--        </ul>-->
<!--    </div>-->

    <div id="contentcontainer" style="width:90%; padding-left:15px;">
        <div class="dynamic" id="content-1">
           <?php include_once('tenantpages/announcements.php'); ?>
        </div>
<!--        <div class="dynamic" id="content-2" style="display:none;">-->
<!--            --><?php //include_once('tenantpages/ledger_payment.php'); ?>
<!--        </div>-->
<!--        <div class="dynamic" id="content-4" style="display:none;">-->
<!--            --><?php //include_once('tenantpages/contractdetails.php'); ?>
<!--        </div>-->
<!--        <div class="dynamic" id="content-5" style="display:none;">-->
<!--            --><?php //include_once('tenantpages/tenantledger.php'); ?>
<!--        </div>-->
<!--        <div class="dynamic" id="content-6" style="display:none;">-->
<!--            --><?php //include_once('tenantpages/maintenance.php'); ?>
<!--        </div>-->

    </div>

<?php }

