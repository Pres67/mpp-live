<?php


$current_user = wp_get_current_user();
if(!current_user_can('edit_users')) die("Access Denied");

wp_register_script( 'wppm_tenants_js', plugins_url() . '/wppmanage/includes/js/tenants.js' );
wp_enqueue_script( 'wppm_tenants_js' );

//Filter Setup
$args = array(
    'post_type' => 'tenants',
    'post_status' => 'publish',
    'posts_per_page' => -1,
    'orderby' => 'title',
    'order' => 'asc',
    'meta_key' => '_mpp_tenantstatus_value',
    'meta_value' => '2'
);
$tenants = get_posts($args);
$args = array(
    'post_type' => 'properties',
    'post_status' => 'publish',
    'posts_per_page' => -1,
    'orderby' => 'title',
    'order' => 'asc'
);
$properties = get_posts($args);
?>
<script type="text/javascript">
    function isNumberKey(evt, obj) {

        var charCode = (evt.which) ? evt.which : event.keyCode
        var value = obj.value;
        if (charCode == 45 && value== "") return true;
        var dotcontains = value.indexOf(".") != -1;
        if (dotcontains)
            if (charCode == 46) return false;
        if (charCode == 46) return true;
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
</script>
<div class="popedit" id="popedit">
</div>
<div class="popcontent" id="popcontent">
</div>
<style>
    #popedit, #popcontent {
        display:none;
    }
    .popedit{
        position:absolute;
        top:0;
        left:-10px;
        z-index:1;
    }
    .popcontent{
        z-index:2;
        position:absolute;
        max-width:300px;
    }
    input{
        width:100%;
        margin:0px;
        padding:0px;
    }
    th{
        cursor:pointer;
        padding:10px;
        background-color:#fff;
    }
    th.reset{
        background-color:#000;
    }
    th.reset a{
        padding:10px;
        color:#fff;
        text-decoration: none;
    }
    .sorting td{
        padding-left:5px;
        background-color:#fff;
    }
    .sorting td a{
        display:block;
        padding-top:10px;
        padding-bottom:10px;
        text-decoration: none;
    }
    .isotopeitem td{
        padding-left:5px;
        background-color:#fff;
    }
    .isotopeitem td a{
        display:block;
        padding-top:10px;
        padding-bottom:10px;
        text-decoration: none;
    }
    .arrows{
        float:right;
    }
    .sorting td .arrows a{
        padding:0;
    }
    .sorting td .arrows a:focus, #filters td a:focus{
        box-shadow:none;
    }
    .toparrow{
        padding: 5px;
    }
    .bottomarrow{
        padding: 5px;
    }
    .sortname{
        float:left;
        margin-top:18px;
    }
    .sortable.active{
        background-color:yellow;
    }
    #filters td a{
        text-decoration: none;
        display:block;
        padding: 3px 3px;
    }
    #filters td a:hover{
        background-color:#eaeaea;
    }
    .searchbox{
        height: 37px;
        margin-top: 3px;
        margin-left: 3px;
        width: 280px;
        margin-bottom: 15px;
        padding:8px;
    }
    td.linker a{
        padding:10px;
    }
    .tenantdetails{
        display:none;
    }
    .ratetitle{
        float:left;
    }
    .tenantinfo input{
        float:left;
        width:auto;
    }

</style>

<div class="wrap" id="maincontent">
    <h2>Welcome My Provo Property Administrator!
    </h2>
    <h4>If you are not an MPP Administrator and do not have permission to view this page, please leave immediately.  Your IP address has been recorded.  This information is confidential and private. </h4>
<!--    <div id="showall"><h3>Show all transactions</h3></div>-->
<div id="poststuff">
    <div id="post-body" class="metabox-holder columns-2">
        <div id="post-body-content" style="width:auto;">
        <!--**************** FILTERING SECTION ****************-->
            <section id="options" class="clearfix options">
                <div class="filtersection" style="z-index: 999;">
                    <div id="filters" class="filtertable option-set clearfix" data-option-key="filter">
                        <input type="text" class="searchbox" id="quicksearch" placeholder="Or Search" />

                        <table class="sublist expandable" style="float:left;"><thead><tr><th id="All Tenants">All Tenants</th></tr></thead>
                            <tbody style="display: none; position:absolute; background:#FFF; z-index:1;">
                            <tr><td><a href="#filter" data-option-value=".typeall">All Tenants</a></td></tr>
                            <?php foreach($tenants as $tenant){ echo '<tr><td><a href="#filter" data-option-value=".'. $tenant->ID.'">'. get_the_title($tenant->ID) .'</a></td></tr>'; }?>
                            </tbody>
                        </table>
                        <table class="sublist expandable" style="float:left;"><thead><tr><th id="All Apartments">All Apartments</th></tr></thead>
                            <tbody style="display: none; position:absolute; z-index:1; background:#fff; padding:5px;">
                            <tr><td><a href="#filter" data-option-value=".typeall">All Apartments</a></td></tr>
                            <?php foreach($properties as $property){ echo '<tr><td><a href="#filter" data-option-value=".'. $property->ID.'">'. get_the_title($property->ID) .'</a></td></tr>'; }?>
                            </tbody>
                        </table>
<!---->
<!--                        <table class="sublist expandable" style="float:left;"><thead><tr><th id="All Types">All Types</th></tr></thead>-->
<!--                            <tbody style="display: none; position:absolute; z-index:1; background:#fff; padding:5px;">-->
<!--                            <tr><td><a href="#filter" data-option-value=".typeall">All Types</a></td></tr>-->
<!--                            <tr><td><a href="#filter" data-option-value=".deposit">Deposit</a></td></tr>-->
<!--                            <tr><td><a href="#filter" data-option-value=".fee">Fee</a></td></tr>-->
<!--                            <tr><td><a href="#filter" data-option-value=".rent">Rent</a></td></tr>-->
<!--                            <tr><td><a href="#filter" data-option-value=".payment">Payment</a></td></tr>-->
<!--                            </tbody>-->
<!--                        </table>-->
                        <table id="nofilter" style="display:none; float:left;">
                            <tbody style="display:none;">
                            <tr><th class="reset"><a href="#filter" data-option-value=".all" ><span class="xclose">X </span>  Reset Filters</a></th></tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section> <!-- #options -->

            <div class="clear"></div>
                <h2>Tenants</h2>

            <div class="sorting">
                <table id="sort-by">
                    <tr>
                        <td width="25">
                            <input type="checkbox" id="allchecked" />
                        </td>
                        <td width="150">
                            <div class="sortname">Name</div>
                                <div class="arrows">
                                    <div class="toparrow sortable">
                                        <a href="#name"><img style="width:12px;" src="<?php echo plugins_url() . '/wppmanage/images/toparrow.png'; ?>" /></a>
                                    </div>
                                    <div class="bottomarrow sortable">
                                        <a href="#named"><img style="width:12px;" src="<?php echo plugins_url() . '/wppmanage/images/bottomarrow.png'; ?>" /></a>
                                    </div>
                                </div>
                        </td>
                        <td width="100">  <div class="sortname">Property</div>
                            <div class="arrows">
                                <div class="toparrow sortable">
                                    <a href="#property"><img style="width:12px;" src="<?php echo plugins_url() . '/wppmanage/images/toparrow.png'; ?>" /></a>
                                </div>
                                <div class="bottomarrow sortable">
                                    <a href="#propertyd"><img style="width:12px;" src="<?php echo plugins_url() . '/wppmanage/images/bottomarrow.png'; ?>" /></a>
                                </div>
                            </div>
                        </td>
                        <td width="50">  <div class="sortname">Sex</div>
                            <div class="arrows">
                                <div class="toparrow sortable">
                                    <a href="#sex"><img style="width:12px;" src="<?php echo plugins_url() . '/wppmanage/images/toparrow.png'; ?>" /></a>
                                </div>
                                <div class="bottomarrow sortable">
                                    <a href="#sexd"><img style="width:12px;" src="<?php echo plugins_url() . '/wppmanage/images/bottomarrow.png'; ?>" /></a>
                                </div>
                            </div>
                        </td>
                        <td width="100">  <div class="sortname">Phone</div>
<!--                            <div class="arrows">-->
<!--                                <div class="toparrow sortable">-->
<!--                                    <a href="#phone"><img style="width:12px;" src="--><?php //echo plugins_url() . '/wppmanage/images/toparrow.png'; ?><!--" /></a>-->
<!--                                </div>-->
<!--                                <div class="bottomarrow sortable">-->
<!--                                    <a href="#phoned"><img style="width:12px;" src="--><?php //echo plugins_url() . '/wppmanage/images/bottomarrow.png'; ?><!--" /></a>-->
<!--                                </div>-->
<!--                            </div>-->
                        </td>
                        <td width="250">  <div class="sortname">Email</div>
<!--                            <div class="arrows">-->
<!--                                <div class="toparrow sortable">-->
<!--                                    <a href="#email"><img style="width:12px;" src="--><?php //echo plugins_url() . '/wppmanage/images/toparrow.png'; ?><!--" /></a>-->
<!--                                </div>-->
<!--                                <div class="bottomarrow sortable">-->
<!--                                    <a href="#emaild"><img style="width:12px;" src="--><?php //echo plugins_url() . '/wppmanage/images/bottomarrow.png'; ?><!--" /></a>-->
<!--                                </div>-->
<!--                            </div>-->
                        </td>
<!--                        <td width="70">  <div class="sortname">School</div>-->
<!--                            <div class="arrows">-->
<!--                                <div class="toparrow sortable">-->
<!--                                    <a href="#school"><img style="width:12px;" src="--><?php //echo plugins_url() . '/wppmanage/images/toparrow.png'; ?><!--" /></a>-->
<!--                                </div>-->
<!--                                <div class="bottomarrow sortable">-->
<!--                                    <a href="#school"><img style="width:12px;" src="--><?php //echo plugins_url() . '/wppmanage/images/bottomarrow.png'; ?><!--" /></a>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </td>-->
<!--                        <td width="50">  <div class="sortname">Rate</div>-->
<!--                            <div class="arrows">-->
<!--                                <div class="toparrow sortable">-->
<!--                                    <a href="#rate"><img style="width:12px;" src="--><?php //echo plugins_url() . '/wppmanage/images/toparrow.png'; ?><!--" /></a>-->
<!--                                </div>-->
<!--                                <div class="bottomarrow sortable">-->
<!--                                    <a href="#rated"><img style="width:12px;" src="--><?php //echo plugins_url() . '/wppmanage/images/bottomarrow.png'; ?><!--" /></a>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </td>-->
<!--                        <td width="50">  <div class="sortname">Dep</div>-->
<!--                            <div class="arrows">-->
<!--                                <div class="toparrow sortable">-->
<!--                                    <a href="#deposit"><img style="width:12px;" src="--><?php //echo plugins_url() . '/wppmanage/images/toparrow.png'; ?><!--" /></a>-->
<!--                                </div>-->
<!--                                <div class="bottomarrow sortable">-->
<!--                                    <a href="#deposited"><img style="width:12px;" src="--><?php //echo plugins_url() . '/wppmanage/images/bottomarrow.png'; ?><!--" /></a>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </td>-->
                        <td width="50">  <div class="sortname">Dep</div>
                            <div class="arrows">
                                <div class="toparrow sortable">
                                    <a href="#left"><img style="width:12px;" src="<?php echo plugins_url() . '/wppmanage/images/toparrow.png'; ?>" /></a>
                                </div>
                                <div class="bottomarrow sortable">
                                    <a href="#leftd"><img style="width:12px;" src="<?php echo plugins_url() . '/wppmanage/images/bottomarrow.png'; ?>" /></a>
                                </div>
                            </div>
                        </td>
                        <td width="50">  <div class="sortname">Bal</div>
                            <div class="arrows">
                                <div class="toparrow sortable">
                                    <a href="#due"><img style="width:12px;" src="<?php echo plugins_url() . '/wppmanage/images/toparrow.png'; ?>" /></a>
                                </div>
                                <div class="bottomarrow sortable">
                                    <a href="#dued"><img style="width:12px;" src="<?php echo plugins_url() . '/wppmanage/images/bottomarrow.png'; ?>" /></a>
                                </div>
                            </div>
                        </td>
                        <td  width="50" align="center" class="sortable"><a href=""><span style="font-size:7px;">CLEAR</span></a></td>
                    </tr>
                </table>
            </div>

            <div id="container">
                <?php
                    $args = array(
                        'post_type' => 'properties',
                        'post_status' => 'publish',
                        'posts_per_page' => -1,
                        'orderby' => 'title',
                        'order' => 'asc'
                    );
                    $properties = get_posts($args);

                    foreach($tenants as $tenant){

                        $args = array(
                            'post_type' => 'transactions',
                            'posts_per_page' => -1,
                            'meta_key' => '_mpp_transactiondate_value',
                            'orderby' => 'meta_value_num',
                            'order' => 'ASC',
                            'meta_query' => array(
                                'relation' => 'AND',
                                array(
                                    'key' => '_mpp_tenantid_value',
                                    'value' => $tenant->ID,
                                    'compare' => '='
                                ),
                                array(
                                    'key' => '_mpp_transactiontype_value',
                                    'value' => 'deposit',
                                    'compare' => '='
                                )
                            )
                        );
                        $payments = get_posts($args);

                        $tenant_meta = get_post_meta($tenant->ID);

                        echo '<table id="'.$tenant->ID.'" class="isotopeitem '. $tenant->ID.' '.$tenant_meta['_mpp_property_value'][0].' ">';
                        echo '<tr>';
                        echo '<td width="25" /><input type="checkbox" class="tenantbox"/></td>';
                        echo '<td width="150" class="name">' . $tenant->post_title . '</td>';
                        echo '<td width="100" class="property">'.get_the_title($tenant_meta['_mpp_property_value'][0]).'</td>';
                        echo '<td width="50" class="sex">'.$tenant_meta['_mpp_gender_value'][0].'</td>';
                        echo '<td width="100" class="phone">'.$tenant_meta['_mpp_phone_value'][0].'</td>';
                        echo '<td width="250" class="email">'.$tenant_meta['_mpp_email_value'][0].'</td>';
//                        echo '<td width="70" class="school">'.$tenant_meta['_mpp_college_value'][0].'</td>';
//                        echo '<td width="50" class="rate">'.get_rental_rate($tenant->ID) .'</td>';
//                        echo '<td width="50" class="dep">'.get_post_meta( $post_id, '_mpp_deposit_value', true).'</td>';
                        echo '<td width="50" class="left">'.get_deposit_balance($tenant->ID, 'html') .'</td>';
                        echo '<td width="50" class="due">'.get_balance($tenant->ID, 'html') .'</td>';

                        echo '</td>'; ?>
                        <td width="50" class="linker" ></td>
                        <?php
                        echo '</tr>';
                        echo '<tr class="tenantdetails"><td></td><td colspan="7">';
                        echo '<div class="tenantinfo">';


                       echo '<div class="depositinformation">
                            <div class="sorting">
                                <table id="sort-by" style="background-color: white;">
                                    <tr style="font-weight:bold;">
                                        <td width="200">Name</td>
                                        <td width="100">Date</td>
                                        <td width="100">Amount</td>
                                        <td width="500">Description</td>
                                    </tr>
                                </table>
                            </div>';

                        echo '<table>';


        foreach($payments as $payment){

            $meta = get_post_meta($payment->ID);
            $type = $meta['_mpp_transactiontype_value'][0];

            $tenant = get_post($meta['_mpp_tenantid_value'][0]);
            $tenantmeta = get_post_meta($tenant->ID);
            $property = get_post($tenantmeta['_mpp_property_value'][0]);

            echo '<tr class="'. $meta['_mpp_tenantid_value'][0].' '.$tenantmeta['_mpp_property_value'][0].' '. $type.'">';
            echo '<td width="200" class="name">' . $tenant->post_title . '</td>';
            echo '<td width="100" class="date">' . date("m/d/Y", strtotime($meta['_mpp_transactiondate_value'][0])) . '<span style="display:none;" class="calcdate">'.$meta['_mpp_transactiondate_value'][0].'</span></td>';
            echo '<td width="100" class="amount">$' . number_format($meta['_mpp_amount_value'][0],2) . '</td>';
            echo '<td width="500" class="description">' . $meta['_mpp_reason_value'][0] . '</td>';
            echo '</td>';
            echo '</tr>';
        }

        echo '</table>
<div class="sorting">
            <table id="sort-by" style="background-color: white;">
                <tr style="font-weight:bold;">
                    <td width="200"></td>
                    <td width="100">Deposit Left :</td>
                    <td width="100">$' . number_format($deposit,2) . '</td>
                    <td width="500"></td>
                </tr>
            </table>
        </div>
            </div>';


//
//                        echo '<input type="text" size="13" name="_mpp_first_value" id="_mpp_first_value" value="'.$tenant_meta['_mpp_first_value'][0].'">';
//                        echo '<input type="text" size="13" name="_mpp_last_value" id="_mpp_last_value" value="'.$tenant_meta['_mpp_last_value'][0].'">';
//                        echo '<input type="text" size="25" name="_mpp_email_value" id="_mpp_email_value" value="'.$tenant_meta['_mpp_email_value'][0].'">';
//                        echo '<input type="text" size="11" name="_mpp_phone_value" id="_mpp_phone_value" value="'.$tenant_meta['_mpp_phone_value'][0].'">';
//                        echo '<input type="text" size="1" name="_mpp_gender_value" id="_mpp_gender_value" value="'.$tenant_meta['_mpp_gender_value'][0].'">';
//                        echo '<input type="text" size="13" name="_mpp_college_value" id="_mpp_college_value" value="'.$tenant_meta['_mpp_college_value'][0].'">';
//
//                        echo '<select name="_mpp_property_value">
//                        <option value="">Please Select </option>';
//
//                        foreach($properties as $property){
//                            echo '<option value="'.$property->ID.'">' . $property->post_title .'</option>';
//                        }
//                        echo '</select>
//                        <input type="text" size="8" name="_mpp_startdate_value" id="_mpp_startdate_value" value="'.$tenant_meta['_mpp_startdate_value'][0].'">
//                        <input type="text" size="8" name="_mpp_enddate_value" id="_mpp_enddate_value" value="'.$tenant_meta['_mpp_enddate_value'][0].'">
//                        <input type="text" size="5" name="_mpp_deposit_value" id="_mpp_deposit_value" value="'.$tenant_meta['_mpp_deposit_value'][0].'">
//                        <input type="text" size="5" name="_mpp_lmr_value" id="_mpp_lmr_value" value="'.$tenant_meta['_mpp_lmr_value'][0].'">
//
//                        <div class="contractbox">
//
//                        </div>
//                        <div class="ratebox">
//                            <div class="rateboxtitle">RATES</div>
//                                <div class="ratetitle">F/W</div><input type="text" size="3" name="_mpp_fwrate_value" id="_mpp_fwrate_value" value="'.$tenant_meta['_mpp_fwrate_value'][0].'">
//                                <div class="ratetitle">S/S</div><input type="text" size="3" name="_mpp_ssrate_value" id="_mpp_ssrate_value" value="'.$tenant_meta['_mpp_ssrate_value'][0].'">
//                                <div class="ratetitle">YR</div><input type="text" size="3" name="_mpp_yrrate_value" id="_mpp_yrrate_value" value="'.$tenant_meta['_mpp_yrrate_value'][0].'">
//                        </div>
//                        <div class="status">
//                            <input type="text" name="_mpp_tenantstatus_value" id="_mpp_tenantstatus_value" value="'.$tenant_meta['_mpp_tenantstatus_value'][0].'">
//                            <p>Status Values: <br> 0 - Applicant <br> 1 - Future Tenant <br> 2 - Current Tenant <br> 3 - Prior Tenant</p>';


                        echo '<table>
                                <tr>
                                <td><button type="button" id="'.$tenant->ID.'" class="finances">View Deposit Summary</button></td>
                                <td></td>
                                <td></td>
                                </tr>
                              </table>';
                        echo '</td></tr>';
                        echo '</table>';
                }
                ?>

            </div>
        </div>
<!--        <div id="postbox-container-1" class="postbox-container" style="margin-top:135px; float:left;">-->
<!--            <div id="side-sortables" class="meta-box-sortables">-->
<!--                <div id="new-meta-boxes-leder-transaction" class="postbox" style="width:300px;">-->
<!--                    <h3 class="hndle"><span>Record Transaction</span></h3>-->
<!--                    <div class="inside">-->
<!--                        <div class="meta-field">-->
<!--                            <input type="hidden" name="_mpp_pbalance_noncename" id="_mpp_pbalance_noncename" value="28cb5b2291">-->
<!--                            <input type="hidden" id="location" name="location" value="ledger">-->
<!--                            <div style="display:none;" class="" id="editid"></div>-->
<!--                <span id="tenantselector">-->
<!--                    <label for="tenant_name">Tenant </label>-->
<!--                    <select name="tenant_name" id="tenant_name" style="float:right;">-->
<!--                        <option value="">Please Select </option>-->
<!--                        --><?php //foreach($tenants as $tenant){
//                            echo '<option value="'.$tenant->ID.'.'.$tenant->post_title.'">' . $tenant->post_title .'</option>';
//                        } ?>
<!--                    </select>-->
<!--                    <div style="clear:both"></div>-->
<!--                </span>-->
<!--                <span id="aptselector">-->
<!--                    <label for="property_id">Apt </label>-->
<!--                    <select name="property_id" id="property_id" style="float:right;">-->
<!--                        <option value="">Please Select </option>-->
<!--                        --><?php //foreach($properties as $property){
//                            echo '<option value="'.$property->ID.'">' . $property->post_title .'</option>';
//                        } ?>
<!--                    </select>-->
<!--                    <div style="clear:both"></div>-->
<!--                </span>-->
<!--                            <label for="transactiondate">Date </label><input style="width:60%; float:right;" type="text" name="transactiondate" id="transactiondate" value="--><?php //echo date('m/d/Y'); ?><!--"><br>-->
<!--                            <div style="clear:both"></div>-->
<!--                            <label for="transactionamount">Amount </label>-->
<!--                            <input style="width:60%; float:right;" type="text" class="dollarinput" name="transactionamount" id="transactionamount" onKeyPress="return false;"><br>-->
<!--                            <div style="clear:both"></div>-->
<!--                            <label for="transactiontype">Type </label>-->
<!--                            <select name="transactiontype" id="transactiontype" style="float:right;">-->
<!--                                <option value="">Please Select </option>-->
<!--                                <option value="deposit">Deposit</option>-->
<!--                                <option value="fee">Fee Due </option>-->
<!--                                <option value="rent">Rent Due </option>-->
<!--                                <option value="payment">Payment</option>-->
<!--                            </select>-->
<!--                            <div style="clear:both"></div>-->
<!--                <span id="paymethod" style="display:none;">-->
<!--                    <label for="transactionmethod">Pay Method </label>-->
<!--                    <select name="transactionmethod" id="transactionmethod" style="float:right;">-->
<!--                        <option value="">Please Select </option>-->
<!--                        <option value="Paypal">Paypal</option>-->
<!--                        <option value="Intuit">Intuit</option>-->
<!--                        <option value="Check">Check</option>-->
<!--                        <option value="Other">Other</option>-->
<!--                    </select>-->
<!--                    <div style="clear:both"></div>-->
<!--                </span>-->
<!--                            <label for="transactiondescription">Description </label><br>-->
<!--                            <textarea rows="4" style="width:100%;" name="transactiondescription" id="transactiondescription"></textarea><br>-->
<!--                            <button type="button" id="recordtransactionbutton">Record Transaction</button><span id="transactionstatus" class="transactionstatus"></span>-->
<!--                        </div><br style="clear:both"></div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
    </div>
  </div>
</div> <!-- wrap -->

