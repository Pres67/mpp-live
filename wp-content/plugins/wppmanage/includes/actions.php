<?php require_once '../../../../wp-load.php';
/* TODO:  Fix this reference to the wp load file; TODO: Log all actions into a history database/table */

$action = $_POST['action'];
if(!$action) $action = $_GET['action'];
//if(!current_user_can('edit_users') && $action != 'uploadlicense' || !$action) die('Access Denied');

$returnText = '';

$args = array(
    'post_type' => 'tenants',
    'posts_per_page' => -1,
    'orderby' => 'title',
    'order' => "asc",
    'meta_query' => array(
        'relation' => 'OR',
        array(
            'key' => '_mpp_tenantstatus_value',
            'value' => '3',
            'compare' => '!=',
        ),
        array(
            'key' => '_mpp_depositremaining_value',
            'value' => '0.00',
            'compare' => '!=',
        ),
    ),
);

$tenants = get_posts($args);
$args = array(
    'post_type' => 'properties',
    'posts_per_page' => -1
);
$properties = get_posts($args);

switch($action){
    case 'uploadlicense':
        $tenid = $_GET['tenid'];
        $target_dir =  wp_upload_dir();
        $uploadOk = 1;

        $target_dirfile = $target_dir["path"] . '/';
        $actual_name = pathinfo( basename($_FILES["images"]["name"][0]),PATHINFO_FILENAME);
        $extension = pathinfo( basename($_FILES["images"]["name"][0]), PATHINFO_EXTENSION);
        $original_name = $actual_name;

        $check = getimagesize($_FILES["images"]["tmp_name"][0]);
        if($check == false) {
            $error = "Sorry, file is not an image.";
            $uploadOk = 0;
        }
        $i = 1;
        while(file_exists($target_dirfile . $actual_name . '.' . $extension))
        {
            $actual_name = (string)$original_name . '-' . $i;
            $i++;
        }

        $target_file = $target_dirfile . $actual_name . '.' . $extension;
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

        if (file_exists($target_file)) {
            $error = "Sorry, file already exists.";
            $uploadOk = 0;
        }
 //       if ($_FILES["images"]["size"][0] > 500000) {
 //           $error = "Sorry, your file is too large.";
 //           $uploadOk = 0;
 //       }
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
            $error = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            $uploadOk = 0;
        }
        if ($uploadOk == 1) {
            if (move_uploaded_file($_FILES["images"]["tmp_name"][0], $target_file)) {
                $parent_post_id = $tenid;
                $filetype = wp_check_filetype( basename( $target_file ), null );
                $wp_upload_dir = wp_upload_dir();
                $attachment = array(
                    'guid'           => $wp_upload_dir['url'] . '/' . basename( $target_file ),
                    'post_mime_type' => $filetype['type'],
                    'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $target_file ) ),
                    'post_content'   => '',
                    'post_status'    => 'inherit'
                );
                $attach_id = wp_insert_attachment( $attachment, $target_file, $parent_post_id);
                require_once( ABSPATH . 'wp-admin/includes/image.php' );
                $attach_data = wp_generate_attachment_metadata( $attach_id, $target_file );
                wp_update_attachment_metadata( $attach_id, $attach_data );

                if(update_field('drivers_license_copy', $attach_id, $tenid)){
                    echo "License Copy Successfully Uploaded.  Thank you.";
                } else {
                    echo "Sorry, there was an error uploading your license copy.";
                }
            } else {
                echo "Sorry, there was an error uploading your license copy.";
            }
        } else {
		echo $error;
	}
        break;
    case 'uploadid':
        $tenid = $_GET['tenid'];
        $target_dir =  wp_upload_dir();
        $uploadOk = 1;

        $target_dirfile = $target_dir["path"] . '/';
        $actual_name = pathinfo( basename($_FILES["images"]["name"][0]),PATHINFO_FILENAME);
        $extension = pathinfo( basename($_FILES["images"]["name"][0]), PATHINFO_EXTENSION);
        $original_name = $actual_name;

        $check = getimagesize($_FILES["images"]["tmp_name"][0]);
        if($check == false) {
            $error = "Sorry, file is not an image.";
            $uploadOk = 0;
        }
        $i = 1;
        while(file_exists($target_dirfile . $actual_name . '.' . $extension))
        {
            $actual_name = (string)$original_name . '-' . $i;
            $i++;
        }

        $target_file = $target_dirfile . $actual_name . '.' . $extension;
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

        if (file_exists($target_file)) {
            $error = "Sorry, file already exists.";
            $uploadOk = 0;
        }
//        if ($_FILES["images"]["size"][0] > 500000) {
//            $error = "Sorry, your file is too large.";
//            $uploadOk = 0;
//        }
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
            $error = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            $uploadOk = 0;
        }
        if ($uploadOk == 1) {
            if (move_uploaded_file($_FILES["images"]["tmp_name"][0], $target_file)) {
                $parent_post_id = $tenid;
                $filetype = wp_check_filetype( basename( $target_file ), null );
                $wp_upload_dir = wp_upload_dir();
                $attachment = array(
                    'guid'           => $wp_upload_dir['url'] . '/' . basename( $target_file ),
                    'post_mime_type' => $filetype['type'],
                    'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $target_file ) ),
                    'post_content'   => '',
                    'post_status'    => 'inherit'
                );
                $attach_id = wp_insert_attachment( $attachment, $target_file, $parent_post_id);
                require_once( ABSPATH . 'wp-admin/includes/image.php' );
                $attach_data = wp_generate_attachment_metadata( $attach_id, $target_file );
                wp_update_attachment_metadata( $attach_id, $attach_data );

                if(update_field('additional_id', $attach_id, $tenid)){
                    echo "Additional ID Successfully Uploaded.  Thank you.";
                } else {
                    echo "Sorry, there was an error uploading your ID copy.";
                }
            } else {
                echo "Sorry, there was an error uploading your ID copy.";
            }
        } else {
		echo $error;
	}
        break;
    case 'updatebalances':
        $tenID = $_POST['id'];
        if(get_post_type($tenID) != 'tenants'){
            $meta = get_post_meta($post_id);
            $tenID = $meta['_mpp_tenantid_value'][0];
        }
            $dep = 0;
            $args = array(
                'post_type' => 'transactions',
                'post_status' => 'publish',
                'posts_per_page' => -1,
                'meta_query' => array(
                    'relation' => 'AND',
                    array(
                        'key' => '_mpp_transactiontype_value',
                        'value' => 'deposit'
                    ),
                    array(
                        'key' => '_mpp_tenantid_value',
                        'value' => $tenID
                    )
                ),
            );
            $deductions = get_posts($args);
            $sum = 0;
            foreach($deductions as $deduction){
                $amount = get_post_meta($deduction->ID, '_mpp_amount_value', true);
                $sum = $sum + $amount;
            }

            $left = number_format($sum,2,".","");
            update_post_meta($tenID, '_mpp_depositremaining_value', $left);

            $args2 = array(
                'post_type' => 'transactions',
                'post_status' => 'publish',
                'posts_per_page' => -1,
                'meta_query' => array(
                    'relation' => 'AND',
                    array(
                        'key' => '_mpp_transactiontype_value',
                        'value' => array('fee', 'rent'),
                    ),
                    array(
                        'key' => '_mpp_tenantid_value',
                        'value' => $tenID
                    )
                ),
            );

            $args3 = array(
                'post_type' => 'transactions',
                'post_status' => 'publish',
                'posts_per_page' => -1,
                'meta_query' => array(
                    'relation' => 'AND',
                    array(
                        'key' => '_mpp_transactiontype_value',
                        'value' => 'payment'
                    ),
                    array(
                        'key' => '_mpp_tenantid_value',
                        'value' => $tenID
                    )
                ),
            );

            $fees = get_posts($args2);
            $payments = get_posts($args3);

            $feesum = 0;
            $pmtsum = 0;

            foreach($fees as $fee){
                $amount = get_post_meta($fee->ID, '_mpp_amount_value', true);
                $feesum += $amount;
            }

            foreach($payments as $pmt){
                $amount = get_post_meta($pmt->ID, '_mpp_amount_value', true);
                $pmtsum += $amount;
            }

            $balance = number_format($feesum - $pmtsum,2,".","");
            update_post_meta($tenID, '_mpp_currentbalance_value', $balance);

        break;
    case 'showfinancialsummary':
            $tenID = $_POST['id'];
            $args = array(
                'post_type' => 'transactions',
                'posts_per_page' => -1,
                'meta_key' => '_mpp_transactiondate_value',
                'orderby' => 'meta_value_num',
                'order' => 'ASC',
                'meta_query' => array(
                    'relation' => 'AND',
                    array(
                        'key' => '_mpp_tenantid_value',
                        'value' => $tenID,
                        'compare' => '='
                    ),
                    array(
                        'key' => '_mpp_transactiontype_value',
                        'value' => array('deposit','fee','rent','payment'),
                        'compare' => 'IN'
                    )
                )
            );
            $transactions = get_posts($args);

            $ledgerString = '
            <div class="depositinformation">
                <h2>Deposit Summary</h2>

                <div class="sorting">
                    <table id="sort-by" style="background-color: white;">
                        <tr style="font-weight:bold;">
                            <td width="200">Name</td>
                            <td width="100">Date</td>
                            <td width="100">Amount</td>
                            <td width="500">Description</td>
                            <td width="100"></td>
                        </tr>
                    </table>
                </div>';

            $ledgerString .= '<table class="divided">';

            $ledgerText = '<div class="ledger">
                <h2>Rent, Fees, and Payments</h2>
                <div class="sorting">
                    <table id="sort-by" class="divided" style="background-color: white; border-collapse: collapse;">
                        <tr style="font-weight:bold;">
                            <td width="200">Name</td>
                            <td width="100">Date</td>
                            <td width="100">Amount</td>
                            <td width="400">Description</td>
                            <td width="100">Type</td>
                            <td width="100"></td>
                        </tr>
                    </table>
                </div>
                <table class="divided">';

            foreach($transactions as $deposittransaction){

                $starter = '';
                $meta = get_post_meta($deposittransaction->ID);
                $type = $meta['_mpp_transactiontype_value'][0];
                if($type =='payment'){
                    $starter = "-";
                }
                $tenant = get_post($meta['_mpp_tenantid_value'][0]);
                $tenantmeta = get_post_meta($tenant->ID);
                $property = get_post($tenantmeta['_mpp_property_value'][0]);
                $rent = checkTenantBalance($tenID);
                $nonce_delete = wp_nonce_url(get_bloginfo("url") . "/wp-admin/post.php?action=delete&amp;post=" . $deposittransaction->ID, "delete-post_" . $deposittransaction->ID);


                if($type == 'deposit'){

                    $ledgerString.= '<tr class=" '. $meta['_mpp_tenantid_value'][0].' '.$tenantmeta['_mpp_property_value'][0].' '. $type.'">';
                    $ledgerString.= '<td width="200" class="tenname">' . $tenant->post_title . '</td>';
                    $ledgerString.= '<td width="100" class="date">' . date("m/d/Y", strtotime($meta['_mpp_transactiondate_value'][0])) . '<span style="display:none;" class="calcdate">'.$meta['_mpp_transactiondate_value'][0].'</span></td>';
                    $ledgerString.= '<td width="100" class="amount">$' . number_format($meta['_mpp_amount_value'][0],2) . '</td>';
                    $ledgerString.= '<td width="500" class="description">' . $meta['_mpp_reason_value'][0] . '</td>';
                    $ledgerString.= '<td width="100">
                                <div class="linker" style="float:left; margin-right:10px;">
                                    <a class="edittransaction" id="' . $deposittransaction->ID . '"><img style="width:12px;" src="'. plugins_url() . '/wppmanage/images/Modify.png" /></a>
                                </div>
                                <div class="linker">
                                    <a class="deletetransaction" tenid="'.$tenant->ID.'" delurl="'. $nonce_delete .'">
                                        <img style="width:12px;" src="'. plugins_url() . '/wppmanage/images/delete-icon.gif" />
                                    </a>
                                </div>
                    </td>';

                    $ledgerString.= '</tr>';
                    } else {

                    $ledgerText .='<tr class=" '. $meta['_mpp_tenantid_value'][0].' '.$tenantmeta['_mpp_property_value'][0].' '. $type.'">
                    <td width="200" class="tenanme">' . $tenant->post_title . '</td>
                    <td width="100" class="date">' . date("m/d/Y", strtotime($meta['_mpp_transactiondate_value'][0])) . '<span style="display:none;" class="calcdate">'.$meta['_mpp_transactiondate_value'][0].'</span></td>
                    <td width="100" class="amount">'.$starter.'$' . number_format($meta['_mpp_amount_value'][0],2) . '</td>
                    <td width="400" class="description">' . $meta['_mpp_reason_value'][0] . '</td>
                    <td width="100" class="type">' . ucfirst($type) . '</td>
                    <td width="100">
                                 <div class="linker" style="float:left; margin-right:10px;">
                                        <a class="edittransaction" id="' . $deposittransaction->ID . '"><img style="width:12px;" src="'. plugins_url() . '/wppmanage/images/Modify.png" /></a>
                                    </div>
                                    <div class="linker">
                                        <a class="deletetransaction" tenid="'.$tenant->ID.'" delurl="'. $nonce_delete .'">
                                            <img style="width:12px;" src="'. plugins_url() . '/wppmanage/images/delete-icon.gif" />
                                        </a>
                                    </div>
                    </td>
                    </tr>';

                }

            }

            $ledgerString .= '</table>';
            $ledgerString .= '
            <div class="sorting">
                <table id="sort-by" style="background-color: white;">
                    <tr style="font-weight:bold;">
                        <td width="200"></td>
                        <td width="100">Balance : </td>
                        <td width="100">$' . number_format(checkTenantDeposit($tenID),2) .'</td>
                        <td width="500"></td>
                        <td width="100"></td>
                    </tr>
                </table>
            </div>';
            $ledgerString .= '</div><br/><hr/>';

            $ledgerText .= '</table>
            <div class="sorting">
                <table id="sort-by" style="background-color: white;">
                    <tr style="font-weight:bold;">
                        <td width="200"></td>
                        <td width="100">Balance :</td>
                        <td width="100">$'.number_format($rent,2).'</td>
                        <td width="400"></td>
                        <td width="100"></td>
                    </tr>
                </table>
            </div>
            </div>';

        echo $ledgerString . '<br/>' . $ledgerText;
        break;
    case 'deletetenant':
        $id = $_POST['id'];
        $email = get_post_meta($id, '_mpp_email_value', true);
        $login = get_post_meta($id, '_mpp_username_value', true);
        $user = get_user_by( 'login', $login );
        wp_delete_user($user->ID);
        wp_delete_post($id);
        break;
    case 'approvetenant':
        $id = $_POST['id'];
        update_post_meta($id, '_mpp_tenantstatus_value','A');
        break;
    case 'unapprovetenant':
        $id = $_POST['id'];
        update_post_meta($id, '_mpp_tenantstatus_value','0');
        break;
    case 'depositledger':
        $ledger = '
            <style>
            .depositledger{
                width:800px;
                font-family: Arial, Helvetica, sans-serif;

            }
            .header{
                height:40px;
                background-color:#000080;
                color:#ffffff;
                padding-top:8px;
                font-size:25px;
                padding-left:20px;
            }
            .subtitle{
                font-size:15px;
                float:right;
                padding-right:30px;
                padding-top:8px;
            }
            .prepared{
                float:left;
                width:25%;
                font-size:13px;
            }
            .inner{
                margin:10px;
                margin-top:15px;
            }
                .tenantname{
                    text-transform: capitalize;
                }
                .depositinformation{
                    margin-top:50px;
                }
                .deposittitle{
                    margin-bottom:20px;
                    text-align:center;
                    font-weight:bold;
                    font-size:15px;
                    text-transform: uppercase;
                }
                table td{
                    border: 1px solid grey;
                    font-size:12px;
                }
                table#sort-by td{
                    border:none;
                    background-color:#d3d3d3;
                    text-align:center;
                    text-transform:uppercase;
                    padding:5px;
                }
                .twocol{
                    width:50%;
                    float:left;

                }
                .coltitle{
                    font-weight:bold;
                    font-size:13px;
                    text-transform: uppercase;
                }
                .colinfo{
                    font-size:13px;
                }
                .fourcol{
                    margin-top:60px;
                }
                .totaltitle{
                    width:150px;
                    float:left;
                    font-weight:bold;
                    font-size:15px;
                    text-transform: uppercase;
                    padding:20px;
                    border:1px solid grey;
                    text-align:center;
                }
                .totalamount{
                    width:80px;
                    float:left;
                    font-weight:bold;
                    font-size:15px;
                    text-transform: uppercase;
                    padding:20px; border:1px solid grey;
                    border-left:none;
                    text-align:center;
                }
                .summarytext{
                    width:250px;
                    float:left;
                    font-weight:bold;
                    font-size:12px;
                    text-transform: uppercase;
                    padding-right:30px;
                    padding-left:31px;
                    border:1px solid grey;
                    border-top:none;
                    padding-top:20px;
                    padding-bottom:20px;
                }
            </style>';
        $tenid = $_POST['id'];
        $current_user = wp_get_current_user();
        if($current_user->user_firstname == '' && $current_user->user_lastname == ''){
            $username = "MPP Admin";
        } else {
            $username = $current_user->user_firstname . ' ' . $current_user->user_lastname;
        }
        date_default_timezone_set('America/Denver');

        $ledger .=  '<div class="depositledger">';
        $ledger .= '<div class="header">My Provo Property LLC<div class="subtitle">CHECKOUT SUMMARY</div></div>';

        $ledger .= '<div class="inner">';
        $ledger .= '<div class="prepared"><b>PREPARED BY:</b><br /><span class="pareparedby">'.$username.'</span></div>';
        $ledger .= '<div class="prepared"><b>PREPARED ON:</b><br /><span class="preparedon">'.date('M d, Y h:i A').'</span></div>';
        $ledger .= '<div class="prepared"><b>PREPARED FOR:</b><br /><span class="tenantname">'.get_the_title($tenid).'</span></div>';
        $ledger .= '<div class="prepared"><b>Moved In:    </b>'.get_post_meta($tenid, '_mpp_startdate_value', true) .'</b><br /><b>Moved Out:  </b>'.get_post_meta($tenid, '_mpp_enddate_value', true) .'</div>';
        $ledger .= '<br /><br />';
        $ledger .= '<div class="depositinformation">';
        $ledger .= '<div class="deposittitle">Deposit Summary</div>
        <div class="sorting">
            <table id="sort-by" style="background-color: white;">
                <tr style="font-weight:bold;">
                    <td width="100">Date</td>
                    <td width="100">Type</td>
                    <td width="500">Description</td>
                    <td width="100">Amount</td>
                </tr>
            </table>
        </div>';

        $ledger .= '<table>';
        $args = array(
            'post_type' => 'transactions',
            'posts_per_page' => -1,
            'meta_key' => '_mpp_transactiondate_value',
            'orderby' => 'meta_value_num',
            'order' => 'ASC',
            'meta_query' => array(
                'relation' => 'AND',
                array(
                    'key' => '_mpp_tenantid_value',
                    'value' => $tenid,
                    'compare' => '='
                ),
                array(
                    'key' => '_mpp_transactiontype_value',
                    'value' => 'deposit',
                    'compare' => '='
                )
            )
        );
        $payments = get_posts($args);

        $ledger .= '</tr>';
        $remaining = 0;

        foreach($payments as $payment){

            $meta = get_post_meta($payment->ID);
            $type = $meta['_mpp_transactiontype_value'][0];
            $nonce_delete = wp_nonce_url(get_bloginfo("url") . "/wp-admin/post.php?action=delete&amp;post=" . $payment->ID, "delete-post_" . $payment->ID);

            $tenant = get_post($meta['_mpp_tenantid_value'][0]);
            $tenantmeta = get_post_meta($tenant->ID);
            $property = get_post($tenantmeta['_mpp_property_value'][0]);

            $remaining = $remaining + $meta['_mpp_amount_value'][0];
            $typetext = ($meta['_mpp_amount_value'][0] > 0)? 'Deposit' : 'Deduction';
            $ledger .= '<tr>';
            $ledger .= '<td width="100" class="date" style="text-align:center;">' . date("m/d/Y", strtotime($meta['_mpp_transactiondate_value'][0])) . '<span style="display:none;" class="calcdate">'.$meta['_mpp_transactiondate_value'][0].'</span></td>';
            $ledger .= '<td width="100" class="type" style="text-align:center; text-transform:capitalize;">'.$typetext.'</td>';
            $ledger .= '<td width="500" colspan="5" class="description">' . $meta['_mpp_reason_value'][0] . '</td>';
            $ledger .= '<td width="100" class="amount" style="text-align:center;">$' . number_format($meta['_mpp_amount_value'][0],2) . '</td>';
            $ledger .= '</td>'; ?>
            <?php
            $ledger .= '</tr>';
        }

        $ledger .= '</table>';

        $ledger .= '
        <div class="sorting">
            <table id="sort-by" style="background-color: white;">
                <tr style="font-weight:bold;">
                    <td width="100"></td>
                    <td width="100"></td>
                    <td width="500" style="text-align:right;">Total Balance :</td>
                    <td width="100">$' . number_format($remaining,2) . '</td>
                </tr>
            </table>
        </div>';

        if($remaining > 0){
            $deposittext = 'Deposit Refund';
            $summarytext = 'Your refund check is included with this notice.';
        } else {
            $deposittext = 'Amount Due';
            $deposit = $deposit * -1;
            $summarytext = 'Please remit payment to My Provo Property within 30 days of receipt of this notice.';
            $due = true;
        }

        $ledger .= '<div class="fourcol">';
        $ledger .= '<div class="twocol" style="margin-right:74px;">
                <div class="coltitle">Direct all inquiries to:</div><br />
                <div class="colinfo">Shalisa Larson<br />(801) 784-0624<br />email: payments@myprovoproperty.com</div>';

        if($due){
            $ledger .= '<br /><div class="coltitle">Make all checks payable to:</div><br />
                <div class="colinfo">My Provo Property<br />PO Box 446<br />Orem, UT 84059</div>';
            $ledger .= '<div style="float:none; clear:both;"></div>';
        }

         $ledger .= '</div>';
        $ledger .= '<div>
                <div class="totaltitle">' . $deposittext . '</div>
                <div class="totalamount">$'.number_format($remaining,2).'</div>
                <div class="summarytext">'.$summarytext.'</div>
                </div>';
        $ledger .= '<div style="float:none; clear:both;"></div>';

        $ledger .= '</div></div></div>';
        $ledger .= '<div class="header" style="margin-top:30px;"><div class="subtitle" style="text-transform: uppercase; font-size:15px;">Thank you for staying with my provo property llc </div></div>';

        echo $ledger;
        break;
    case 'addtransaction':
        $transactiondate = date("Ymd", strtotime($_POST['date']));
        $amount = $_POST['amount'];
        $type = $_POST['type'];
        $description = $_POST['description'];
        $id = $_POST['id'];
        $name = $_POST['name'];
        $method = $_POST['method'];
        $entity = $_POST['entity'];
        $editpost = $_POST['postid'];

        if($entity == 'properties'){

            $args = array(
                'posts_per_page' => -1,
                'meta_query' => array(
                    'relation' => 'AND',
                    array(
                        'key' => '_mpp_tenantstatus_value',
                        'value' => '2',
                        'compare' => '='
                    ),
                    array(
                        'key' => '_mpp_property_value',
                        'value' => $id,
                        'compare' => '='
                    )
                ),
                'post_type' => 'tenants'
            );
            $tenants = get_posts($args);

            foreach($tenants as $tenant){

                $tenantId = $tenant->ID;
                $post = array(
                    'post_type' => 'transactions',
                    'post_status' => 'publish',
                    'post_title' => $tenant->post_title
                );

                $newfeeid = wp_insert_post($post, false);

                if($newfeeid != 0){
                    add_post_meta($newfeeid, '_mpp_transactiondate_value', $transactiondate, true);
                    add_post_meta($newfeeid, '_mpp_amount_value', $amount, true);
                    add_post_meta($newfeeid, '_mpp_reason_value', $description, true);
                    add_post_meta($newfeeid, '_mpp_tenantid_value', $tenantId, true);
                    add_post_meta($newfeeid, '_mpp_transactiontype_value', $type, true);
                    after_transaction_change($newfeeid);
                } else {
                    $returnText = 'FAILED TO MAKE POST PROPERTY';
                }
            }
        } else {

            if($editpost){
                $post = array(
                    'ID' => $editpost,
                    'post_type' => 'transactions',
                    'post_status' => 'publish',
                    'post_title' => $name
                );

                wp_update_post($post);

                $newtransaction = $editpost;

            } else {
                $post = array(
                    'post_type' => 'transactions',
                    'post_status' => 'publish',
                    'post_title' => $name
                );

                $newtransaction = wp_insert_post($post, false);
            }

            update_post_meta($newtransaction, '_mpp_transactiondate_value', $transactiondate);
            update_post_meta($newtransaction, '_mpp_amount_value', $amount);
            update_post_meta($newtransaction, '_mpp_method_value', $method);
            update_post_meta($newtransaction, '_mpp_reason_value', $description);
            update_post_meta($newtransaction, '_mpp_tenantid_value', $id);
            update_post_meta($newtransaction, '_mpp_transactiontype_value', $type);
            after_transaction_change($newtransaction);

        }
        break;
    case 'gettransaction':
        $post_id = $_POST['id'];
        $post = get_post($post_id);
        $meta = get_post_meta($post_id);
        $selectedt = '';
          $display = 'style="display:none;"';
        switch($meta['_mpp_transactiontype_value'][0]){
            case 'fee':
                $fee = 'selected';
                break;
            case 'rent':
                $rent = 'selected';
                break;
            case 'deposit':
                $ded = 'selected';
                break;
            case 'payment':
                $pmt = 'selected';
                $display = '';
                break;
        }

        switch($meta['_mpp_method_value'][0]){
              case 'Paypal':
                  $paypal = 'selected';
                  break;
              case 'Intuit':
                  $intuit = 'selected';
                  break;
              case 'Check':
                  $check = 'selected';
                  break;
              case 'Other':
                  $other = 'selected';
                  break;
        }

        $response = '<div class="postbox-container" style="float:left;">
                        <div class="meta-box-sortables">
                            <div class="postbox" style="width:300px;">
                                <h3 class="hndle"><span style="padding-left:12px;">Edit Transaction</span></h3>
                                <div class="inside">
                                    <div class="meta-field">
                                        <input type="hidden" name="_mpp_pbalance_noncename" id="_mpp_pbalance_noncename" value="28cb5b2291">
                                        <input type="hidden" id="elocation" name="location" value="ledger">
                                        <input type="hidden" id="popup" name="popup" value="true">
                                        <div style="display:none;" class="" id="eeditid">'.$_POST['id'].'</div>
                            <span id="etenantselector">
                                <label for="tenant_name">Tenant </label>
                                <select name="tenant_name" id="etenant_name" style="float:right;">
                                    <option value="">Please Select </option>';
                                foreach($tenants as $tenant){
                                    $selectedt = ($tenant->post_title == $post->post_title) ? 'selected' : '' ;
                                    $response .= '<option '.$selectedt.' value="'.$tenant->ID.'.'.$tenant->post_title.'">' . $tenant->post_title .'</option>';
                                }
        $response .= '</select>
                                <div style="clear:both"></div>
                                </span>
                            <label for="transactiondate">Date </label><input style="width:60%; float:right;" type="text" name="transactiondate" id="etransactiondate" value="'.date("m/d/Y", strtotime($meta['_mpp_transactiondate_value'][0])).'"><br>
                            <div style="clear:both"></div>
                            <label for="transactionamount">Amount </label>
                            <input style="width:60%; float:right;" type="text" class="dollarinput" name="transactionamount" id="etransactionamount" value="'.number_format($meta['_mpp_amount_value'][0],2,".","").'"  onKeyPress="return false;"><br>
                            <div style="clear:both"></div>
                            <label for="transactiontype">Type </label>
                            <select name="transactiontype" id="etransactiontype" style="float:right;">
                                <option value="">Please Select </option>
                                <option '.$ded.' value="deposit">Deposit</option>
                                <option '.$fee.' value="fee">Fee Due </option>
                                <option '.$rent.' value="rent">Rent Due </option>
                                <option '.$pmt.' value="payment">Payment</option>
                            </select>
                            <div style="clear:both"></div>
                            <span id="epaymethod" '.$payment.'>
                                                <label for="transactionmethod">Pay Method </label>
                                                <select name="transactionmethod" id="etransactionmethod" style="float:right;">
                                                    <option value="">Please Select </option>
                                                    <option '.$paypal.' value="Paypal">Paypal</option>
                                                    <option '.$intuit.' value="Intuit">Intuit</option>
                                                    <option '.$check.' value="Check">Check</option>
                                                    <option '.$other.' value="Other">Other</option>
                                                </select>
                                                <div style="clear:both"></div>
                                            </span>
                            <label for="transactiondescription">Description </label><br>
                            <textarea rows="4" style="width:100%;" name="transactiondescription" id="etransactiondescription">'.$meta['_mpp_reason_value'][0].'</textarea><br>
                            <button type="button" class="edit" id="edittransactionbutton">Edit Transaction</button><span id="etransactionstatus" class="transactionstatus"></span>
                            <button type="button" id="canceledittransactionbutton">Cancel</button>
                            </div><br style="clear:both"></div>
                            </div>
                            </div>
                            </div>';
        echo $response;

        break;
    case 'newtransaction':
        $response = '<div id="postbox-container-1" class="postbox-container recordform" style="width:700px; float:left;">
                        <div id="side-sortables" class="meta-box-sortables">
                            <div id="new-meta-boxes-leder-transaction" class="postbox" style="width:300px;">
                                <h3 class="hndle"><span style="margin-left:25px;">Record Transaction</span></h3>
                                <div class="inside">
                                    <div class="meta-field">
                                        <input type="hidden" name="_mpp_pbalance_noncename" id="_mpp_pbalance_noncename" value="28cb5b2291">
                                        <input type="hidden" id="location" name="location" value="overview">
                                        <input type="hidden" id="popup" name="popup" value="true">
                                        <div style="display:none;" class="" id="editid"></div>
                                        <span id="tenantselector">
                                            <label for="tenant_name">Tenant </label>
                                            <select name="tenant_name" id="tenant_name" style="float:right;">
                                                <option value="">Please Select </option>';
                                                foreach($tenants as $tenant){
                                                    $response .= '<option value="'.$tenant->ID.'.'.$tenant->post_title.'">' . $tenant->post_title .'</option>';
                                                }
        $response .=                       '</select>
                                            <div style="clear:both"></div>
                                        </span>
                                        <span id="aptselector">
                                            <label for="property_id">Apt </label>
                                            <select name="property_id" id="property_id" style="float:right;">
                                                <option value="">Please Select </option>';
                                                foreach($properties as $property){
                                                    $response .= '<option value="'.$property->ID.'">' . $property->post_title .'</option>';
                                                }
        $response .=                    '</select>
                                            <div style="clear:both"></div>
                                        </span>
                                        <label for="transactiondate">Date </label><input style="width:60%; float:right;" type="text" name="transactiondate" id="transactiondate" value="'. date('m/d/Y') .'"><br>
                                        <div style="clear:both"></div>
                                        <label for="transactionamount">Amount </label>
                                        <input style="width:60%; float:right;" type="text" class="dollarinput" name="transactionamount" id="transactionamount" onkeypress="return false;"><br>
                                        <div style="clear:both"></div>
                                        <label for="transactiontype">Type </label>
                                        <select name="transactiontype" id="transactiontype" style="float:right;">
                                            <option value="">Please Select </option>
                                            <option value="deposit">Deposit</option>
                                            <option value="fee">Fee Due </option>
                                            <option value="rent">Rent Due </option>
                                            <option value="payment">Payment</option>
                                        </select>
                                        <div style="clear:both"></div>
                                        <span id="paymethod" style="display:none;">
                                            <label for="transactionmethod">Pay Method </label>
                                            <select name="transactionmethod" id="transactionmethod" style="float:right;">
                                                <option value="">Please Select </option>
                                                <option value="Paypal">Paypal</option>
                                                <option value="Intuit">Intuit</option>
                                                <option value="Check">Check</option>
                                                <option value="Other">Other</option>
                                            </select>
                                            <div style="clear:both"></div>
                                        </span>
                                        <label for="transactiondescription">Description </label><br>
                                        <textarea rows="4" style="width:100%;" name="transactiondescription" id="transactiondescription"></textarea><br>
                                        <button type="button" id="recordtransactionbutton">Record Transaction</button><span id="transactionstatus" class="transactionstatus"></span>
                                        <button type="button" id="canceledittransactionbutton">Cancel</button>
                                    </div><br style="clear:both">
                                </div>
                            </div>
                        </div>
                    </div>';

        echo $response;

        break;

    case 'updateledger':

        $count = $_POST['count'];
        $count = ($count) ? $count : -1;

        $args = array(
            'post_type' => 'transactions',
            'post_status' => 'publish',
            'posts_per_page' => $count,
            'orderby' => 'modified'
        );
        $payments = get_posts($args);
        $result = '';
        foreach($payments as $payment){

            $meta = get_post_meta($payment->ID);
            $type = $meta['_mpp_transactiontype_value'][0];
            $nonce_delete = wp_nonce_url(get_bloginfo("url") . "/wp-admin/post.php?action=delete&amp;post=" . $payment->ID, "delete-post_" . $payment->ID);

            $tenant = get_post($meta['_mpp_tenantid_value'][0]);
            $tenantmeta = get_post_meta($tenant->ID);
            $property = get_post($tenantmeta['_mpp_property_value'][0]);

            $result .= '<div class="isotopeitem '. $meta['_mpp_tenantid_value'][0].' '.$tenantmeta['_mpp_property_value'][0].' '. $type.'">';

            $result .=  '<div style="width:150px;" class="name">' . $tenant->post_title . '</div>';
            $result .=  '<div style="width:100px" class="property">'.get_the_title($property->ID) .'</div>';
            $result .=  '<div style="width:100px" class="date">' . date("m/d/Y", strtotime($meta['_mpp_transactiondate_value'][0])) . '<span style="display:none;" class="calcdate">'.$meta['_mpp_transactiondate_value'][0].'</span></div>';
            $result .=  '<div style="width:75px" class="amount">$' . number_format($meta['_mpp_amount_value'][0],2,".","") . '</div>';
            $result .=  '<div style="width:200px" class="description">' . $meta['_mpp_reason_value'][0] . '&nbsp; </div>';
            $result .=  '<div style="width:105px" class="type">' . ucfirst($type) . '</div>';
            $result .= '<div class="linker">
                <a class="edittransaction" id="'. $payment->ID .'">
                    <img style="width:12px;" src="'. plugins_url() .'/wppmanage/images/Modify.png" />
                </a>
            </div>';
            $result .= '<div class="linker">
                <a class="deletetransaction" tenid="'.$tenant->ID.'" delurl="'. $nonce_delete .'">
                    <img style="width:12px;" src="'. plugins_url() . '/wppmanage/images/delete-icon.gif" />
                </a>
            </div>';
            $result .= '<div class="clearall"></div></div>';
        }

        echo $result;
        break;
    default:
        $returnText = 'Access Denied:  Invalid Action';
        break;
}

return $returnText;

//TODO: Tie into the admin-ajax and give these commands their actions on the back end there