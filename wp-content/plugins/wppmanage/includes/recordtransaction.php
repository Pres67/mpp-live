<div id="postbox-container-1" class="postbox-container recordform" style="width:700px; float:left;">
    <div id="side-sortables" class="meta-box-sortables">
        <div id="new-meta-boxes-leder-transaction" class="postbox" style="width:300px;">
            <h3 class="hndle"><span style="margin-left:25px;">Record Transaction</span></h3>
            <div class="inside">
                <div class="meta-field">
                    <input type="hidden" name="_mpp_pbalance_noncename" id="_mpp_pbalance_noncename" value="28cb5b2291">
                    <input type="hidden" id="location" name="location" value="ledger">
                    <div style="display:none;" class="" id="editid"></div>
                    <span id="tenantselector">
                        <label for="tenant_name">Tenant </label>
                        <select name="tenant_name" id="tenant_name" style="float:right;">
                            <option value="">Please Select </option>
                            <?php foreach($tenants as $tenant){
                                echo '<option value="'.$tenant->ID.'.'.$tenant->post_title.'">' . $tenant->post_title .'</option>';
                            } ?>
                        </select>
                        <div style="clear:both"></div>
                    </span>
                    <span id="aptselector">
                        <label for="property_id">Apt </label>
                        <select name="property_id" id="property_id" style="float:right;">
                            <option value="">Please Select </option>
                            <?php foreach($properties as $property){
                                echo '<option value="'.$property->ID.'">' . $property->post_title .'</option>';
                            } ?>
                        </select>
                        <div style="clear:both"></div>
                    </span>
                    <label for="transactiondate">Date </label><input style="width:60%; float:right;" type="text" name="transactiondate" id="transactiondate" value="<?php echo date('m/d/Y'); ?>"><br>
                    <div style="clear:both"></div>
                    <label for="transactionamount">Amount </label>
                    <input style="width:60%; float:right;" type="text" class="dollarinput" name="transactionamount" id="transactionamount" onkeypress="return false;"><br>
                    <div style="clear:both"></div>
                    <label for="transactiontype">Type </label>
                    <select name="transactiontype" id="transactiontype" style="float:right;">
                        <option value="">Please Select </option>
                        <option value="deposit">Deposit</option>
                        <option value="fee">Fee Due </option>
                        <option value="rent">Rent Due </option>
                        <option value="payment">Payment</option>
                    </select>
                    <div style="clear:both"></div>
                    <span id="paymethod" style="display:none;">
                        <label for="transactionmethod">Pay Method </label>
                        <select name="transactionmethod" id="transactionmethod" style="float:right;">
                            <option value="">Please Select </option>
                            <option value="Paypal">Paypal</option>
                            <option value="Intuit">Intuit</option>
                            <option value="Check">Check</option>
                            <option value="Other">Other</option>
                        </select>
                        <div style="clear:both"></div>
                    </span>
                    <label for="transactiondescription">Description </label><br>
                    <textarea rows="4" style="width:100%;" name="transactiondescription" id="transactiondescription"></textarea><br>
                    <button type="button" id="recordtransactionbutton">Record Transaction</button><span id="transactionstatus" class="transactionstatus"></span>
                </div><br style="clear:both">
            </div>
        </div>
    </div>
</div>