<?php

/*
update_post_meta(3074, '_mpp_username_value','kdavis');
update_post_meta(3074, '_mpp_password_value','F8je4H8f');
*/
/*
Plugin Name: WP Property Management
Plugin URI: none
Description: Full service plugin for property and tenant management
Author: Little Blue Wagon, LLC
Version: 0.1
Author URI: http://www.littlebluewagon.com
*/

/* TODO: Combine style/custom calls into less calls */
/* TODO: create vacancies page and template automatically */
/* TODO: Make page insertion happen on plugin activation - and page removal on deletion */
/* TODO : Make redirect locations backend options */
/* TODO:  Allow choosing of what profile things to remove, etc. - also limit just to the right users (tenants) */
/* TODO:  Redirect users away from the dashboard to their main page */
/* TODO: Get google analytics code in this plugin somehow */

date_default_timezone_set('America/Denver');
global $user;

require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
require_once('includes/functions.php');
require_once('includes/types.php');

add_action('template_redirect', 'mpp_redirect');
add_action('admin_enqueue_scripts', 'wppm_load_scripts' );
add_action('wp_enqueue_scripts', 'mpp_enqueue_script' );
add_action('admin_bar_menu', 'wppm_admin_bar_custom', 11 );
add_action('admin_enqueue_scripts', 'admin_theme_style');
add_action('login_enqueue_scripts', 'admin_theme_style');
add_action('login_head', 'custom_login_logo');
add_action('login_enqueue_scripts', 'my_login_stylesheet' );
add_action('admin_menu', 'wppm_vacancies_page');
add_action('wp_logout','go_home');
add_action('publish_tenants', 'mpp_create_user');
add_action('admin_menu', 'wppm_redirect_dashboard');
add_action('admin_init', 'wpse_remove_footer' );
add_action('admin_notices', 'hide_update_notice', 1 );
add_action('admin_menu', 'remove_contact_menu');
add_action('wp_before_admin_bar_render', 'remove_admin_bar_links' );
add_action('admin_menu', 'remove_menus' );
add_action('admin_head-user-edit.php','clean_profile_page');
add_action('admin_head-profile.php','clean_profile_page');
add_action('_mpp_add_rent', '_mpp_check_rent' );
add_action('_mpp_add_late_fees', '_mpp_check_fees' );
add_action( 'plugins_loaded', array( 'PageTemplater', 'get_instance' ) );
add_action( 'save_post', 'after_transaction_change');
add_action( 'wp_insert_post', 'after_transaction_change' );
add_action( 'before_delete_post', 'before_transaction_delete' );
add_action( 'wpcf7_mail_sent', 'create_user_after_application' );


add_filter('page_template', 'wppm_page_template' );
add_filter('contextual_help', 'mycontext_remove_help', 999, 3 );
add_filter('wp_mail_from', 'new_mail_from');
add_filter('wp_mail_from_name', 'new_mail_from_name');
add_filter("login_redirect", "my_login_redirect", 10, 3);

register_activation_hook( __FILE__, '_mpp_activation' );
register_deactivation_hook( __FILE__, '_mpp_deactivation' );