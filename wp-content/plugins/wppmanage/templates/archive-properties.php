<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
	exit;
}
$term = 'Fall 2016';
$wargs = array(
    'meta_query' => array(
        array(
            'key' => '_mpp_pgender_value',
            'value' => 'F'
        )
    ),
    'post_type' => 'properties',
    'orderby' => 'title',
    'order' => 'ASC'
);

$margs = array(
    'meta_query' => array(
        array(
            'key' => '_mpp_pgender_value',
            'value' => 'M'
        )
    ),
    'post_type' => 'properties',
    'orderby' => 'title',
    'order' => 'ASC'
);

$wprops = get_posts($wargs);
$mprops = get_posts($margs);



get_header(); ?>

<div id="content-archive">

<!--
<?php /*
   <div class="pagetitle">
        Property Openings
        <p style="color:green;">Current Openings</b></p>
    </div>


 <h2>Men's Units</h2>

    <?php foreach($mprops as $prop){
        $propmeta = get_post_meta($prop->ID); 
	if($propmeta['_mpp_apt_value'][0] == '104'){		
?>
        <div id="post-<?php echo $prop->ID; ?>" <?php post_class(); ?>>
            <div class="post-entry" style="width:100%; margin-bottom:20px;">

                <?php if(!empty($propmeta['_mpp_galleryid_value'][0])){ ?>
                    <div class="adimages">
                        <?php echo do_shortcode('[nggallery id=' .$propmeta['_mpp_galleryid_value'][0] . ']'); ?>
                        <div class="imageicon"><img src="<?php echo plugins_url() . '/wppmanage/images/gallery.png'; ?>"/></div>
                    </div>
                <? } ?>

                <?php $openings = ($propmeta['_mpp_openings_value'][0])? '<span class="available">' . $propmeta['_mpp_openings_value'][0] . ' ' . $propmeta['_mpp_roomtype_value'][0] . '</span>': '<span style="color:red;">Full</span>'; ?>

                <div class="propcontent" style="float:left; margin-left:10px;">
                    <div class="proptitles">
                        <div class="entry-title post-title" style="font-size:25px; font-weight:400; padding-top:7px;"><?php echo $propmeta['_mpp_nickname_value'][0] . " #" . $propmeta['_mpp_apt_value'][0]; ?></div>
                        <div class="addy" style="font-size:10px; font-style:italic;"><?php echo $propmeta['_mpp_address_value'][0] . ' - ' . $propmeta['_mpp_city_value'][0] . ', '. $propmeta['_mpp_state_value'][0] . ' ' . $propmeta['_mpp_zip_value'][0]; ?></div>
                    </div>
                    <?php if($propmeta['_mpp_openings_value'][0] > 0){ ?>
                        <div class="applybuttontop"><a href="http://www.myprovoproperty.com/apply/<?php echo '?prop=' . get_the_title($prop->ID) . '&term=' . $term; ?>">APPLY NOW</a></div>
                    <?php }?>
                    <div style="clear:both;"></div>
                    <div class="propinfo">
                        <ul class="proplist" style="padding-left:0px;">
                            <li class="roomtype"><label>Rooms</label><?php echo $propmeta['_mpp_capacity_value'][0] . ' ' . $propmeta['_mpp_roomtype_value'][0]; ?></li>
                            <li class="roomtype"><label>Deposit</label>$<?php echo $propmeta['_mpp_pdeposit_value'][0]; ?></li>
                            <li class="roomtype"><label>Rent</label>$<?php echo $propmeta['_mpp_yrrent_value'][0]; ?></li>
                            <li class="roomtype"><label>Utilities You Pay</label><?php echo $propmeta['_mpp_utilities_value'][0]; ?></li>
                            <li class="roomtype"><label>Spots Available</label>1</li>
                            <li class="roomtype"><label>Contract Length</label>To Aug 12, '16</li>
                        </ul>
                    </div>
                    <div style="clear:both;"></div>
                    <div class="description">
                        <div class="desctext" style="font-size:12px; margin-top:10px;">
                            <?php echo $propmeta['_mpp_adtext_value'][0]; ?>
                        </div>
                    </div>
                </div>

                <?php the_excerpt(); ?>
            </div><!-- end of .post-entry -->
        </div><!-- end of #post- -->
        <div style="clear:both;"></div>
    <?php }} ?>
*/ ?>
-->

    <div class="pagetitle">
        Property Openings
        <p style="color:red;">No Openings for <b>Fall 2016</b></p>
    </div>

    <h2>Women's Units - <?php echo $term; ?></h2>

    <?php foreach($wprops as $prop){
        $propmeta = get_post_meta($prop->ID); ?>
			<div id="post-<?php echo $prop->ID; ?>" <?php post_class(); ?>>
				<div class="post-entry" style="width:100%; margin-bottom:20px;">

                    <?php if(!empty($propmeta['_mpp_galleryid_value'][0])){ ?>
                    <div class="adimages">
                        <?php echo do_shortcode('[nggallery id=' .$propmeta['_mpp_galleryid_value'][0] . ']'); ?>
                        <div class="imageicon"><img src="<?php echo plugins_url() . '/wppmanage/images/gallery.png'; ?>"/></div>
                    </div>
                    <? } ?>

                    <?php $openings = ($propmeta['_mpp_openings_value'][0])? '<span class="available">' . $propmeta['_mpp_openings_value'][0] . ' ' . $propmeta['_mpp_roomtype_value'][0] . '</span>': '<span style="color:red;">Full</span>'; ?>

                    <div class="propcontent" style="float:left; margin-left:10px;">
                        <div class="proptitles">
                            <div class="entry-title post-title" style="font-size:25px; font-weight:400; padding-top:7px;"><?php echo $propmeta['_mpp_nickname_value'][0] . " #" . $propmeta['_mpp_apt_value'][0]; ?></div>
                            <div class="addy" style="font-size:10px; font-style:italic;"><?php echo $propmeta['_mpp_address_value'][0] . ' - ' . $propmeta['_mpp_city_value'][0] . ', '. $propmeta['_mpp_state_value'][0] . ' ' . $propmeta['_mpp_zip_value'][0]; ?></div>
                        </div>
                        <?php if($propmeta['_mpp_openings_value'][0] > 0){ ?>
                            <div class="applybuttontop"><a href="http://www.myprovoproperty.com/apply/<?php echo '?prop=' . get_the_title($prop->ID) . '&term=' . $term; ?>">APPLY NOW</a></div>
                        <?php }?>
                        <div style="clear:both;"></div>
                        <div class="propinfo">
                            <ul class="proplist" style="padding-left:0px;">
                                <li class="roomtype"><label>Rooms</label><?php echo $propmeta['_mpp_capacity_value'][0] . ' ' . $propmeta['_mpp_roomtype_value'][0]; ?></li>
                                <li class="roomtype"><label>Deposit</label>$<?php echo $propmeta['_mpp_pdeposit_value'][0]; ?></li>
                                <li class="roomtype"><label>Rent</label>$<?php echo $propmeta['_mpp_yrrent_value'][0]; ?></li>
                                <li class="roomtype"><label>Utilities You Pay</label><?php echo $propmeta['_mpp_utilities_value'][0]; ?></li>
                                <li class="roomtype"><label>Spots Available</label><?php echo $openings; ?></li>
                                <li class="roomtype"><label>Contract Length</label><?php echo $propmeta['_mpp_contractlength_value'][0]; ?></li>
                            </ul>
                        </div>
                        <div style="clear:both;"></div>
                        <div class="description">
                            <div class="desctext" style="font-size:12px; margin-top:10px;">
                               <?php echo $propmeta['_mpp_adtext_value'][0]; ?>
                            </div>
                        </div>
                    </div>

                    <?php the_excerpt(); ?>
				</div><!-- end of .post-entry -->
			</div><!-- end of #post- -->
            <div style="clear:both;"></div>
    <?php } ?>

    <h2>Men's Units - <?php echo $term; ?></h2>

    <?php foreach($mprops as $prop){
        $propmeta = get_post_meta($prop->ID); ?>
        <div id="post-<?php echo $prop->ID; ?>" <?php post_class(); ?>>
            <div class="post-entry" style="width:100%; margin-bottom:20px;">

                <?php if(!empty($propmeta['_mpp_galleryid_value'][0])){ ?>
                    <div class="adimages">
                        <?php echo do_shortcode('[nggallery id=' .$propmeta['_mpp_galleryid_value'][0] . ']'); ?>
                        <div class="imageicon"><img src="<?php echo plugins_url() . '/wppmanage/images/gallery.png'; ?>"/></div>
                    </div>
                <? } ?>

                <?php $openings = ($propmeta['_mpp_openings_value'][0])? '<span class="available">' . $propmeta['_mpp_openings_value'][0] . ' ' . $propmeta['_mpp_roomtype_value'][0] . '</span>': '<span style="color:red;">Full</span>'; ?>

                <div class="propcontent" style="float:left; margin-left:10px;">
                    <div class="proptitles">
                        <div class="entry-title post-title" style="font-size:25px; font-weight:400; padding-top:7px;"><?php echo $propmeta['_mpp_nickname_value'][0] . " #" . $propmeta['_mpp_apt_value'][0]; ?></div>
                        <div class="addy" style="font-size:10px; font-style:italic;"><?php echo $propmeta['_mpp_address_value'][0] . ' - ' . $propmeta['_mpp_city_value'][0] . ', '. $propmeta['_mpp_state_value'][0] . ' ' . $propmeta['_mpp_zip_value'][0]; ?></div>
                    </div>
                    <?php if($propmeta['_mpp_openings_value'][0] > 0){ ?>
                        <div class="applybuttontop"><a href="http://www.myprovoproperty.com/apply/<?php echo '?prop=' . get_the_title($prop->ID) . '&term=' . $term; ?>">APPLY NOW</a></div>
                    <?php }?>
                    <div style="clear:both;"></div>
                    <div class="propinfo">
                        <ul class="proplist" style="padding-left:0px;">
                            <li class="roomtype"><label>Rooms</label><?php echo $propmeta['_mpp_capacity_value'][0] . ' ' . $propmeta['_mpp_roomtype_value'][0]; ?></li>
                            <li class="roomtype"><label>Deposit</label>$<?php echo $propmeta['_mpp_pdeposit_value'][0]; ?></li>
                            <li class="roomtype"><label>Rent</label>$<?php echo $propmeta['_mpp_yrrent_value'][0]; ?></li>
                            <li class="roomtype"><label>Utilities You Pay</label><?php echo $propmeta['_mpp_utilities_value'][0]; ?></li>
                            <li class="roomtype"><label>Spots Available</label><?php echo $openings; ?></li>
                            <li class="roomtype"><label>Contract Length</label><?php echo $propmeta['_mpp_contractlength_value'][0]; ?></li>
                        </ul>
                    </div>
                    <div style="clear:both;"></div>
                    <div class="description">
                        <div class="desctext" style="font-size:12px; margin-top:10px;">
                            <?php echo $propmeta['_mpp_adtext_value'][0]; ?>
                        </div>
                    </div>
                </div>

                <?php the_excerpt(); ?>
            </div><!-- end of .post-entry -->
        </div><!-- end of #post- -->
        <div style="clear:both;"></div>
    <?php } ?>

    <div class="generalapplication">
        <h4>Don't have a specific apartment? -- Looking for housing for next semester/year?  <br />Apply online to be pre-approved and ready to take the best available slots! </h4>
    </div>
    <div class="applybuttonbottom"><a href="http://www.myprovoproperty.com/apply/">APPLY NOW</a></div>




</div><!-- end of #content-archive -->

<?php get_footer(); ?>