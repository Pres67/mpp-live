(function ($) {

    "use strict";
    //when auto add my signature checked
    $('input[name="add_signature"]').on('change', function () {
        if ($('input[name="add_signature"]').attr('checked')) {
           
            // show tb window of admin signature view
            tb_show('+Terms Of Use', esigAutoadd.fileURL + '?width=350&height=350', false);
            // check mark false untill user click in agree and sign
            $('input[name="add_signature"]').prop('checked', false);

            $('#esig-auto-add-signature-popup').hide();

            $('#esig-terms-condition').show();

            return false;
        }
    });

    $('body').on('click', '#esig-auto-add-confirm', function () {

        if ($('input[name="esign-add-auto-agree"]').attr('checked')) {
            tb_remove();
            $('input[name="add_signature"]').prop('checked', true);
            return true;
        }
        else {
            tb_remove();
            $('input[name="add_signature"]').prop('checked', false);
            return false;
        }

    });

    $('body').on('click', '#esign-goback', function () {

        $('#esig-auto-add-signature-popup').show();
        $('#esig-terms-condition').hide();

    });

  


    // terms of use 
    // click terms of service . 
    $('body').on('click', '#esig-terms', function () {

        $('#esig-auto-add-signature-popup').hide();
        $('#esig-terms-condition').show();

        jQuery.ajax({
            type: "POST",
            url: esigAjax.ajaxurl + "?action=esig_terms_condition",
            success: function (data, status, jqXHR) {
                $('#esign-terms-goback').show();
                $('#esign-terms-nextstep').show();
                $('#esign-terms-content').html(data);
            },
            error: function (xhr, status, error) {
                $('.esig-terms-modal-lg .modal-body').html('<h1>No internet connection</h1>');
            }
        });

    });




} (jQuery));