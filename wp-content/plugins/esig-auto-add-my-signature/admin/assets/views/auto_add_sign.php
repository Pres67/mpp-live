<?php
// Silence is golden

require_once('../../../../../../wp-load.php');

if ( file_exists( ABSPATH . 'wp-config.php') ) {

	/** The config file resides in ABSPATH */
	require_once(ABSPATH . 'wp-config.php');

} elseif ( file_exists( dirname(ABSPATH) . '/wp-config.php' ) && ! file_exists( dirname(ABSPATH) . '/wp-settings.php' ) ) {

	/** The config file resides one level above ABSPATH but is not part of another install */
	require_once(dirname(ABSPATH) . '/wp-config.php');

}

$esig = WP_E_Sig();
$api = $esig->shortcode;

$settings = new WP_E_Setting();
$user = new WP_E_User();
$signature = new WP_E_Signature();
$wp_user_id = get_current_user_id();

$WPuser= $user->getUserByWPID($wp_user_id);

?>

<div id="esig-auto-add-signature-popup">
<form>
<div class="auto-signature-agree">
<center><input type="checkbox" name="esign-add-auto-agree" id="esign-add-auto-signature-agree" value="1" checked><b> I Agree (Required)</b></center>
</div>

<div>
I am <b><?php echo $WPuser->first_name ; ?></b> and i understand that by clicking "Agree & Sign" 
<span class="auto-signature-strong"><b>I agree to be legally bound </b></span>by The WP E-Signature <a href="#" id="esig-terms" class="doc-terms"><b><?php _e('Terms of Use', 'esig'); ?></b></a>
and the contents of this agreement in its completed form.
</div>

<div align="center" class="auto-signature-pad">
<?php 

$sig_data=$signature->getSignatureData($WPuser->user_id); 
			
$signature_type = $sig_data->signature_type ;

 $signature_full = '';
			
if($signature_type == 'typed')
{

$signature_type = $signature->getUserSignature_by_type($WPuser->user_id ,'typed');

}
else
{
	$signature_full= $signature->getUserSignature_by_type($WPuser->user_id,'full');

}

$font_choice = $settings->get('esig-signature-type-font'.$WPuser->user_id);
echo '<input type="hidden" name="font_type" id="font-type" value="'. $font_choice .'">';

?>

 <input id="esignature-in-text" value="<?php echo $signature_type ?>" name="esignature_in_text" class="required text-input" maxlength="64" type="hidden">

 <input type="hidden" name="output" class="output" value='<?php echo $signature_full; ?>'/>
	
	
<div class="signature-wrapper-displayonly-signed" >
					
	<canvas id="signatureCanvas2" class="sign-here pad signed" width="420" height="100" ></canvas>
	
	
</div>
<p>
<a href="#" class="esig-blue-btn" id="esig-auto-add-confirm"  title="Agree and submit your signature."><?php _e('Agree & Sign', 'esig') ?></a>
</p>
</div>

</form>
</div><!-- esig-auto-add-signature-popup  -->

<!-- terms and condition start here -->
<div id="esig-terms-condition" style="display:none;">
 
      <p id="esign-terms-goback" style="display:none;"><a href="#" id="esign-goback">Go Back</a></p/>
     
	<div id="esign-terms-content">  <h1>Loading ........</h1></div>
	
     <p id="esign-terms-nextstep" style="display:none;"><a href="#" id="esign-goback">Next Step</a></p/>
</div>


<script type='text/javascript' src='<?php echo plugins_url(); ?>/esig-auto-add-my-signature/admin/assets/js/auto-add-signature.js?ver=1.1.6'></script>