(function($){

$('#esigactivecampaign').click(function(){ 
    var esig_active_tag = $('input[name="esig_active_campaign_tag"]');
	var esig_active_tag_id = $('input[name="esig_active_document_id"]');
    jQuery.ajax({  
        type:"POST",  
        url: active_campaign_script.ajaxurl,   
        data: {
			esig_active_campaign_tag:esig_active_tag.val(),
			esig_active_document_id:esig_active_tag_id.val(),
		},  
        success:function(data, status, jqXHR){    
            jQuery("#esig_active_campaign").html(data);  
        },  
        error: function(xhr, status, error){  
            alert(xhr.responseText); 
        }  
    });  
    return false;  
});

})(jQuery);

