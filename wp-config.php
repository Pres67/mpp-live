<?php
/** Enable Esign Super Admin */
define('ESIGN_DOC_SUPERADMIN_USERID', true); // Added by E-signature



switch($_SERVER['HTTP_HOST']) {
    case 'www.myprovoproperty.com':
    case 'myprovoproperty.com':
    case 'www.littlebluewagon.com':
    case 'littlebluewagon.com':
        require_once(ABSPATH.'wp-config-prod.php');
        break;
    case 'localhost/work/mpp-live/':
    default:
        require_once(ABSPATH.'wp-config-dev.php');
        break;
}